<?php
require __DIR__ . '/database_connection.php';

$result = [
    'success' => false,
    'resultCode' => 400,
    'errorMsg' => '資料不足',
];

$sql = 'SELECT * FROM installation_location';
$stmt = $pdo->query($sql);

$result = [
    'success' => true,
    'resultCode' => 200,
    'error' => '',
    'result' => $stmt->fetchALL(PDO::FETCH_ASSOC),
];

echo json_encode($result['result'], JSON_UNESCAPED_UNICODE);