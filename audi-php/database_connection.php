<?php
// this program establishes a connection with the database. Import this into your other program. 
$database_name = 'audi';
$database_host = '127.0.0.1';
$database_user = 'root';
$database_password = '';

//database source name
// $dsn = 'mysql:dbname=express01; host=localhost';
// do not use spaces in between, it can cause error: 
$dsn = sprintf('mysql:dbname=%s; host=%s', $database_name, $database_host);

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

//If you are not using the default 3306 port, you must set the port number here:
// mysql:dbname=front02_proj; host=localhost; port=3307;

try {
    $pdo = new PDO($dsn, $database_user, $database_password);
    
    // use ut8 unicode when making a connection with the database 
    // (or else Chinese characters wont display properly when you fetch data from the DB)
    $pdo -> query("SET NAMES utf8"); 
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
} catch (PDOException $example) {
    echo 'Connection failed: ' . $example -> getMessage();
};

if(! isset($_SESSION)){
    session_start(); //啓用session功能 - this is necessary for login functionality to work!!
};