<?php
require __DIR__ . '/database_connection.php';
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

$id = intval($_GET['id']);

$result = [
    'success' => false,
    'resultCode' => 400,
    'errorMsg' => '資料不足',
];

$sql = sprintf("SELECT * FROM orders where order_sid=%s",$id) ;
$stmt = $pdo->query($sql);

$result = [
    'success' => true,
    'resultCode' => 200,
    'error' => '',
    'result' => $stmt->fetchALL(PDO::FETCH_ASSOC),
];

echo json_encode($result["result"], JSON_UNESCAPED_UNICODE);