<?php
require __DIR__ . '/database_connection.php';


$id = intval($_GET['id']);

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
$result = [
    'success' => false,
    'resultCode' => 400,
    'errorMsg' => '資料不足',
];

$sql = sprintf("SELECT * FROM mailbox where id=%s",$id );
$stmt = $pdo->query($sql);

$result = [
    'success' => true,
    'resultCode' => 200,
    'error' => '',
    'result' => $stmt->fetch(PDO::FETCH_ASSOC),
];

echo json_encode($result, JSON_UNESCAPED_UNICODE);