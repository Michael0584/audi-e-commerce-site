import React, { Component } from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import "./App.scss";
import ScrollToTopRoute from './ScrollToTopRoute';
import Navbar from "./components/navbar/navbar.js";
import Home from "./components/home/home";
import Footer from "./components/footer/footer";
import Login from "./components/login/login";
import Membercenter from "./components/membercenter/membercenter";
import Chatroom from "./components/chatroom/chatroom";
import AccessoriesProductListPage from "./components/products/accessories/accessories-products-list";
import AccessoriesProductPage from "./components/products/accessories/accessories-products-product-page";
import CollectionsListPage from "./components/products/collections/collections-products-list";
import DetailPage from "./components/products/collections/collections-detail/product_detail";
import Forgot from "./components/login/forgot/forgot";
import Registered from "./components/login/register/registered";
import ShoppingCartPage from "./components/shopping-cart/shopping-cart-page/shopping-cart-page";
import OrderPage from "./components/order/cart-content/cart-content";
import ConfirmedOrder from "./components/order/confirmed-order/confirmed-order";
import Promotion from "./components/promotion/promotion" ;
import Promotion01 from "./components/promotion/promotion_detail/promotion01" ;
import Promotion02 from "./components/promotion/promotion_detail/promotion02" ;
import Promotion03 from "./components/promotion/promotion_detail/promotion03" ;
import Promotion04 from "./components/promotion/promotion_detail/promotion04" ;
import Promotion05 from "./components/promotion/promotion_detail/promotion05" ;
import Promotion06 from "./components/promotion/promotion_detail/promotion06" ;

//import/init audi UI Components
import "dom4";
import "babel-polyfill";
import aui from "@audi/audi-ui";
import { fetchProfile } from "./store.js";

aui.upgradeAllElements();

class App extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		fetchProfile();
	}

	render() {
		return (
			<Router>
				<React.Fragment>
					<Navbar />
					<div className='container-fluid paddingAgainstFixedMenuBar'>
						<ScrollToTopRoute exact path='/' component={Home} />
						<ScrollToTopRoute exact path='/home' component={Home} />
						<ScrollToTopRoute exact path='/index' component={Home} />
						<Route exact path='/login' component={Login} />
						<Route exact path='/login/forgot' component={Forgot} />
						<Route exact path='/login/registered' component={Registered} />
						<Route path='/membercenter' component={Membercenter} />
						<ScrollToTopRoute path='/shopping-cart' component={ShoppingCartPage} />
						<ScrollToTopRoute exact path='/products/accessories' component={AccessoriesProductListPage} />
						<ScrollToTopRoute exact path='/products/accessories/:category' component={AccessoriesProductListPage} />
						<ScrollToTopRoute
							exact
							path='/products/accessories/:category/:subcategory'
							component={AccessoriesProductListPage}
						/>
						<ScrollToTopRoute
							exact
							path='/products/accessories/:category/:subcategory/:product'
							component={AccessoriesProductPage}
						/>
						<ScrollToTopRoute exact path='/products/collections/:category' component={CollectionsListPage} />
						<ScrollToTopRoute
							exact
							path='/products/collections/:category/:subcategory'
							component={CollectionsListPage}
						/>
						<ScrollToTopRoute exact path='/products/collections' component={CollectionsListPage} />
						<ScrollToTopRoute
							exact
							path='/products/collections/:category/:subcategory/:product'
							component={DetailPage}
						/>
						<ScrollToTopRoute exact path='/order' component={OrderPage} />
						<ScrollToTopRoute exact path='/order/:order_id' component={ConfirmedOrder} />
						<ScrollToTopRoute path='/promotion' component={Promotion} />
						<ScrollToTopRoute path='/promotion01' component={Promotion01} />
						<ScrollToTopRoute path='/promotion02' component={Promotion02} />
						<ScrollToTopRoute path='/promotion03' component={Promotion03} />
						<ScrollToTopRoute path='/promotion04' component={Promotion04} />
						<ScrollToTopRoute path='/promotion05' component={Promotion05} />
						<ScrollToTopRoute path='/promotion06' component={Promotion06} />
					</div>
					<Chatroom />
				</React.Fragment>
			</Router>
		);
	}
}

export default App;
