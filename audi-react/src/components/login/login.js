import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
//import Membercenter from '../membercenter/membercenter';

import SignIn from './login-page/signIn';
import Register from './login-page/register';
import './login.scss';

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loggedin: false
    }
  }
  render() {
    return (
      <React.Fragment>
        <div className="container loginCN">
          <Row>
            <Col lg={2}></Col>
            <Col lg={4} className="spr">
              <SignIn />
            </Col>
            <div className="line"></div>
            <Col lg={4} className="spl">
              <Register />
            </Col>
          </Row>
        </div>
      </React.Fragment>  
    );
  }
}

export default Login;