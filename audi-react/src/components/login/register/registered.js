import React, { Component } from 'react';
import { Label, FormGroup, Input, Col, Row, Form } from 'reactstrap';
import TwCitySelector from 'tw-city-selector';

import './registered.scss'

class Registered extends Component {
  constructor(props) {
    super(props)
    this.state = { // 預設值
      email        : "",
      password     : "",
      checkPassword: "",
      username     : "",
      mobile       : "",
      birthday     : "",
      county       : "",
      district     : "",
      address      : ""
      }
      // console.log(this)
  }

  componentWillMount() {
    new TwCitySelector ({
      el: ".my-selector-c",
      elCounty: ".county",
      elDistrict: ".district"
    })
  }

  handlerChange = (evt) => { // 輸入的值產生JSON 格式
    let key   = evt.target.id,
        data  = evt.target.value,
          // 帳號 E-mail 確認
        email               = document.getElementById('email').value,
        email_Function      = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
        email_Check         = email_Function.test(email),
        email_Wrong         = document.getElementById('email_Wrong'),
          // 密碼確認
        password            = document.getElementById('password').value,
        checkPassword       = document.getElementById('checkPassword').value,
        password_Wrong      = document.getElementById('password_Wrong'),
        checkPassword_Wrong = document.getElementById('checkPassword_Wrong'),
          // 電話格式確認
        mobile              = document.getElementById('mobile').value,
        mobile_Wrong        = document.getElementById('mobile_Wrong'),
        mobile_Function     = /^09[0-9]{8}$/,
        mobile_Check        = mobile_Function.test(mobile);
    
    switch(email_Check){  // 帳號 E-mail 確認
      case false:
        email_Wrong.className="alert_Show"; break;
      default:
        email_Wrong.className="alert_Hidden"; 
    } if(email == ""){
      email_Wrong.className="alert_Hidden"; 
    }
    if (password.length < 6){  // 密碼確認
      password_Wrong.className="alert_Show";
      }else if (password != checkPassword) {
        password_Wrong.className="alert_Hidden";
        checkPassword_Wrong.className="alert_Show";
        }else {
            password_Wrong.className="alert_Hidden";
            checkPassword_Wrong.className="alert_Hidden";
          } if (password == ""){
            password_Wrong.className="alert_Hidden";
            checkPassword_Wrong.className="alert_Hidden";
          }
    switch(mobile_Check){ // 電話格式確認
      case false:
        mobile_Wrong.className="alert_Show"; break;
      default:
        mobile_Wrong.className="alert_Hidden";
    } if(mobile == ""){
      mobile_Wrong.className="alert_Hidden";
    }

    this.setState({
      [key]:data
    })
    // console.log(this.state)
  }

  addHandler = () => {
    var email         = document.getElementById('email').value,
        password      = document.getElementById('password').value,
        checkPassword = document.getElementById('checkPassword').value,
        username      = document.getElementById('username').value,
        mobile        = document.getElementById('mobile').value,
        birthday      = document.getElementById('birthday').value,
        county        = document.getElementById('county').value,
        district      = document.getElementById('district').value,
        address       = document.getElementById('address').value,
        success       = true;
    
    if (email==="" || password==="" || checkPassword==="" || username==="" || mobile==="" || birthday==="" || county==="" || district==="" || address===""){
      success = false;
      alert( "註冊資料未填寫完成" );
      }

    if (success){ // 送出註冊
      delete this.state.checkPassword;
      fetch('http://localhost:3000/members/registered', {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(this.state),
        headers: new Headers({
          'Content-Type': 'application/json'
        })
      })
      .then(res => res.json())
      .then(data => {
        window.location.href="http://localhost:3001/login"
        alert(data.message)
      });
      // console.log(this.state)
    }            
  }

  render() {
    return (
      <React.Fragment>
        <Form className="container registeredCN">
          <Row>
            <Col md={1}></Col>
            <Col md={10}>
              <h2 className="registeredFont">REGISTER</h2>
              <br />
              <h6>* 為必填資料</h6>
              <br />
              <Row>
                <Col md={5}>
                <FormGroup>
                  <h6 htmlFor="email">會員帳號(E-mail) *</h6>
                  <Input type="email" name="email" id="email" value={this.state.email} onChange={this.handlerChange} autoComplete="off" placeholder="請輸入常用信箱作為帳號" />
                  <Label id="email_Wrong" className="alert_Hidden">E-mail 格式不符, 請填寫正確格式</Label>
                </FormGroup>
              </Col>
              </Row>
              <Row>
                <Col md={5}>
                  <h6 htmlFor="password">會員密碼 *</h6>
                  <Input type="password" name="password" id="password" value={this.state.password} onChange={this.handlerChange} autoComplete="off" placeholder="請輸入6位至12位數之密碼" />
                  <Label id="password_Wrong" className="alert_Hidden">密碼長度過短</Label>
                  <Label id="checkPassword_Wrong" className="alert_Hidden">密碼輸入不一致</Label>
                </Col>
                <Col md={5}>
                  <h6 htmlFor="checkPassword">確認會員密碼 *</h6>
                  <Input type="password" name="checkPassword" id="checkPassword" value={this.state.checkPassword} onChange={this.handlerChange} autoComplete="off" placeholder="請輸入與會員密碼一致" />
                </Col>
              </Row>
              <hr />
              <Row>
                <Col md={5}>
                  <h6 htmlFor="username">姓名 *</h6>
                  <Input type="text" name="username" id="username" value={this.state.username} onChange={this.handlerChange} autoComplete="off" placeholder="請輸入您的姓名" />
                </Col>
              </Row>
              <br />
              <Row>
                <Col md={5}>
                  <h6 htmlFor="mobile">手機電話 *</h6>
                  <Input type="text" name="mobile" id="mobile" maxLength="10" value={this.state.mobile} onChange={this.handlerChange} autoComplete="off" placeholder="請輸入您的手機號碼" />
                  <Label id="mobile_Wrong" className="alert_Hidden">手機電話格式不符, 請填寫正確格式</Label>
                </Col>
                <Col md={5}>
                  <h6 htmlFor="birthday">生日 *</h6>
                  <Input type="date" name="birthday" id="birthday" value={this.state.birthday} onChange={this.handlerChange} autoComplete="off" placeholder="1999-11-11" />
                </Col>
              </Row>
              <br />

              <Row className="my-selector-c">
                <Col md={3}>
                  <FormGroup>
                    <h6 htmlFor="county">縣市 *</h6>
                    <select type="text" className="county form-control" name="county" id="county" value={this.state.county} onChange={this.handlerChange} ></select>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <FormGroup>
                    <h6 htmlFor="district">地區 *</h6>
                    <select type="text" className="district form-control" name="district" id="district" value={this.state.district} onChange={this.handlerChange} ></select>
                    <Label className="alert_Hidden check">請填寫資料</Label>
                  </FormGroup>
                </Col>
              </Row>

              <FormGroup>
                <h6 htmlFor="address">地址 *</h6>
                <Input type="text" name="address" id="address" value={this.state.address} onChange={this.handlerChange} autoComplete="off" placeholder="請輸入您的住址" />
              </FormGroup>
              <br />
            </Col>
          </Row>
          <Row>
            <Col md={1}/>
            <Col md={2}>
              <button type='button' className="registeredBtn checkOutBtn aui-js-response aui-button aui-button--primary aui-button--stretched" onClick={this.addHandler}>
                註冊
              </button>
            </Col>
          </Row>
        </Form>
        <br />
        <br />
      </React.Fragment>
    );
  }
}

export default Registered;