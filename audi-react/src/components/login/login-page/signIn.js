import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Col, Row } from 'reactstrap';
import { setCookie } from "../../../cookie";
import './singIn.scss';


class SignIn extends Component {
  constructor(props) {
    super(props)
    this.state = { // 預設值
      email        : "",
      password     : "",
      checkPassword: "",
      username     : "",
      birthday     : "",
      zip          : "",
      state        : "",
      city         : "",
      address      : "",
      url          : document.referrer
    }
    console.log(this);
    console.log(this.state.url);
  }

  handlerChange = (evt) => { // 輸入的值產生JSON 格式
    let key  = evt.target.id,
        data = evt.target.value;
    this.setState({
      [key]:data
    })
    // console.log(this.state)
  }

  logInHandler = evt => {
    evt.preventDefault()
    var email             = this.state.email,
        password          = this.state.password,
        emailFail         = document.getElementById('emailFail'),
        emailFunctionFail = document.getElementById('emailFunctionFail'),
        passwordFail      = document.getElementById('passwordFail'),
        passwordWordWrong = document.getElementById('passwordWordWrong'),
        emailFunctionI    = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
        emailFunctionII   = emailFunctionI.test(email),
        success           = true;
    
    if (email === ""){
      success = false;
      emailFail.style.display = 'block';
      emailFunctionFail.style.display = 'none';
      }else if (emailFunctionII === false){
        success = false;
        emailFunctionFail.style.display = 'block';
        emailFail.style.display = 'none';
        }else {
          emailFail.style.display = 'none';
          emailFunctionFail.style.display = 'none';
          };
    if(password === ""){
      success = false;
      passwordFail.style.display = 'block';
      passwordWordWrong.style.display = 'none';
      }else if(password.length < 6){
        success = false;
        passwordWordWrong.style.display = 'block';
        passwordFail.style.display = 'none';
        }else {
          passwordWordWrong.style.display = 'none';
          passwordFail.style.display = 'none';
          };
    if (success){
      fetch('http://localhost:3000/members/signIn', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify(this.state),
        headers: new Headers({
          'Content-Type': 'application/json'
        })
      })
      .then(res => res.json())
      .then(data => {
        alert(data.message);
        
        if(data.success){

          if(this.state.url === window.location.href || this.state.url ==="http://localhost:3001/login/registered"){ //這個是寫死的 需再改
            setCookie('isLogin', true)
            window.location.href="/membercenter/inbox"
          }else{
            window.location.href= this.state.url
          }
          
        }
      })
    }
  }
  
  render() {
    return (
      <React.Fragment>
        <h2 className="singInFont">SIGN IN</h2>
        <h6>登入會員即可快速結帳</h6>
        <Form className="signForm" name="form01" >
          <FormGroup>
            <h6 htmlFor="email">會員帳號(E-mail)</h6>
            <Input type="email" name="email" id="email"  value={this.state.email} onChange={this.handlerChange} placeholder="請輸入E-mail" />
            <Label id="emailFail" style={{color:'red', display:'none'}}>請輸入您註冊的 E-mail</Label>
            <Label id="emailFunctionFail" style={{color:'red', display:'none'}}>E-mail 格式不符, 請填寫正確格式</Label>
          </FormGroup>
          <FormGroup>
            <h6 htmlFor="password">會員密碼</h6>
            <Input type="password" name="password" id="password"  value={this.state.password} onChange={this.handlerChange} placeholder="請輸入密碼" />
            <Label id="passwordFail" style={{color:'red', display:'none'}}>請輸入您註冊的密碼</Label>
            <Label id="passwordWordWrong" style={{color:'red', display:'none'}}>請輸入您註冊的6-12位密碼</Label>
          </FormGroup>
          <Row>
            <Col md={6}>
              <FormGroup check>
                <h6 check>
                  <Input type="checkbox" />{' '}
                  記住帳號
                </h6>
              </FormGroup>
            </Col>
            <Col md={6}>
              <h6>
                <Link to="/login/forgot" className="forgot">
                  忘記密碼
                </Link>  
              </h6>
            </Col>
          </Row>
          <br />
          <Row>
            <Col>
              <button type='submit' className="signInBtn checkOutBtn aui-js-response aui-button aui-button--primary aui-button--stretched" onClick={this.logInHandler}>
                SIGN IN
              </button>
            </Col>
          </Row>
        </Form>
      </React.Fragment>  
    );
  }
}

export default SignIn;