import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Label } from 'reactstrap';

import './register.scss';

class Register extends Component {
  render() {
    return (
      <div className="d-flex flex-column registerWrapper">
        <h2 className="registerFont">REGISTER</h2>
        <h6>若您還未註冊會員 <br />
            我們將會請您提供必需資訊以便輕鬆購物</h6>
        <Link to="/login/registered" className="d-inline-flex mt-auto registerBtn">
          <button type='button' className="registerBtn checkOutBtn aui-js-response aui-button aui-button--primary aui-button--stretched" id="registered">
              創建使用者帳戶
          </button>
        </Link>
      </div>  
    );
  }
}

export default Register;