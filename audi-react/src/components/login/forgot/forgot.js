import React, { Component } from 'react';
import { Input, Col, Row } from 'reactstrap';

import './forgot.scss'

class Forgot extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="container forgotCN">
          <Row>
            <Col md={3}></Col>
            <Col md={6}>
              <h2 className="forgotFont">FORGOT PASSWORD</h2>
              <br />
              <Row>
                <Col md={2}></Col>
                <Col md={8}>
                  <p>忘記密碼 <br />
                  請輸入您的會員帳號(E-mail)以接收密碼</p>
                  <Input type="email" name="email" placeholder="" />
                  <br />
                  <button type="submit" className="forgotButton checkOutBtn aui-js-response aui-button aui-button--primary aui-button--stretched">
                    送出
                  </button>
                </Col>
              </Row>
            </Col>
          </Row> 
        </div>  
      </React.Fragment>
    );
  }
}

export default Forgot;