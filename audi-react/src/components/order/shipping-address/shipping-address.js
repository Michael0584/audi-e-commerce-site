import "./shipping-address.scss";
import { connect } from "../../../store";
import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import axios from "axios";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import { black } from "material-ui/styles/colors";
import Dialog from "material-ui/Dialog";
import TextField from "material-ui/TextField";
import TwCitySelector from "tw-city-selector";

class CitySelectorForBilling extends Component {
	//documentation: https://github.com/dennykuo/tw-city-selector/issues/14

	componentDidMount() {
		new TwCitySelector({
			el         : ".my-selector-c",
			elCounty   : ".county",
			elDistrict : ".district"
		});
	}

	//pass data to parent component
	setBillingCounty(e) {
		let selectedCounty = e.target.value;
		this.props.passBillingCountyData(selectedCounty);
	}

	//pass data to parent component
	setBillingDistrict(e) {
		let selectedDistrict = e.target.value;
		this.props.passBillingDistrictData(selectedDistrict);
	}

	render() {
		return (
			<Container className='my-selector-c'>
				<Row>
					<Col xs='5' className='aui-select aui-select--floating-label' id='county'>
						<select className='aui-select__input county' onChange={(e) => this.setBillingCounty(e)} />
					</Col>
					<Col xs='1' />
					<Col xs='5' className='aui-select aui-select--floating-label' id='district'>
						<select className='aui-select__input district' onChange={(e) => this.setBillingDistrict(e)} />
					</Col>
				</Row>
			</Container>
		);
	}
}
class CitySelectorForShipping extends Component {
	//documentation: https://github.com/dennykuo/tw-city-selector/issues/14

	componentDidMount() {
		new TwCitySelector({
			el         : ".my-selector-c2",
			elCounty   : ".county2",
			elDistrict : ".district2"
		});
	}

	//pass data to parent component
	setShippingCounty(e) {
		let selectedCounty = e.target.value;
		this.props.passShippingCountyData(selectedCounty);
	}

	//pass data to parent component
	setShippingDistrict(e) {
		let selectedDistrict = e.target.value;
		this.props.passShippingDistrictData(selectedDistrict);
	}

	render() {
		return (
			<Container className='my-selector-c2'>
				<Row>
					<Col xs='5' className='aui-select aui-select--floating-label' id='county'>
						<select className='aui-select__input county2' onChange={(e) => this.setShippingCounty(e)} />
					</Col>
					<Col xs='1' />
					<Col xs='5' className='aui-select aui-select--floating-label' id='district'>
						<select className='aui-select__input district2' onChange={(e) => this.setShippingDistrict(e)} />
					</Col>
				</Row>
			</Container>
		);
	}
}

// https://v0.material-ui.com/#/customization/themes
const muiTheme = getMuiTheme({
	palette : {
		primary1Color : black,
		primary2Color : black,
		primary3Color : black,
		accent1Color  : black,
		accent2Color  : black,
		accent3Color  : black,
		borderColor   : black,
		disabledColor : "#616161"
	}
});
class ShippingAddress extends Component {
	constructor(props) {
		super(props);
		this.state = {
			confirmationModalOpen : false,
			memberID              : this.props.profile.id,
			email                 : this.props.profile.email,
			validBillingPhone     : true,
			validShippingPhone    : true,
			billingAddress        : {
				name     : this.props.profile.username,
				zip      : this.props.profile.zip,
				county   : this.props.profile.county,
				district : this.props.profile.district,
				address  : this.props.profile.address,
				phone    : this.props.profile.mobile
			},
			shippingAddress       : {
				name     : this.props.profile.username,
				zip      : this.props.profile.zip,
				county   : this.props.profile.county,
				district : this.props.profile.district,
				address  : this.props.profile.address,
				phone    : this.props.profile.mobile
			},
			newBillingAddress     : {
				name     : this.props.profile.username,
				zip      : this.props.profile.zip,
				county   : this.props.profile.county,
				district : this.props.profile.district,
				address  : this.props.profile.address,
				phone    : this.props.profile.mobile
			},
			checked               : true
		};
	}

	// get member default info
	async componentWillMount() {
		const API_MEMBER_EMAIL = `http://localhost:3000/members/member/${this.props.profile.email}`;
		const response = await fetch(API_MEMBER_EMAIL);
		const member = await response.json();
		member.map((memberInfo) =>
			this.setState(() => ({
				memberID        : memberInfo.id,
				email           : memberInfo.email,
				billingAddress  : {
					name     : memberInfo.username,
					zip      : memberInfo.zip,
					county   : memberInfo.county,
					district : memberInfo.district,
					address  : memberInfo.address,
					phone    : memberInfo.mobile
				},
				shippingAddress : {
					name     : memberInfo.username,
					zip      : memberInfo.zip,
					county   : memberInfo.county,
					district : memberInfo.district,
					address  : memberInfo.address,
					phone    : memberInfo.mobile
				}
			}))
		);
	}

	// Get member info
	componentDidUpdate(prevProps, prevState) {
		// get member info（email） from session,
		// and then in turn get the member's id from it:
		if (prevState.email !== this.props.profile.email) {
			const API_MEMBER_EMAIL = `http://localhost:3000/members/member/${this.props.profile.email}`;
			axios.get(`${API_MEMBER_EMAIL}`).then((response) => {
				response.data.map((memberInfo) =>
					this.setState(() => ({
						memberID           : memberInfo.id,
						email              : memberInfo.email,
						validBillingPhone  : true,
						validShippingPhone : true,
						billingAddress     : {
							name     : memberInfo.username,
							zip      : memberInfo.zip,
							county   : memberInfo.county,
							district : memberInfo.district,
							address  : memberInfo.address,
							phone    : memberInfo.mobile
						},
						shippingAddress    : {
							name     : memberInfo.username,
							zip      : memberInfo.zip,
							county   : memberInfo.county,
							district : memberInfo.district,
							address  : memberInfo.address,
							phone    : memberInfo.mobile
						}
					}))
				);
				// console.log(response.data);
			});
		}
		// console.log("component did update");
	}

	componentDidMount() {
		this.props.onRef(this);
	}
	componentWillUnmount() {
		this.props.onRef(undefined);
	}

	passBillingCountyData = (selectedCounty) => {
		var newBillingAddress = { ...this.state.newBillingAddress };
		newBillingAddress.county = selectedCounty;
		this.setState({ newBillingAddress },()=>{console.log(this.state.newBillingAddress)});
	};

	passBillingDistrictData = (selectedDistrict) => {
		var newBillingAddress = { ...this.state.newBillingAddress };
		newBillingAddress.district = selectedDistrict;
		this.setState({ newBillingAddress },()=>{console.log(this.state.newBillingAddress)});
	};

	passShippingCountyData = (selectedCounty) => {
		var shippingAddress = { ...this.state.shippingAddress };
		shippingAddress.county = selectedCounty;
		this.setState({ shippingAddress });
	};

	passShippingDistrictData = (selectedDistrict) => {
		var shippingAddress = { ...this.state.shippingAddress };
		shippingAddress.district = selectedDistrict;
		this.setState({ shippingAddress });
	};

	cancelEdit = () => {
		// const API_MEMBER_EMAIL = `http://localhost:3000/members/member/${this.props.profile.email}`;
		// axios.get(`${API_MEMBER_EMAIL}`).then((response) => {
		// 	response.data.map((memberInfo) =>
		// 		this.setState(() => ({
		// 			billingAddress : {
		// 				name     : memberInfo.username,
		// 				zip      : memberInfo.zip,
		// 				county   : memberInfo.county,
		// 				district : memberInfo.district,
		// 				address  : memberInfo.address,
		// 				phone    : memberInfo.mobile
		// 			}
		// 		}))
		// 	);
		// });
		this.setState({ confirmationModalOpen: false });
    };
    

	validateBillingPhone(phoneNumber) {
		const regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
		return regex.test(phoneNumber)
			? this.setState({ validBillingPhone: true })
			: this.setState({ validBillingPhone: false });
	}

	validateShippingPhone(phoneNumber) {
		const regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
		return regex.test(phoneNumber)
			? this.setState({ validShippingPhone: true })
			: this.setState({ validShippingPhone: false });
	}

	handleCheck = () => {
		this.setState({ checked: !this.state.checked });
	};

	syncShippingWithBilling = () => {
		var shippingAddress = { ...this.state.shippingAddress };
		var billingAddress = { ...this.state.billingAddress };

		switch (this.state.checked === true) {
			case billingAddress.name !== shippingAddress.name:
				shippingAddress.name = this.state.billingAddress.name;
				this.setState({ shippingAddress });
				break;
			case billingAddress.county !== shippingAddress.county:
				shippingAddress.county = this.state.billingAddress.county;
				this.setState({ shippingAddress });
				break;
			case billingAddress.district !== shippingAddress.district:
				shippingAddress.district = this.state.billingAddress.district;
				this.setState({ shippingAddress });
				break;
			case billingAddress.address !== shippingAddress.address:
				shippingAddress.address = this.state.billingAddress.address;
				this.setState({ shippingAddress });
				break;
			case billingAddress.phone !== shippingAddress.phone:
				shippingAddress.phone = this.state.billingAddress.phone;
				this.setState({ shippingAddress });
				break;
			default:
				return;
		}
	};

	renderShippingAddressFields = () => {
		if (this.state.checked === false) {
			return (
				<MuiThemeProvider muiTheme={muiTheme}>
					<Container fluid={true}>
						<Row>
							<Col>
								<TextField
									style={{ display: "block" }}
									name='name'
									hintText='姓名'
									floatingLabelText='姓名'
									onChange={(evt, newValue) => {
										var shippingAddress = { ...this.state.shippingAddress };
										shippingAddress.name = newValue;
										this.setState({ shippingAddress });
									}}
									// defaultValue={this.state.shippingAddress.name}
									// this.setState({ billingAddress: { name: newValue } })}
								/>
							</Col>
						</Row>
						<Row>
							<Col>
								<CitySelectorForShipping
									passShippingCountyData={this.passShippingCountyData}
									passShippingDistrictData={this.passShippingDistrictData}
								/>
							</Col>
						</Row>
						<Row>
							<Col>
								<TextField
									style={{ display: "block" }}
									name='address'
									hintText='地址'
									floatingLabelText='地址'
									onChange={(evt, newValue) => {
										var shippingAddress = { ...this.state.shippingAddress };
										shippingAddress.address = newValue;
										this.setState({ shippingAddress });
									}}
									// defaultValue={this.state.shippingAddress.address}
									// onChange={(evt, newValue) => this.setState({ billingAddress: { address: newValue } })}
								/>
							</Col>
							<Col>
								<TextField
									style={{ display: "block" }}
									name='phone'
									hintText='電話'
									floatingLabelText='電話'
									errorText={this.state.validShippingPhone ? null : "請填寫正確格式的電話"}
									onChange={(evt, newValue) => {
										this.validateShippingPhone(newValue);
										var shippingAddress = { ...this.state.shippingAddress };
										shippingAddress.phone = newValue;
										this.setState({ shippingAddress });
									}}
									// defaultValue={this.state.shippingAddress.phone}
									// onChange={(evt, newValue) => this.setState({ billingAddress: { phone: newValue } })}
								/>
							</Col>
						</Row>
					</Container>
				</MuiThemeProvider>
			);
		} else {
			this.syncShippingWithBilling();
		}
	};

	collectAddress = () => {
		let billingAddress = { ...this.state.billingAddress };
		let shippingAddress = { ...this.state.shippingAddress };
		let orderAddress = {
			billingAddress  : billingAddress,
			shippingAddress : shippingAddress
		};
		localStorage.setItem("orderAddress", JSON.stringify(orderAddress));
	};

	editBillingAddress = () => {
		this.setState({ billingAddress: this.state.newBillingAddress }, ()=>{
            this.setState({ confirmationModalOpen: false });
        });
	};

	render() {
		const { confirmationModalOpen } = this.state;
		const modalActions = [
			<div class='aui-button-group'>
				<button
					class='aui-js-response aui-button aui-button--primary'
					type='button'
					onClick={() => this.editBillingAddress()}
				>
					修改地址
				</button>
				<button
					class='aui-js-response aui-button aui-button--secondary'
					type='button'
					onClick={() => this.cancelEdit()}
				>
					取消
				</button>
			</div>
		];
		return (
			<Container fluid={true}>
				<Row>
					<Col xs='6' className='leftCol billingAddress'>
						<h1>收據地址</h1>
						<div className='memberInfo'>
							<p>姓名：{this.state.billingAddress.name}</p>
							<p>城市：{this.state.billingAddress.county}</p>
							<p>區：{this.state.billingAddress.district}</p>
							<p>地址：{this.state.billingAddress.address}</p>
							<p>電話：{this.state.billingAddress.phone}</p>
						</div>
						<br />
						<button
							class='aui-button aui-button--secondary aui-js-response aui-button--stretched'
							onClick={() =>
								this.setState({
									confirmationModalOpen : !this.state.confirmationModalOpen
								})}
							type='button'
						>
							編輯地址
						</button>
						<MuiThemeProvider muiTheme={muiTheme}>
							<Dialog modal={true} open={confirmationModalOpen} actions={modalActions} title='修改收據地址'>
								<TextField
									style={{ display: "block" }}
									name='name'
									hintText='姓名'
									floatingLabelText='姓名'
									onChange={(evt, newValue) => {
										var newBillingAddress = { ...this.state.newBillingAddress };
										newBillingAddress.name = newValue;
										this.setState({ newBillingAddress });
									}}
									// this.setState({ billingAddress: { name: newValue } })}
								/>
								<CitySelectorForBilling
									passBillingCountyData={this.passBillingCountyData}
									passBillingDistrictData={this.passBillingDistrictData}
								/>
								<TextField
									style={{ display: "block" }}
									name='address'
									hintText='地址'
									floatingLabelText='地址'
									onChange={(evt, newValue) => {
										var newBillingAddress = { ...this.state.newBillingAddress };
										newBillingAddress.address = newValue;
										this.setState({ newBillingAddress });
									}}
									// onChange={(evt, newValue) => this.setState({ billingAddress: { address: newValue } })}
								/>
								<TextField
									style={{ display: "block" }}
									name='phone'
									hintText='電話'
									errorText={this.state.validBillingPhone ? null : "請填寫正確格式的電話"}
									onChange={(evt, newValue) => {
										this.validateBillingPhone(newValue);
										var newBillingAddress = { ...this.state.newBillingAddress };
										newBillingAddress.phone = newValue;
										this.setState({ newBillingAddress });
									}}
									// onChange={(evt, newValue) => this.setState({ billingAddress: { phone: newValue } })}
								/>
							</Dialog>
						</MuiThemeProvider>
					</Col>
					<Col xs='6' className='rightCol shippingAddress'>
						<h1>寄送地址</h1>
						<label class='pure-material-checkbox'>
							<input type='checkbox' onChange={this.handleCheck} defaultChecked={this.state.checked} />
							<span>和收據地址相同</span>
						</label>
						{this.renderShippingAddressFields()}
					</Col>
				</Row>
			</Container>
		);
	}
}

export default connect(ShippingAddress);
