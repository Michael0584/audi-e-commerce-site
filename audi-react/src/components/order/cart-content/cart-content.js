import "./cart-content.scss";
import React, { Component, forwardRef, useRef, useImperativeMethods } from "react";
// https://stackoverflow.com/questions/37949981/call-child-method-from-parent
// https://github.com/kriasoft/react-starter-kit/issues/909#issuecomment-252969542
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import { Container, Col, Row } from "reactstrap";
import { connect } from "../../../store";
import axios from "axios";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import { black } from "material-ui/styles/colors";
import TextField from "material-ui/TextField";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import ShippingAddress from "../shipping-address/shipping-address";
import InstallationReservation from "../installation-reservation/installation-reservation";
import AppointmentApp from "../installation-reservation/AppointmentApp";
import moment from "moment";

const muiTheme = getMuiTheme({
	palette : {
		primary1Color : black,
		primary2Color : black,
		primary3Color : black,
		accent1Color  : black,
		accent2Color  : black,
		accent3Color  : black,
		borderColor   : black,
		disabledColor : "#616161"
	}
});

const CCMonths = [
	<MenuItem key={1} value={1} primaryText='01' />,
	<MenuItem key={2} value={2} primaryText='02' />,
	<MenuItem key={3} value={3} primaryText='03' />,
	<MenuItem key={4} value={4} primaryText='04' />,
	<MenuItem key={5} value={5} primaryText='05' />,
	<MenuItem key={6} value={6} primaryText='06' />,
	<MenuItem key={7} value={7} primaryText='07' />,
	<MenuItem key={8} value={8} primaryText='08' />,
	<MenuItem key={9} value={9} primaryText='09' />,
	<MenuItem key={10} value={10} primaryText='10' />,
	<MenuItem key={11} value={11} primaryText='11' />,
	<MenuItem key={12} value={12} primaryText='12' />
];

const CCYears = [
	<MenuItem key={1} value={2019} primaryText='2019' />,
	<MenuItem key={2} value={2020} primaryText='2020' />,
	<MenuItem key={3} value={2021} primaryText='2021' />,
	<MenuItem key={4} value={2022} primaryText='2022' />,
	<MenuItem key={5} value={2023} primaryText='2023' />,
	<MenuItem key={6} value={2024} primaryText='2024' />,
	<MenuItem key={7} value={2025} primaryText='2025' />,
	<MenuItem key={8} value={2026} primaryText='2026' />,
	<MenuItem key={9} value={2027} primaryText='2027' />,
	<MenuItem key={10} value={2028} primaryText='2028' />,
	<MenuItem key={11} value={2029} primaryText='2029' />,
	<MenuItem key={12} value={2029} primaryText='2030' />
];
class OrderPage extends Component {
	constructor(props) {
		super(props);

		this.numberWithCommas = (number) => {
			return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		};

		this.totalPrice = () => {
			let sessionShoppingCart = JSON.parse(localStorage.getItem("cart"));
			let totalPrice = 0;
			sessionShoppingCart.forEach((item) => {
				totalPrice += item.quantity * item.product_price;
				// console.log(totalPrice);
				this.setState({
					total : totalPrice
				});
			});
		};
		this.state = {
			memberID             : "",
			memberEmail          : "",
			shoppingCart         : [],
			total                : 0,
			installationRequired : false,
			fieldIsEmpty         : {
				cardHolderName   : true,
				cardNumber       : true,
				cardSecurityCode : true
			},
			CreditCardHolderName : "持卡人姓名",
			CreditCardNumber     : "卡號",
			CreditCardCWW        : "CWW2",
			validCCNumber        : false,
			validCWW             : false,
			selectedCCMonth      : null,
			selectedCCYear       : null,
			orderID              : "",
			allFieldsFilled      : false
		};

		// console.log(this);

		this.setData = () => {
			let shoppingCart = this.state.shoppingCart;
			let sessionCart = localStorage.setItem("cart", JSON.stringify(shoppingCart));
		};
	}

	componentDidMount() {
		if (localStorage.getItem("cart")) {
			let shoppingCartContent = JSON.parse(localStorage.getItem("cart"));
			let totalPrice = 0;
			shoppingCartContent.forEach((item) => {
				totalPrice += item.quantity * item.product_price;
				// console.log(totalPrice);
				this.setState({
					total : totalPrice
				});
			});

			//檢查購物車内是否有需要預約安裝的商品:
			shoppingCartContent.forEach((item) => {
				if (item.installation_required === 1) {
					this.setState(() => ({
						installationRequired : true
					}));
				}
			});

			//將session購物車内容放進state:
			this.setState({
				shoppingCart : shoppingCartContent
			});
			// console.log(shoppingCartContent);
		}
	}

	installationRequiredCheck(item) {
		if (item !== 0) {
			return "是";
		} else {
			return "否";
		}
	}

	renderInstallationAppointment = () => {
		if (this.state.installationRequired === true) {
			return (
				<div>
					<InstallationReservation onRef={(ref) => (this.reservation = ref)} orderID={this.state.orderID} />
					<hr className='divisionLinesMargin' />
				</div>
			);
		}
	};

	verifyCCNumber = (event) => {
		let regExp = /^((67\d{2})|(4\d{3})|(5[1-5]\d{2})|(6011))-?\s?\d{4}-?\s?\d{4}-?\s?\d{4}|3[4,7]\d{13}$/;
		if (!regExp.test(event.target.value)) {
			this.setState({
				validCCNumber : true
			});
		} else {
			this.setState({
				validCCNumber : false
			});
		}
	};

	changeMonth = (event, index, value) => this.setState({ selectedCCMonth: value });

	changeYear = (event, index, value) => this.setState({ selectedCCYear: value });

	checkIfEmpty = (e) => {
		let field = e.target.name;
		let fieldIsEmpty = { ...this.state.fieldIsEmpty };
		if (e.target.value === "") {
			fieldIsEmpty[field] = false;
			this.setState({
				fieldIsEmpty    : fieldIsEmpty,
				allFieldsFilled : false
			});
		}
		if (e.target.value !== "") {
			fieldIsEmpty[field] = true;
			this.setState({
				fieldIsEmpty    : fieldIsEmpty,
				allFieldsFilled : true
			});
		}
	};

	submitMemberIdentification = async () => {
		const API_MEMBER_EMAIL = `http://localhost:3000/members/member/${this.props.profile.email}`;
		const getMemberInfo = await fetch(API_MEMBER_EMAIL);
		const results = await getMemberInfo.json();
		results.map((memberInfo) =>
			this.setState(
				() => ({
					memberEmail : memberInfo.email,
					memberID    : memberInfo.id
				}),
				() => {
					localStorage.setItem("memberID", JSON.stringify(this.state.memberID));
					localStorage.setItem("memberEmail", JSON.stringify(this.state.memberEmail));
					let memberID = JSON.parse(localStorage.getItem("memberID"));
					let memberEmail = JSON.parse(localStorage.getItem("memberEmail"));
					console.log(this.state.memberID);
					console.log(this.state.memberEmail);
					console.log(memberID);
					console.log(memberEmail);
				}
			)
		);
	};

	submitCart = () => {
		let shoppingCartContent = JSON.parse(localStorage.getItem("cart"));
		let storeTotalPrice = localStorage.setItem("totalPrice", JSON.stringify(this.state.total));
		let totalPrice = JSON.parse(localStorage.getItem("totalPrice"));
		console.log(shoppingCartContent);
		console.log(totalPrice);
	};

	submitAddress = () => {
		this.address.collectAddress();
		const orderAddress = JSON.parse(localStorage.getItem("orderAddress")); //get reservation data in local storage
		console.log(orderAddress);
	};

	submitCCInfo = () => {
		const collectPaymentInfo = {
			CreditCardHolderName : this.state.CreditCardHolderName,
			CreditCardNumber     : this.state.CreditCardNumber,
			CreditCardCWW        : this.state.CreditCardCWW,
			selectedCCMonth      : this.state.selectedCCMonth,
			selectedCCYear       : this.state.selectedCCYear,
			transactionTime      : moment().format("YYYY/MM/DD HH:mm")
		};
		let setPaymentInfo = localStorage.setItem("paymentInfo", JSON.stringify(collectPaymentInfo));
		let paymentInfo = JSON.parse(localStorage.getItem("paymentInfo"));
		console.log(paymentInfo);
	};

	generateOrderID = async () => {
		const orderID = Math.random().toString(36).substr(2, 9).toUpperCase();
		await this.setState({ orderID }, () => {
			localStorage.setItem("orderID", JSON.stringify(this.state.orderID));
			const orderID = JSON.parse(localStorage.getItem("orderID"));
			console.log(orderID);
		});
	};

	postOrder = async () => {
		const newOrder = {
			member_id                     : JSON.parse(localStorage.getItem("memberID")),
			member_email                  : JSON.parse(localStorage.getItem("memberEmail")),
			order_id                      : JSON.parse(localStorage.getItem("orderID")),
			order_address                 : localStorage.getItem("orderAddress"),
			cart                          : localStorage.getItem("cart"),
			installation_reservation_info : localStorage.getItem("reservationInfo"),
			payment_info                  : localStorage.getItem("paymentInfo"),
			total_price                   : JSON.parse(localStorage.getItem("totalPrice"))
		};
		if (!this.state.allFieldsFilled) {
			return alert("請填入信用卡資訊");
		}
		const API_BASE = `http://localhost:3000/order/submit_order`;
		await axios.post(API_BASE, newOrder).then((response) => {
			console.log("訂單送出!");
			//Redirect page to order page after everything is posted.});
			window.location.assign(`/order/${newOrder.order_id}`);
		});
		localStorage.clear();
		return localStorage.setItem("cart", JSON.stringify([]));
	};

	collectOrderData = async () => {
		await this.submitMemberIdentification(); //Member ID & Email
		await this.generateOrderID(); //Generate Order ID
		await this.submitCart(); //Cart Items
		await this.submitAddress(); //Billing & Shipping Address
		await this.submitCCInfo(); //payment Info
		if (this.state.installationRequired === true) {
			//Installation Reservation
			await this.reservation.reservationInfo(); //collect reservation data, which will be stored in local storage
			await this.reservation.submitReservation(); //sends out installation reservation, POST into database
		}
		return;
	};

	//IMPORTANT! Use this to send out ALL data and into the order receipt page.
	submitOrder = async () => {
		await this.collectOrderData();
		if (this.state.installationRequired === true) {
			if (this.reservation.reservationState() === true && this.state.allFieldsFilled === true) {
				await this.postOrder(); //Post everything to DB
			}
		} else {
			await this.postOrder(); //Post everything to DB
		}
		return;
	};

	render() {
		if (localStorage.getItem("cart") !== "[]") {
			return (
				<Container fluid={true} className='containerModifier'>
					<Row>
						<Col>
							<h1 className='headline'>訂單明細</h1>
						</Col>
					</Row>
					<Row className='cartContent'>
						<Col>
							<div className='aui-table aui-table--stretched'>
								<table>
									<thead className='cartContentTitle'>
										<tr>
											<th>商品</th>
											<th className='text-center'>顔色/材質</th>
											<th className='text-center'>大小</th>
											<th className='text-center'>數量</th>
											<th className='text-center'>金額</th>
											<th className='text-center'>需預約安裝</th>
										</tr>
									</thead>
									{this.state.shoppingCart.map((item, i) => (
										<tr key={i} className='cartItems cartItem00'>
											<td className='product'>
												<img
													src={"/img/" + item.product_img.listingImg}
													className='productImage'
													alt=''
												/>
												<p className='productName'>{item.product_name}</p>
											</td>
											<td className='color'>
												<p className='colorText text-center'>{item.color}</p>
											</td>
											<td className='size'>
												<p className='sizeText text-center'>{item.size}</p>
											</td>
											<td className='quantity'>
												<div className='flexWrap'>
													<input
														className='aui-textfield__input quantityInputNumber'
														type='number'
														value={item.quantity}
														min='0'
														step='1'
														disabled
													/>
												</div>
											</td>
											<td className='price'>
												<p className='priceText text-center'>
													${this.numberWithCommas(item.product_price * item.quantity)}
												</p>
											</td>
											<td className='installation'>
												<p className='installationText text-center'>
													{this.installationRequiredCheck(item.installation_required)}{" "}
												</p>
											</td>
										</tr>
									))}
								</table>
							</div>
						</Col>
					</Row>
					<Row>
						<Col>
							<hr className='divisionLinesMarginBottom' />
							<ShippingAddress onRef={(ref) => (this.address = ref)} />
							<hr className='divisionLinesMargin' />
						</Col>
					</Row>
					<Row>
						<Col>{this.renderInstallationAppointment()}</Col>
					</Row>
					<Row>
						<Col>
							<Container fluid={true}>
								<Row>
									<Col xs='6' className='paymentInformation'>
										<Row>
											<Col>
												<h2>信用卡資訊</h2>
												<MuiThemeProvider muiTheme={muiTheme} className='creditCardWrapper'>
													<Row>
														<Col>
															<TextField
																style={{ display: "block" }}
																className='cardHolderName'
																name='cardHolderName'
																hintText='持卡人姓名'
																floatingLabelText='持卡人姓名'
																errorText={
																	this.state.fieldIsEmpty.cardHolderName ? null : (
																		"請填寫持卡人姓名"
																	)
																}
																onChange={(e, newValue) => {
																	this.checkIfEmpty(e);
																	this.setState({ CreditCardHolderName: newValue });
																}}
																defaultValue={this.state.CreditCardHolderName}
																onFocus={(e) =>
																	e.target.value == "持卡人姓名"
																		? (e.target.value = "")
																		: this.checkIfEmpty(e)} //clear field on focus if input === default value
																onBlur={(e) => this.checkIfEmpty(e)} //check if field is blank when out of focus
															/>
														</Col>
														<Col>
															<TextField
																style={{ display: "block" }}
																className='cardNumber'
																name='cardNumber'
																hintText='卡號'
																floatingLabelText='卡號'
																maxLength='16'
																errorText={
																	this.state.fieldIsEmpty.cardNumber === true &&
																	this.state.validCCNumber === false ? null : (
																		"請填寫正確格式的信用卡號碼"
																	)
																}
																onChange={(e, newValue) => {
																	this.checkIfEmpty(e);
																	this.setState({ CreditCardNumber: newValue });
																	this.verifyCCNumber(e);
																}}
																defaultValue={this.state.CreditCardNumber}
																onFocus={(e) =>
																	e.target.value == "卡號"
																		? (e.target.value = "")
																		: this.checkIfEmpty(e)}
																onBlur={(e) => this.checkIfEmpty(e)}
															/>
														</Col>
													</Row>
													<Row>
														<Col>
															<TextField
																style={{ display: "block" }}
																className='cardSecurityCode'
																name='cardSecurityCode'
																hintText='CWW2 安全碼'
																floatingLabelText='CWW2 安全碼'
																maxLength='3'
																errorText={
																	this.state.fieldIsEmpty.cardSecurityCode ? null : (
																		"請填寫CWW2安全碼(卡片背面後3碼)"
																	)
																}
																onChange={(e, newValue) => {
																	this.checkIfEmpty(e);
																	this.setState({ CreditCardCWW: newValue });
																	// this.verifyCWWNumber(e);
																}}
																defaultValue={this.state.CreditCardCWW}
																onFocus={(e) =>
																	e.target.value == "CWW2"
																		? (e.target.value = "")
																		: this.checkIfEmpty(e)} //clear field on focus if input === default value
																onBlur={(e) => this.checkIfEmpty(e)} //check if field is blank when out of focus
															/>
														</Col>
														<Col>
															<SelectField
																style={{
																	display     : "inline-block",
																	width       : "25%",
																	marginRight : "20px"
																}}
																value={
																	this.state.selectedCCMonth == null ? (
																		1
																	) : (
																		this.state.selectedCCMonth
																	)
																}
																onChange={this.changeMonth}
																floatingLabelText='月'
															>
																{CCMonths}
															</SelectField>
															<SelectField
																style={{
																	display : "inline-block",
																	width   : "50%"
																}}
																value={
																	this.state.selectedCCYear == null ? (
																		2019
																	) : (
																		this.state.selectedCCYear
																	)
																}
																onChange={this.changeYear}
																floatingLabelText='年'
															>
																{CCYears}
															</SelectField>
														</Col>
													</Row>
												</MuiThemeProvider>
											</Col>
										</Row>
									</Col>

									<Col xs='6' className='basketTotal'>
										<Row>
											<Col>
												<h2>訂單金額</h2>
												<hr className='marginTop' />
												<div className='flexWrap'>
													<p className='title'>商品金額:</p>
													<p className='price'>${this.numberWithCommas(this.state.total)}</p>
												</div>
												<div className='flexWrap'>
													<p className='title'>預計寄送費用:</p>
													<p className='price'>${this.numberWithCommas(0)}</p>
												</div>
												{/* <div className='flexWrap'>
													<p className='title'>折扣:</p>
													<p className='price'>-${this.numberWithCommas(0)}</p>
												</div> */}
												<div className='flexWrap total'>
													<p className='title bold'>總金額:</p>
													<p className='price bold'>
														${this.numberWithCommas(this.state.total)}
													</p>
												</div>
												<hr />
											</Col>
										</Row>
										<Row>
											<Col xs='12' className='checkOutOptions'>
												<div className='aui-button-group'>
													<Link
														to='#'
														className='orderBtn aui-js-response aui-button aui-button--primary'
														onClick={this.submitOrder}
													>
														送出訂單
													</Link>

													<Link
														to='/shopping-cart'
														className='backToCartBtn aui-js-response aui-button aui-button--secondary'
													>
														回到購物車
													</Link>
												</div>
											</Col>
										</Row>
									</Col>
								</Row>
							</Container>
						</Col>
					</Row>
				</Container>
			);
		}
	}
}

export default connect(OrderPage);
