// Documentation: https://medium.com/reactninja/building-appointment-scheduler-app-in-react-and-nodejs-f163c4eaab6b

import "../../brand-colors.scss";
import React, { Component, forwardRef, useRef, useImperativeMethods } from "react";
import RaisedButton from "material-ui/RaisedButton";
import FlatButton from "material-ui/FlatButton";
import moment from "moment";
import DatePicker from "material-ui/DatePicker";
import Dialog from "material-ui/Dialog";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import TextField from "material-ui/TextField";
import SnackBar from "material-ui/Snackbar";
import Card from "material-ui/Card";
import { Step, Stepper, StepLabel, StepContent } from "material-ui/Stepper";
import { RadioButton, RadioButtonGroup } from "material-ui/RadioButton";
import { connect } from "../../../store";
import axios from "axios";

const API_BASE = "http://localhost:3000/installation_locations/";

// to do:
// 生產/撈訂單ID

class AppointmentApp extends Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			name                    : "",
			email                   : "",
			schedule                : [],
			confirmationModalOpen   : false,
			appointmentDateSelected : false,
			appointmentMeridiem     : 0,
			validEmail              : true,
			validPhone              : true,
			finished                : false,
			smallScreen             : window.innerWidth < 768,
			stepIndex               : 0,
			selectedLocation        : this.props.selectedLocation,
			selectedLocationName    : "",
			memberEmail             : "",
			memberID                : "",
			orderID                 : Math.random().toString(36).substr(2, 9).toUpperCase(),
			phone                   : "",
			allFieldsFilled         : false
		};
	}

	// async componentWillMount() {
	// 	const response = await fetch(`${API_BASE}reservations`);
	//     const timeSlots = await response.json();
	//     console.log("response via db: ", timeSlots);
	//     // this.handleDBResponse(timeSlots);
	// }

	// fetch DB data for pre-existing appointment timeslots
	componentWillMount() {
		axios.get(`${API_BASE}reservations/${this.props.selectedLocation}`).then((response) => {
			// console.log("response via db (appointment timeslots): ", response.data);
			this.handleDBResponse(response.data);
		});
		//location name of the selected location
		axios.get(`${API_BASE}locations/${this.props.selectedLocation}`).then((response) => {
			response.data.map((location) =>
				this.setState(() => ({
					selectedLocationName : location.location_name
				}))
			);
			// console.log(this.state.selectedLocationName);
		});

		// console.log(`Selected Location ID: ${this.props.selectedLocation}`); // test:get selected location passed down from other component
	}

	// necessary for onRef, allowing parent component to access methods within the child component:
	// https://github.com/kriasoft/react-starter-kit/issues/909#issuecomment-252969542
	componentDidMount() {
		this.props.onRef(this);
	}
	componentWillUnmount() {
		this.props.onRef(undefined);
	}

	// fetch new DB timeslot data based on location selected
	componentDidUpdate(prevProps, prevState) {
		if (this.state.selectedLocation !== this.props.selectedLocation) {
			//setState is asynch, so you have to use: this.setState(() => ({ state: newState }));
			this.setState(() => ({
				selectedLocation : this.props.selectedLocation
			}));

			//get reservation timeslots under the selected locations
			axios.get(`${API_BASE}reservations/${this.props.selectedLocation}`).then((response) => {
				// console.log("response via db: ", response.data);
				this.handleDBResponse(response.data);
			});

			//get location name of the selected location
			axios.get(`${API_BASE}locations/${this.props.selectedLocation}`).then((response) => {
				// console.log("response via db (appointment timeslots): ", response.data);
				response.data.map((location) =>
					this.setState(() => ({
						selectedLocationName : location.location_name
					}))
				);
				// console.log(this.state.selectedLocationName);
			});
		}
		// console.log(this.props.selectedLocation); // test: update selected location passed down from other component

		// get member info（email） from session,
		// and then in turn get the member's id from it:
		if (prevState.memberEmail !== this.props.profile.email) {
			const API_MEMBER_EMAIL = `http://localhost:3000/members/member/${this.props.profile.email}`;
			axios.get(`${API_MEMBER_EMAIL}`).then((response) => {
				response.data.map((memberInfo) =>
					this.setState(() => ({
						memberEmail : memberInfo.email,
						memberID    : memberInfo.id
					}))
				);
			});
			// console.log(this.state.memberEmail); //test if member email was successfully fetched and set as state
			// console.log(this.state.memberID); //test member id was successfully fetched and set as state
		}
	}

	// The checkDisableDate method passes disabled dates to the date picker component.
	checkDisableDate(day) {
		const dateString = moment(day).format("YYYY-MM-DD");
		return this.state.schedule[dateString] === true || moment(day).startOf("day").diff(moment().startOf("day")) < 0;
	}

	// The handleDBResponse method handle the appointment slot data from the database.
	handleDBResponse(response) {
		const appointments = response;
		const today = moment().startOf("day"); //start of today 12 am
		const initialSchedule = {};
		initialSchedule[today.format("YYYY-MM-DD")] = true;
		const schedule = !appointments.length
			? initialSchedule
			: appointments.reduce((currentSchedule, appointment) => {
					const { slot_date, slot_time } = appointment;
					const dateString = moment(slot_date, "YYYY-MM-DD").format("YYYY-MM-DD");
					if (!currentSchedule[slot_date]) {
						currentSchedule[dateString] = Array(8).fill(false);
					}
					if (Array.isArray(currentSchedule[dateString])) {
						currentSchedule[dateString][slot_time] = true;
					}
					return currentSchedule;
				}, initialSchedule);

		for (let day in schedule) {
			let slots = schedule[day];
			if (slots.length) {
				if (slots.every((slot) => slot === true)) {
					schedule[day] = true;
				}
			}
		}

		this.setState({
			schedule : schedule
		});
	}

	// The renderAppointmentTimes method renders available time slots to user and disables the rest if any.
	renderAppointmentTimes() {
		if (!this.state.isLoading) {
			const slots = [
				...Array(8).keys()
			];
			return slots.map((slot) => {
				const appointmentDateString = moment(this.state.appointmentDate).format("YYYY-MM-DD");
				const time1 = moment().hour(9).minute(0).add(slot, "hours");
				const time2 = moment().hour(9).minute(0).add(slot + 1, "hours");
				const scheduleDisabled = this.state.schedule[appointmentDateString]
					? this.state.schedule[moment(this.state.appointmentDate).format("YYYY-MM-DD")][slot]
					: false;
				const meridiemDisabled = this.state.appointmentMeridiem
					? time1.format("a") === "am"
					: time1.format("a") === "pm";
				return (
					<RadioButton
						label={time1.format("h:mm a") + " - " + time2.format("h:mm a")}
						key={slot}
						value={slot}
						style={{
							marginBottom : 15,
							display      : meridiemDisabled ? "none" : "inherit"
						}}
						disabled={scheduleDisabled || meridiemDisabled}
					/>
				);
			});
		} else {
			return null;
		}
	}

	//moves stepper to next position
	handleNext = () => {
		const { stepIndex } = this.state;
		this.setState({
			stepIndex : stepIndex + 1,
			finished  : stepIndex >= 2
		});
	};

	//moves stepper to previous position
	handlePrev = () => {
		const { stepIndex } = this.state;
		if (stepIndex > 0) {
			this.setState({ stepIndex: stepIndex - 1 });
		}
	};

	//The renderStepActions method renders the Next, finish and Back Buttons.
	renderStepActions(step) {
		const { stepIndex } = this.state;

		return (
			<div style={{ margin: "12px 0" }}>
				<RaisedButton
					label={stepIndex === 2 ? "完成" : "下一步"}
					disableTouchRipple={true}
					disableFocusRipple={true}
					primary={true}
					onClick={this.handleNext}
					backgroundColor='#00C853 !important'
					style={{ marginRight: 12, backgroundColor: "#00C853" }}
				/>
				{step > 0 && (
					<FlatButton
						label='上一步'
						disabled={stepIndex === 0}
						disableTouchRipple={true}
						disableFocusRipple={true}
						onClick={this.handlePrev}
					/>
				)}
			</div>
		);
	}

	//The validateEmail method checks the email parameter and return true if email value is valid or false if it is not.
	validateEmail(email) {
		const regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
		return regex.test(email)
			? this.setState({ email: email, validEmail: true })
			: this.setState({ validEmail: false });
	}

	//The validatePhone method checks the phone parameter and return true if phone value is valid or false if it is not.
	validatePhone(phoneNumber) {
		const regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
		return regex.test(phoneNumber)
			? this.setState({ phone: phoneNumber, validPhone: true })
			: this.setState({ validPhone: false });
	}

	// The handleSetAppointmentDate method is used to set the state of the appointmentDate field.
	handleSetAppointmentDate(date) {
		this.setState({ appointmentDate: date, confirmationTextVisible: true });
	}

	// The handleSetAppointmentSlot method is used to set the state of the appointmentSlot field.
	handleSetAppointmentSlot(slot) {
		this.setState({ appointmentSlot: slot });
	}

	// The handleSetAppointmentMeridiem method is used to set the state of the appointmentMeridiem field.
	handleSetAppointmentMeridiem(meridiem) {
		this.setState({ appointmentMeridiem: meridiem });
	}

	// The renderAppointmentConfirmation method display a modal with the user’s inputted information and asks the user to confirm, before saving to database.
	renderAppointmentConfirmation() {
		const spanStyle = { color: "#00C853" };
		return (
			<section>
				<p>
					姓名: <span style={spanStyle}>{this.state.name}</span>
				</p>
				<p>
					電話: <span style={spanStyle}>{this.state.phone}</span>
				</p>
				<p>
					Email: <span style={spanStyle}>{this.state.email}</span>
				</p>
				<p>
					預約安裝地點: <span style={spanStyle}>{this.state.selectedLocationName}</span>
				</p>
				<p>
					預約時間:{" "}
					<span style={spanStyle}>
						{moment(this.state.appointmentDate).format("dddd[,] MMMM Do[,] YYYY")}
					</span>{" "}
					at{" "}
					<span style={spanStyle}>
						{moment().hour(9).minute(0).add(this.state.appointmentSlot, "hours").format("h:mm a")}
					</span>
				</p>
			</section>
		);
	}

    reservationState = () => {
        let reservationState = this.state.allFieldsFilled;
        return reservationState;
    }

	// The handleSubmit method is used to pass user data to the database via the express app. it will display a snackbar message if the data is sucessfully saved in the database or if it fails.
	handleSubmit = () => {
		// this.setState({ confirmationModalOpen: false });
		const newAppointment = {
			name        : this.state.name,
			email       : this.state.email,
			phone       : this.state.phone,
			location_id : this.state.selectedLocation,
			slot_date   : moment(this.state.appointmentDate).format("YYYY-MM-DD"),
			slot_time   : this.state.appointmentSlot,
			member_id   : this.state.memberID,
			order_id    : this.state.orderID
		};

		if (newAppointment.slot_date == moment().format("YYYY-MM-DD")) {
			this.setState({ allFieldsFilled: false });
			return alert("請選擇預約日期");
		} else if (newAppointment.slot_time === undefined) {
			this.setState({ allFieldsFilled: false });
			return alert("請選擇預約時間");
		} else if (newAppointment.slot_time === "") {
			this.setState({ allFieldsFilled: false });
			return alert("請選擇預約時間");
		} else if (newAppointment.name == "") {
			this.setState({ allFieldsFilled: false });
			return alert("請填寫預約人的姓名");
		} else if (newAppointment.email == "") {
			this.setState({ allFieldsFilled: false });
			return alert("請填寫預約人的Email");
		} else if (newAppointment.phone == "") {
			this.setState({ allFieldsFilled: false });
			return alert("請填寫預約人的聯絡電話");
		} else {
			this.setState({ allFieldsFilled: true });
		}
		if (this.state.allFieldsFilled === true) {
			axios
				.post(API_BASE + "/reservations", newAppointment)
				.then((response) =>
					this.setState({
						confirmationSnackbarMessage : "預約成功!",
						confirmationSnackbarOpen    : false,
						processed                   : true
					})
				)
				.catch((err) => {
					console.log(err);
					return this.setState({
						confirmationSnackbarMessage : "預約失敗",
						confirmationSnackbarOpen    : true
					});
				});
		}
	};

	//stores reservation info inside local storage
	reservationInfo = () => {
		const newAppointment = {
			name        : this.state.name,
			email       : this.state.email,
			phone       : this.state.phone,
			location_id : this.state.selectedLocation,
			slot_date   : moment(this.state.appointmentDate).format("YYYY-MM-DD"),
			slot_time   : this.state.appointmentSlot,
			member_id   : this.state.memberID,
			order_id    : this.state.orderID
		};
		localStorage.setItem("reservationInfo", JSON.stringify(newAppointment));
	};

	// The render method renders all components to the virtual DOM(Document Object Model).
	render() {
		const {
			finished,
			isLoading,
			smallScreen,
			stepIndex,
			confirmationModalOpen,
			confirmationSnackbarOpen,
			...data
		} = this.state;
		const contactFormFilled = data.name && data.phone && data.email && data.validPhone && data.validEmail;
		const DatePickerExampleSimple = () => (
			<div>
				<DatePicker
					hintText='選擇日期'
					mode={smallScreen ? "portrait" : "landscape"}
					onChange={(n, date) => this.handleSetAppointmentDate(date)}
					shouldDisableDate={(day) => this.checkDisableDate(day)}
				/>
			</div>
		);
		const modalActions = [
			<FlatButton label='取消' primary={false} onClick={() => this.setState({ confirmationModalOpen: false })} />,
			<FlatButton
				label='確認預約時間'
				style={{ backgroundColor: "#00C853 !important" }}
				primary={true}
				// onClick={() => this.handleSubmit()}
				onClick={() => this.setState({ confirmationModalOpen: false, processed: true })}
			/>
		];
		return (
			<div>
				<section
					style={{
						maxWidth  : !smallScreen ? "100%" : "100%",
						margin    : "auto",
						marginTop : !smallScreen ? 20 : 0
					}}
				>
					<Card
						style={{
							padding : "12px 12px 25px 12px",
							height  : smallScreen ? "100vh" : null
						}}
					>
						<Stepper activeStep={stepIndex} orientation='vertical' linear={false}>
							<Step>
								<StepLabel>請選擇預約日期</StepLabel>
								<StepContent>
									{DatePickerExampleSimple()}
									{this.renderStepActions(0)}
								</StepContent>
							</Step>
							<Step disabled={!data.appointmentDate}>
								<StepLabel>請選擇預約時間</StepLabel>
								<StepContent>
									<SelectField
										floatingLabelText='AM/PM'
										value={data.appointmentMeridiem}
										onChange={(evt, key, payload) => this.handleSetAppointmentMeridiem(payload)}
										selectionRenderer={(value) => (value ? "PM" : "AM")}
									>
										<MenuItem value={0} primaryText='AM' />
										<MenuItem value={1} primaryText='PM' />
									</SelectField>
									<RadioButtonGroup
										style={{
											marginTop  : 15,
											marginLeft : 15
										}}
										name='appointmentTimes'
										defaultSelected={data.appointmentSlot}
										onChange={(evt, val) => this.handleSetAppointmentSlot(val)}
									>
										{this.renderAppointmentTimes()}
									</RadioButtonGroup>
									{this.renderStepActions(1)}
								</StepContent>
							</Step>
							<Step>
								<StepLabel>請填寫實際前來安裝配件的貴賓的聯絡資料</StepLabel>
								<StepContent>
									<p>
										<section>
											<TextField
												style={{ display: "block" }}
												name='name'
												hintText='姓名'
												floatingLabelText='姓名'
												onChange={(evt, newValue) => this.setState({ name: newValue })}
											/>
											<TextField
												style={{ display: "block" }}
												name='email'
												hintText='youraddress@mail.com'
												floatingLabelText='Email'
												errorText={data.validEmail ? null : "請填寫正確格式的電話Email"}
												onChange={(evt, newValue) => this.validateEmail(newValue)}
											/>
											<TextField
												style={{ display: "block" }}
												name='phone'
												hintText='0912345678'
												floatingLabelText='電話'
												errorText={data.validPhone ? null : "請填寫正確格式的電話"}
												onChange={(evt, newValue) => this.validatePhone(newValue)}
											/>
											<RaisedButton
												style={{ display: "block", backgroundColor: "#00C853" }}
												label={contactFormFilled ? "確認預約時段" : "請填寫個人資料"}
												labelPosition='before'
												primary={true}
												fullWidth={true}
												onClick={() =>
													this.setState({
														confirmationModalOpen : !this.state.confirmationModalOpen
													})}
												disabled={!contactFormFilled || data.processed}
												style={{ marginTop: 20, maxWidth: 100 }}
											/>
										</section>
									</p>
									{this.renderStepActions(2)}
								</StepContent>
							</Step>
						</Stepper>
					</Card>
					<Dialog modal={true} open={confirmationModalOpen} actions={modalActions} title='確認您的預約資訊'>
						{this.renderAppointmentConfirmation()}
					</Dialog>
					<SnackBar
						open={confirmationSnackbarOpen || isLoading}
						message={isLoading ? "確認中... " : data.confirmationSnackbarMessage || ""}
						autoHideDuration={10000}
						onRequestClose={() => this.setState({ confirmationSnackbarOpen: false })}
					/>
				</section>
			</div>
		);
	}
}
export default connect(AppointmentApp);
