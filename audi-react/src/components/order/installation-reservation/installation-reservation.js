import "./installation-reservation.scss";
import React, { Component, forwardRef, useRef, useImperativeMethods } from "react";
// https://stackoverflow.com/questions/37949981/call-child-method-from-parent
// https://github.com/kriasoft/react-starter-kit/issues/909#issuecomment-252969542
import { Container, Row, Col } from "reactstrap";

// to do: pass location id to appointmentapp

// https://v0.material-ui.com/#/
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import { black } from "material-ui/styles/colors";

import AppointmentApp from "./AppointmentApp.js";

// https://v0.material-ui.com/#/customization/themes
const muiTheme = getMuiTheme({
	palette : {
		primary1Color     : black,
		primary2Color     : black,
		primary3Color     : black,
		accent1Color      : black,
		accent2Color      : black,
		accent3Color      : black,
		// canvasColor        : black,
		pickerHeaderColor : black
	}
});
class InstallationReservation extends Component {
	constructor(props) {
		super(props);
		this.state = {
			locations        : [],
			selectedLocation : "1", //default location, 1 = 台北旗艦中心
        };
	}

	// get all store locations
	async componentWillMount() {
		const response = await fetch(`http://localhost:3000/installation_locations/locations`);
		const locations = await response.json();
		this.setState({ locations });
		// console.log(this.state.locations); //test
	}

	// necessary for onRef, allowing parent component to access methods within the child component:
	// https://github.com/kriasoft/react-starter-kit/issues/909#issuecomment-252969542
	componentDidMount() {
		this.props.onRef(this);
	}
	componentWillUnmount() {
		this.props.onRef(undefined);
	}
	submitReservation = () => {
		this.child.handleSubmit(); // access the handleSubmit method stored inside AppointmentApp.js
	};

	reservationInfo = () => {
		this.child.reservationInfo(); // access the reservationInfo method stored inside AppointmentApp.js
    };
    
    reservationState = () => {
        let reservationState = this.child.reservationState(); //check if all fields filled.
        return reservationState
    }

	onChange = (event) => {
		let selectedLocation = event.target.value;
		//setState is asynch, so you have to use: this.setState(() => ({ state: newState }));
		this.setState(() => ({
			selectedLocation : selectedLocation
		}));
		// console.log(selectedLocation); //test
    };

	render() {
		return (
			<Container fluid={true}>
				<Row>
					<Col xs='6' className='leftCol locations'>
						<h2>預約安裝配件地點</h2>
						<label htmlFor='locationSelect'>選擇安裝地點</label>
						<div className='aui-select aui-js-select aui-select--floating-label' id='locationSelect'>
							<select className='aui-select__input' onChange={(e) => this.onChange(e)}>
								{this.state.locations.map((location) => (
									<option
										value={location.location_id}
										// id={location.location_id}
										key={location.location_id}
										className='aui-select__input-label'
									>
										{location.location}
									</option>
								))}
							</select>
						</div>
						<br />
						{this.state.locations.map((location) => {
							if (location.location_id == this.state.selectedLocation) {
								return (
									<div className='locationInformation'>
										<p>據點資訊</p>
										<p>據點名稱：{location.location_name}</p>
										<p>電話：{location.telephone}</p>
										<p>地址：{location.address}</p>
										<p>營業時間：{location.hours_of_operation}</p>
									</div>
								);
							}
						})}
					</Col>
					<Col xs='6' className='rightCol bookingSystem'>
						<h2>預約安裝配件時間</h2>
						<MuiThemeProvider muiTheme={muiTheme}>
							<AppointmentApp
								selectedLocation={this.state.selectedLocation}
								onRef={(ref) => (this.child = ref)}
							/>
						</MuiThemeProvider>
					</Col>
				</Row>
			</Container>
		);
	}
}

export default InstallationReservation;
