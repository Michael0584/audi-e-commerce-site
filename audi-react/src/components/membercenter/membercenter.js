import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import Order from './order/order';
import Profile from './profile/profile';
import Contact from './contact/contact';
import Inbox from './mailbox/inbox';
import Outbox from './mailbox/outbox';
import Favorites from './favorites/favorites';
import Mail from './mailbox/mail'
import './membercenter.scss';
import { Container, Row, Col } from 'reactstrap';
import { connect } from "../../store";

class Membercenter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      listOpen: false,
      username: ""
    }
  }

  componentWillMount() {
    this.getMember()
  }

  getMember = () => {
    fetch(`http://localhost:3000/members/member`, {
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
    .then(res => res.json())
    .then(member_data => {
      this.setState({
        username: member_data[0].username,
      })
    })
  }

  listOpen = () => {
    if (this.state.listOpen === false) {
      this.setState({
        listOpen: true
      })
    } else {
      this.setState({
        listOpen: false
      })
    }

  }

  render() {
    return (
      <Container className="membercenter " fluid={true}>
        <Row>
          <Col lg={2} md={5} sm={5} >
          <div onClick={this.listOpen} className="rwd_member_nav"  >
          <div className="d-flex username">
            <div className="text-white">{this.state.username}，您好</div>
            <div>
              <img className="nav_icon drop_down px-3" src="/img/icons/arrow-narrow-down-small.svg" alt="" />
            </div>
          </div>
              <ul className={` open_list ${this.state.listOpen ? "show" : "hide"}`}>
                <li><Link to="/membercenter/inbox"><img className="nav_icon" src="/img/icons/direct-mail-small.svg" alt="" />收件匣</Link></li>
                <li><Link to="/membercenter/outbox"><img className="nav_icon" src="/img/icons/direct-mail-small.svg" alt="" />寄件備份</Link></li>
                <li><Link to="/membercenter/contact"><img className="nav_icon" src="/img/icons/discussion-small.svg" alt="" />問題回報</Link></li>
                <li><Link to="/membercenter/favorites"><img className="nav_icon" src="/img/icons/favorite-small.svg" alt="" />收藏商品</Link></li>
                <li><Link to="/membercenter/order"><img className="nav_icon" src="/img/icons/document-pricelist-dollar-small.svg" alt="" />訂單查詢</Link></li>
                <li><Link to="/membercenter/profile"><img className="nav_icon" src="/img/icons/edit-small.svg" alt="" />會員資料</Link></li>
                <li><a href="http://localhost:3000/members/logout"><img className="nav_icon" src="/img/icons/walk-small.svg" alt="" />登出</a></li>
              </ul>
            </div>

            <div className="member_nav">
              <ul>
                <li>{this.state.username}，您好</li>
                <li><Link to="/membercenter/inbox"><img className="nav_icon" src="/img/icons/direct-mail-small.svg" alt="" />收件匣</Link></li>
                <li><Link to="/membercenter/outbox"><img className="nav_icon" src="/img/icons/direct-mail-small.svg" alt="" />寄件備份</Link></li>
                <li><Link to="/membercenter/contact"><img className="nav_icon" src="/img/icons/discussion-small.svg" alt="" />問題回報</Link></li>
                <li><Link to="/membercenter/favorites"><img className="nav_icon" src="/img/icons/favorite-small.svg" alt="" />收藏商品</Link></li>
                <li><Link to="/membercenter/order"><img className="nav_icon" src="/img/icons/document-pricelist-dollar-small.svg" alt="" />訂單查詢</Link></li>
                <li><Link to="/membercenter/profile"><img className="nav_icon" src="/img/icons/edit-small.svg" alt="" />會員資料</Link></li>
                <li><a href="http://localhost:3000/members/logout"><img className="nav_icon" src="/img/icons/walk-small.svg" alt="" />登出</a></li>
              </ul>
            </div>


          </Col>
          <Col lg={10} md={7} sm={7}>
            <div className="member_contents">
              <Route path="/membercenter/order" component={Order} />
              <Route path="/membercenter/profile" component={Profile} />
              <Route path="/membercenter/contact" component={Contact} />
              <Route exact path="/membercenter/inbox" component={Inbox} />
              <Route exact path="/membercenter/outbox" component={Outbox} />
              <Route path="/membercenter/favorites" component={Favorites} />
              <Route path="/membercenter/inbox/:id" component={Mail} />
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default connect(Membercenter);
