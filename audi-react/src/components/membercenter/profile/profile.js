import React, { Component } from 'react';
import './profile.scss'
// import profileEdit from './profileEdit.js';
import { Route, Switch } from 'react-router-dom';
import { Col, Row, Form, FormGroup, Label, Input } from 'reactstrap';
import { connect } from "../../../store";
import TwCitySelector from 'tw-city-selector';

class Profile extends Component {
  constructor(props){
    super(props)
    this.state = {
      email         : "",
      password      : "",
      checkPassword : "",
      username      : "",
      mobile        : "",
      birthday      : "",
      county        : "",
      district      : "",
      address       : "", 
      enable        : false
    }
    // console.log(this);
  }

  componentWillMount() {
    new TwCitySelector ({
      el            : ".my-selector-c",
      elCounty      : ".county",
      elDistrict    : ".district",
      // countyValue   : this.props.profile.county,
      // districtValue : this.props.profile.district
    })
    this.getMember()
  }

  getMember = () => {
    fetch(`http://localhost:3000/members/member`, {
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
    .then(res => res.json())
    .then(member_data => {
      console.log(member_data)
      this.setState({
        email   : member_data[0].email,
        password: member_data[0].password,
        username: member_data[0].username,
        mobile  : member_data[0].mobile,
        birthday: member_data[0].birthday,
        county  : member_data[0].county,
        district: member_data[0].district,
        address : member_data[0].address
      })
    })
  }

  editChange = (evt) => {
    let key   = evt.target.id,
        data  = evt.target.value,
          // 電話格式確認
        mobile              = document.getElementById('mobile').value,
        mobile_Wrong        = document.getElementById('mobile_Wrong'),
        mobile_Function     = /^09[0-9]{8}$/,
        mobile_Check        = mobile_Function.test(mobile);

    switch(mobile_Check){ // 電話格式確認
      case false:
        mobile_Wrong.className="alert_Show"; break;
      default:
        mobile_Wrong.className="alert_Hidden";
    } if(mobile == ""){
      mobile_Wrong.className="alert_Hidden";
    }
    
    this.setState({
      [key]:data
    })
  }

  checkChange = (evt) => {
    let key   = evt.target.id,
        data  = evt.target.value,
          // 密碼確認
        password            = document.getElementById('password').value,
        checkPassword       = document.getElementById('checkPassword').value,
        password_Wrong      = document.getElementById('password_Wrong'),
        checkPassword_Wrong = document.getElementById('checkPassword_Wrong');
  
  if (password.length < 6){  // 密碼確認
    checkPassword_Wrong.className="alert_Hidden"
    password_Wrong.className="alert_Show";
    }else if (password != checkPassword){
      checkPassword_Wrong.className="alert_Show"
      password_Wrong.className="alert_Hidden";
      }else {
        checkPassword_Wrong.className="alert_Hidden"
        password_Wrong.className="alert_Hidden";
        }
    this.setState({
      [key]:data
    })
  }

  edit = () => {  // 修改
    let ckp = document.getElementById('ckp'),
        address_Show_1 = document.getElementById('address_Show_1'),
        address_Show_2 = document.getElementById('address_Show_2'),
        address_Edit_1 = document.getElementById('address_Edit_1'),
        address_Edit_2 = document.getElementById('address_Edit_2');
    this.setState({enable: true});
    ckp.className="show";
    address_Show_1.className="hidden";
    address_Show_2.className="hidden";
    address_Edit_1.className="show";
    address_Edit_2.className="show";
  }

  // 修改完成送出
  editOK = (evt) => { 
    evt.preventDefault()
    var data = {
          email         : this.props.profile.email,
          password      :this.state.password,
          username      :this.state.username,
          mobile        : this.state.mobile,
          birthday      : this.state.birthday,
          county        : this.state.county,
          district      : this.state.district,
          address       : this.state.address, 
          // isAdmin       : 0
        }    
    console.log(data)
      fetch(`http://localhost:3000/members/member`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'include',
        body: JSON.stringify(data),
        headers: new Headers({
          'Content-Type': 'application/json'
        })
      })
      .then(res => res.json())
      .then(data => {
        window.location.href="http://localhost:3001/membercenter/profile"
        alert(data.message)
      });
      // console.log(this)
    
  }

  cancel = () => {  // 取消修改
    let ckp = document.getElementById('ckp'),
        address_Show_1 = document.getElementById('address_Show_1'),
        address_Show_2 = document.getElementById('address_Show_2'),
        address_Edit_1 = document.getElementById('address_Edit_1'),
        address_Edit_2 = document.getElementById('address_Edit_2');
    this.setState({enable: true});
    ckp.className="hidden";
    address_Show_1.className="show";
    address_Show_2.className="show";
    address_Edit_1.className="hidden";
    address_Edit_2.className="hidden";
    // window.location.href="http://localhost:3001/membercenter/profile";
  }

  render() {
    return (
      <div className="container-fluid profile">
        會員資料
        <hr />
        <div className="my_profile">個人資料
          <i className="fas fa-edit edit_btn" onClick={this.edit}></i>
        </div>
        <Switch>
          {/* <Route exact path={`${this.props.match.path}/edit`} component={profileEdit} /> */}
          <Form>
            <br />

            <Row form>
              <Col md={1}></Col>
              <Col md={5}>
                <FormGroup>
                  <Label htmlFor="email">會員帳號(E-mail)</Label>
                  <Input type="email" name="email" id="email" disabled value={this.props.profile.email} />
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Col md={1}></Col>
              <Col md={5}>
                <FormGroup>
                  <Label htmlFor="password">會員密碼</Label>
                  <Input type="password" name="password" id="password" autoComplete="off" value={this.state.password || this.props.profile.password} onChange={this.checkChange} disabled={!this.state.enable} />
                  <Label id="password_Wrong" className="alert_Hidden">密碼長度過短</Label>
                  <Label id="checkPassword_Wrong" className="alert_Hidden">密碼輸入不一致</Label>
                </FormGroup>
              </Col>
              <Col md={5}>
                <FormGroup id="ckp" className="alert_Hidden">
                  <Label htmlFor="checkPassword">確認會員密碼 *(如未修改不需填寫)</Label>
                  <Input type="password" name="checkPassword" id="checkPassword" autoComplete="off" value={this.state.checkPassword || this.props.profile.checkPassword} onChange={this.checkChange} disabled={!this.state.enable} />
                </FormGroup>
              </Col>
            </Row>
            <hr />

            <Row form>
              <Col md={1}></Col>
              <Col md={5}>
                <FormGroup>
                  <Label htmlFor="username">姓名</Label>
                  <Input type="text" name="username" id="username" maxLength="10" autoComplete="off" value={this.state.username || this.props.profile.username} onChange={this.editChange} disabled={!this.state.enable} />
                </FormGroup>
              </Col>
            </Row>

            <Row form>
              <Col md={1}></Col>
              <Col md={5}>
                <Label htmlFor="mobile">手機電話</Label>
                <Input type="text" name="mobile" id="mobile" maxLength="10" autoComplete="off" value={this.state.mobile || this.props.profile.mobile} onChange={this.editChange} disabled={!this.state.enable} />
                <Label id="mobile_Wrong" className="alert_Hidden">手機電話格式不符, 請填寫正確格式</Label>
              </Col>
              <Col md={5}>
                <FormGroup>
                  <Label htmlFor="birthday">生日</Label>
                  <Input type="date" name="birthday" id="birthday" autoComplete="off" value={this.state.birthday || this.props.profile.birthday} onChange={this.editChange} disabled={!this.state.enable} />
                </FormGroup>
              </Col>
            </Row>

              {/* 顯示的地址 */}
            <Row form>
              <Col md={1}></Col>
              <Col md={3}>
                <FormGroup id="address_Show_1">
                  <Label htmlFor="county_2">縣市</Label>
                  <Input type="text" name="county" id="county_2" value={this.state.county || this.props.profile.county} onChange={this.editChange} disabled={!this.state.enable} />
                </FormGroup>
              </Col>
              <Col md={3}>
                <FormGroup id="address_Show_2">
                  <Label htmlFor="district_2">地區</Label>
                  <Input type="text" name="district" id="district_2" value={this.state.district || this.props.profile.district} onChange={this.editChange} disabled={!this.state.enable} />
                </FormGroup>
              </Col>
            </Row>

              {/* 修改下拉的地址 */}
            <Row className="my-selector-c">
              <Col md={1}></Col>
              <Col md={3}>
                <FormGroup id="address_Edit_1" className="hidden">
                  <Label htmlFor="county">縣市</Label>
                  <select type="text" className="county form-control" name="county" id="county" value={this.state.county} onChange={this.editChange} disabled={!this.state.enable} ></select>
                </FormGroup>
              </Col>
              <Col md={3}>
                <FormGroup id="address_Edit_2"  className="hidden">
                  <Label htmlFor="district">地區 *(未修改不需調整)</Label>
                  <select type="text" className="district form-control" name="district" id="district" value={this.state.district} onChange={this.editChange} disabled={!this.state.enable} ></select>
                </FormGroup>
              </Col>
            </Row>
            <br />

            <Row form>
              <Col md={1}></Col>
              <Col md={10}>
                <FormGroup>
                  <Label htmlFor="address">地址</Label>
                  <Input type="text" name="address" id="address" autoComplete="off" value={this.state.address || this.props.profile.address} onChange={this.editChange} disabled={!this.state.enable}></Input>
                </FormGroup>
                <br />
                <Row>
                  <Col md={3}>
                    {this.state.enable && <button className="commit_profile_btn checkOutBtn aui-js-response aui-button aui-button--primary aui-button--stretched" type="submit" onClick={this.editOK}>送出</button>}
                  </Col>
                  <Col md={4}>
                    {this.state.enable && <button className="commit_profile_btn checkOutBtn aui-js-response aui-button aui-button--primary aui-button--stretched" onClick={this.cancel}>取消修改</button>}
                  </Col>
                </Row>
              </Col>
            </Row>
          </Form>
        </Switch>
        <br />
      </div>
    );
  }
}

export default connect(Profile);
