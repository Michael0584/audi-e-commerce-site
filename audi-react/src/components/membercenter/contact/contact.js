import React, {
  Component
} from 'react';
import {
  CustomInput
} from 'reactstrap';
import './contact.scss'
import {
  connect
} from "../../../store";

class Contact extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mailSender: this.props.profile.email,
      mailRecipient: "audi",
      mailCategories: "",
      mailSubject: "",
      mailDetail: "",
      mailImage: "",
      mailImageName: "",
      mailDate: Date.now(),
      mailSubjectOptions: [],
      invalidCategories: "",
      invalidSubject: "",
    }
  }
  async componentWillMount() {
    await fetch('http://localhost:3000/membercenter/contact', {
        credentials: 'include'
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)
      this.setState({mailSubjectOptions: data})})
  }

  componentWillReceiveProps(props) {
    this.setState({
      mailSender: props.profile.email
    })
  }

  changeHandler = (evt) => {
    if (evt.target.name === "mailImage") {
      this.setState({
        mailImage: evt.target.files[0],
        mailImageName: Date.now() + evt.target.files[0].name,
      })
    } else {
      this.setState({
        [evt.target.name]: evt.target.value
      })
    }
  }

  submitHandler = (evt) => {
    evt.preventDefault();
    if (this.state.mailCategories === "") {
      this.setState({
        invalidCategories: "請選擇類別"
      })
      if (this.state.mailSubject === "") {
        this.setState({
          invalidSubject: "請選擇標題"
        })
      }
    } else {
      var obj = {
        mailSender: this.props.profile.email,
        mailRecipient: this.state.mailRecipient,
        mailCategories: this.state.mailCategories,
        mailSubject: this.state.mailSubject,
        mailDetail: this.state.mailDetail,
        mailImageName: this.state.mailImageName,
        mailDate: Date.now(),
      }
      fetch('http://localhost:3000/membercenter/contact', {
          method: 'POST',
          body: JSON.stringify(obj),
          mode: 'cors',
          headers: new Headers({
            'Content-Type': 'application/json'
          })
        })
        .then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (data) {
          alert("問題回報成功")
        })

      if (this.state.mailImage !== "") {
        let formData = new FormData();
        formData.append('mailImage', this.state.mailImage, this.state.mailImageName);
        fetch("http://localhost:3000/membercenter/contact/upload", {
            method: "POST",
            body: formData,
          }).then(res => res.json())
          .then(data => console.log(data))
      }
    }

  }

  render() {
    return (
      <div className="container-fluid contact">
        問題回報
           <hr />
        <div className="my_contact">我的問題回報 </div>
        <form onSubmit={this.submitHandler} >
          <br/>
          <div >信件標題(訂單編號)</div>
          <div className="aui-select aui-js-select aui-select--floating-label" >
            <select className="aui-select__input" name="mailSubject" id="sample-select" value={this.state.mailSubject} onChange={this.changeHandler}>
              <option disabled value="">請選擇</option>
              <option value="非訂單問題">非訂單問題</option>
              {this.state.mailSubjectOptions.map(
                mailSubjectOption => <option key={mailSubjectOption}>{mailSubjectOption}</option>)}
            </select>
            <span className="aui-textfield__error invaild_subject">{this.state.invalidSubject}</span>
          </div>
          <div >問題類別</div>
          <div className="aui-select aui-js-select aui-select--floating-label">
            <select className="aui-select__input" name="mailCategories" id="mailCategories" value={this.state.mailCategories} onChange={this.changeHandler} >
              <option disabled value="">請選擇</option>
              <option value="商品退貨">商品退貨</option>
              <option value="取消訂單">取消訂單</option>
              <option value="修改安裝時間">修改安裝時間</option>
              <option value="商品使用問題">商品使用問題</option>
              <option value="商品優惠">商品優惠</option>
              <option value="其他">其他</option>
            </select>
            <span className="aui-textfield__error invaild_categories">{this.state.invalidCategories}</span>
          </div>
          <div className="aui-textfield aui-js-textfield aui-textfield--multiline" >
            <div>詳細情形</div>
            <div className="aui-textfield__field">
              <textarea name="mailDetail" className="aui-textfield__input" id="sample-textfield-multiline" rows="3" value={this.state.mailDetail} onChange={this.changeHandler} placeholder="請描述狀況"></textarea>
            </div>
          </div>
          <div>佐證圖片(限一張)</div>
          <CustomInput type="file" id="mail_image" name="mailImage" onChange={this.changeHandler} label={this.state.mailImageName} />
          <button className="submit_btn">送出</button>
        </form >

            </div>
          );
        }
      }

      export default connect(Contact);