import React, { Component } from 'react';
import { connect, setReadCount } from "../../../store";
import './mail.scss';
class Mail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mails: [],

    }
  }
  componentWillMount() {
    fetch(`http://localhost:3000/membercenter/inbox/${this.props.match.params.id}`)
      .then(res => res.json())
      .then(data => this.setState({ mails: data }))
      .then(setReadCount())
  }
  transferDate(date) {
    let mailDate = new Date(date);
    return mailDate.toLocaleDateString();
  }
  render() {
    return (
      <React.Fragment>
        <div className="container-fluid contact">
          {this.state.mails.map((mail, index) =>
            <div className="mail" key={index}>
              <div className="row">
                <p className="topic col-12 col-lg-2">信件標題</p>
                <p className="px-3 col-12 col-lg-10">{mail.mailSubject}</p>
              </div>
              <div className="row">
                <p className="topic col-12  col-lg-2">寄件時間</p>
                <p className="px-3 col-12 col-lg-10">{this.transferDate(mail.mailDate)}</p>
              </div>
              <div className="row">
                <p className="topic col-12  col-lg-2">寄件者</p>
                <p className="px-3 col-12 col-lg-10">{mail.mailSender}</p>
              </div>
              <div className="row">
                <p className="topic col-12  col-lg-2">收件者</p>
                <p className="px-3 col-12 col-lg-10">{mail.mailRecipient}</p>
              </div>
              <div className="row">
                <p className="topic col-12  col-lg-2">信件類別</p>
                <p className="px-3 col-12 col-lg-10">{mail.mailCategories}</p>
              </div>
              <div className="row">
                <p className="topic col-12  col-lg-2">信件內容</p>
              </div>         
              <div className="px-3  mail_detail">
                <p className=" col-12 ">{mail.mailDetail}</p>
                <img className="upload_img col-12" src={mail.mailImage_name}  alt=""/>
              </div>
            </div>
          )}
        </div>
      </React.Fragment>

    )
  }
}

export default Mail;
