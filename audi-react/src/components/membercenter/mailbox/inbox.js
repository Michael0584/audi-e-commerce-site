import React, { Component } from 'react';
import { Table, Label } from 'reactstrap'
import { Link } from 'react-router-dom';
import './inbox.scss'
import { connect, setReadCount } from "../../../store";


class Inbox extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mails: [],
      selectedIndexs: {},
      allSelected: false,
      search: "",
      username: "",
    }
  }

  componentWillMount() {
    this.getDatas()
  }

  getDatas = () => {
    fetch(`/membercenter/inbox`)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        this.setState({mails: data})
      })
  }

  deleteSelected = (mail) => {
    const isAnySelected = Object.values(this.state.selectedIndexs).some(value => value)
    if (this.state.mails.length === 0 || !isAnySelected) {
      return
    }

    const selectedIds = this.state.mails.filter((mail, index) => {
      return this.state.selectedIndexs[index]
    }).map(mail => mail.id)

    fetch('http://localhost:3000/membercenter/inbox',
      {
        method: 'delete',
        body: JSON.stringify({ ids: selectedIds }),
        headers: new Headers({
          'Content-type': 'application/json'
        })
      })
      .then(res => res.json())
      .then((data) => {
        if (data.success) {
          this.getDatas()
          this.setState({ selectedIndexs: {}, allSelected: false })
        }
        alert(data.message)
      })
      .then(setReadCount())
  }

  selectAll = () => {
    const { selectedIndexs, mails, allSelected } = this.state
    mails.forEach(function (mail, index) {
      selectedIndexs[index] = !allSelected
    })

    this.setState({ selectedIndexs, allSelected: !allSelected })
  }

  isAllSelected = () => {
    const { selectedIndexs, mails } = this.state
    const isAllSelected = mails.every(function (mail, index) {
      return selectedIndexs[index]
    })

    return isAllSelected
  }

  searchHandler = (evt) => {
    this.setState({
      search: evt.target.value
    })
  }

  transferDate(date) {
    let mailDate = new Date(date);
    return mailDate.toLocaleDateString();
  }

  readHandler = (mail) => {
    fetch(`http://localhost:3000/membercenter/inbox/${mail.id}`, {
      method: 'PUT',
    })
    .then(setReadCount())
    
    
  }

  handleCheck(index) {
    const { selectedIndexs } = this.state
    selectedIndexs[index] = !selectedIndexs[index]
    this.setState({ selectedIndexs, allSelected: this.isAllSelected() })
  }

  handleRowDelete(mail, index) {
    fetch(`http://localhost:3000/membercenter/inbox/${mail.id}`, {
      method: 'DELETE',
    })
      .then(res => res.json())
      .then(this.getDatas())
      .then(setReadCount())
  }

  fetchMore(page) {
    this.setState({ currentPage: page }, this.getDatas)
  }




  render() {
    return (
      <div className="container-fluid inbox">
        收件匣
        <hr />

        <div className="aui-textfield__field">
          <input className="aui-textfield__input" type="text" id="sample-textfield-icon" value={this.state.search} onChange={this.searchHandler} placeholder="Search" />
        </div>
        <br />
        <Table bordered hover responsive >
          <thead className="">
            <tr>
              <th>
                <Label check>
                  <input type="checkbox" onChange={this.selectAll} checked={this.state.allSelected} />
                </Label>
              </th>
              <th>信件標題</th>
              <th>信件類別</th>
              <th>信件內容</th>
              <th>信件日期 </th>
              <th>
                <img src="/img/icons/erase-small.svg" className="del_btn" alt="" onClick={() => this.deleteSelected()} />
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.mails.filter(mail => mail.mailDetail.includes(this.state.search) || mail.mailSubject.includes(this.state.search) || mail.mailCategories.includes(this.state.search)).map((mail, index) =>
              (<tr className={`${mail.unRead === 1 ? "read" : "unread"}`} key={mail.id}>
                <td><Label check><input type="checkbox" checked={this.state.selectedIndexs[index] || false} onChange={() => this.handleCheck(index)} /></Label></td>
                <td><Link onClick={() => this.readHandler(mail, index)} to={`/membercenter/inbox/${mail.id}`}>{mail.mailSubject}</Link></td>
                <td >{mail.mailCategories}</td>
                <td className="mail_detail">{mail.mailDetail}</td>
                <td>{this.transferDate(mail.mailDate)}</td>
                <td><img src="/img/icons/erase-small.1.svg" className="del_btn" alt="" onClick={() => this.handleRowDelete(mail, index)} /></td>
              </tr>))}
          </tbody>
        </Table>
      </div >
    );
  }
}

export default connect(Inbox);
