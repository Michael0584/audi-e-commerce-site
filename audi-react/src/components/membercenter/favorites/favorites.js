import React, { Component } from 'react';
import { Table, Label } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { connect } from "../../../store";

import './favorites.scss'
class Favorites extends Component {
  constructor(props) {
    super(props)
    this.initState = {
      product_name: "",
      price       : ""
    }
    this.state = {
      follows : [],
      follow  : this.initState,
      check   : false
    }
    console.log(this)
  }

  componentWillMount() {
    this.getFollow()
  }

  getFollow = () => {
    fetch(`http://localhost:3000/members/follow`, {
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
    .then(res => res.json())
    .then(data => {
      this.setState({
        follows: data
      })
    });
  }

  selectAll = (evt) => {  // checkbox全選
    var boxs     = document.getElementsByName("box"),
        checkall = document.getElementById("check_all");
      if(checkall.checked){
        for(var i=0; i<boxs.length; i++){
          boxs[i].checked = "checked";
        }
      }else {
        for(var i=0; i<boxs.length; i++){
          boxs[i].checked = null;
        }
      }
  }

  Cancel = (evt) => { // 單筆 follow 刪除
    var sid = evt.target.dataset.sid
    fetch(`http://localhost:3000/members/follow/${sid}`, {
      method: 'DELETE',
    })
    .then(res => res.json())
    .then(this.getFollow())
  }

  // CancelAll = (evt) => {
  //   console.log(evt.target.dataset.sids)
  //   var boxs = document.getElementsByName("box"),
  //       ids  = "";
  //   for(var i=0; i<boxs.length; i++){
  //     if (boxs[i].checked){
  //       ids += boxs[i].value + ',';
  //     }
  //   }
  //   fetch(`http://localhost:3000/members/follow/`, {
  //     method: 'DELETE',
  //   })
  //   .then(res => res.json())
  //   .then(this.getFollow())
  // }

  render() {
    return (
      <div className="container-fluid favorites">
        收藏商品
        <hr/>
        <Table striped bordered hover responsive >
          <thead className="">
            <tr key={this.state.follows.sids}>
              {/* <th>
                <Label check>
                  <input type="checkbox" id="check_all" name="scales" onClick={this.selectAll}/>
                </Label>
              </th> */}
              <th>商品圖片</th>
              <th>商品名稱</th>
              <th>單品價格</th>
              <th>
                <img src="/img/icons/erase-small.svg" className="del_btnAll" alt="" data-sid={this.state.follows.sids}/>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.follows.map(follow =>
              <tr key={follow.sid}>
                {/* <td>
                  <Label check>
                    <input type="checkbox" className="scales" name="box" data-sid={follow.sid} onClick={this.checkHandler}/>
                  </Label>
                </td> */}
                <td>
                  {/* <NavLink to={"/product/"}> */}
                    <img onClick="productCheck" className="productImg" src={`/img/${follow.product_img}`}/>
                  {/* </NavLink> */}
                </td>
                <td className="item">{follow.product_name}</td>
                <td className="item">{follow.price}</td>
                <td className="itemImg">
                  <img src="/img/icons/erase-small.1.svg" data-sid={follow.sid} data-type="del" className="del_btn" alt="" onClick={this.Cancel}/>
                </td>
              </tr>
            )}
          </tbody>
        </Table>
          {/* 收藏商品 (需放在會員中心内，要登入後才能開始收藏)
          <br/>
          若未登入，navbar icon點了會有tooltip彈出來提示用戶要登入後面才能開始收藏，提示下面放登入CTA的BTN
          <br/>
          若登入，navbar icon點了會有tooltip彈出來，顯示已收藏商品的清單（看XD設計）
          <br/>
          此icon也會有notification bubble顯示收藏商品數量 */}
      </div>
    );
  }
}

export default connect(Favorites);