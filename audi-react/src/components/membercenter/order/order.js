import React, { Component } from 'react';
import { Table } from 'reactstrap'
import { NavLink } from "react-router-dom";
import './order.scss'

class Order extends Component {
  constructor(props) {
    super(props)
    this.initState = {
      order_id: "",
      cart: "",
      installation_reservation_info: "",
      total_price: ""
    }
    this.state = {
      member_orders: [],
      member_order: this.initState
    }
    this.numberWithCommas = (number) => {
			return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		};
  }

  componentWillMount() {
    this.getMemberOrder()
  }

  getMemberOrder = () => {
    fetch(`http://localhost:3000/members/member_order`, {
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
    .then(res => res.json())
    .then(data => this.setState({
      member_orders: data
    }))
  }

  render() {
    // console.log(this.state.member_orders)
    return (
      <div className="container-fluid order">
        訂單查詢
        <hr />
        <Table striped bordered hover responsive > 
          <thead className="">
            <tr>
              <th>訂單編號</th>
              <th>總金額</th>
              <th>訂單付款日期</th>
            </tr>
            {/* <tr>
              <th></th>
              <th></th>
              <th>處理進度</th>
              <th>規格尺寸</th>
              <th>處理進度</th>
              <th>預約日期</th>
              <th></th>
              <th></th>
            </tr> */}
          </thead>
          <tbody>
            {this.state.member_orders.map(order =>
              <tr key={order.id}>
                <td><NavLink to={"/order/"+ order.order_id}>{order.order_id}</NavLink></td>
                <td>${this.numberWithCommas(order.total_price)}</td>
                <td>{JSON.parse(order.payment_info).transactionTime}</td>
              </tr>
            )}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default Order;