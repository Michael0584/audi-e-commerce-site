import React, { Component } from 'react';
import './chatroom.scss'
import openSocket from 'socket.io-client'
import { connect } from "../../store";
import Message from "./Message";

const socket = openSocket('http://localhost:3000')

class Chatroom extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: "",
      messages: [],
      show: false,
      wait: "",
    }
  }

  componentWillMount() {
    socket.on('historyMessage', async (messages) => {
      await this.setState({
        messages: messages.concat({ email_sender: "audi", message: "歡迎來到audi，請問有什麼可以為您服務的?" }),
      })
      this.scrollToBottom()
    });

    socket.on('recieveMessage', async (msg) => {
      const msgFromOthers = msg.email_sender !== this.props.profile.email
      if (msgFromOthers) {
        await this.setState({
          messages: this.state.messages.concat(msg),
        })
        this.scrollToBottom();
      }
    });

    socket.on('adminOff', async (msg) => {
      await this.setState({
        messages: this.state.messages.concat(msg),
      })
      this.scrollToBottom();
    });

  }

  changeHandler = (evt) => {
    this.setState({
      value: evt.target.value,
    })
  }

  clickHandler = async () => {
    let sendMessage = { email_sender: this.props.profile.email, message: this.state.value }
    if (this.state.value !== "") {
      socket.emit("sendMessage", sendMessage)
      await this.setState({
        messages: this.state.messages.concat(sendMessage),
        value: ""
      })
      this.scrollToBottom()
    }
  }

  keyDownHandler = (evt) => {
    if (evt.keyCode === 13) {
      this.clickHandler();
    }
  }
  maximizeHandler = () => {
    this.setState({
      show: true,
    })

  }
  minimizeHandler = () => {
    this.setState({
      show: false,
    })
  }
  scrollToBottom() {
    const messageContainer = this.refs.messageContainer
    messageContainer.scrollTop = messageContainer.scrollHeight - messageContainer.offsetHeight
  }

  render() {
    return (
      <React.Fragment>
        <div className={`chatroom_window ${this.state.show ? "show" : "hide"}`}>
          <div className="chatroom">
            <div className="chatroom_bar">
              {/* <span className="wait">目前等待人數:{this.state.wait}</span> */}
              <span className="chatroom_minimize_area " onClick={this.minimizeHandler}>
                <i className="fas fa-minus chatroom_close"></i>
              </span>
            </div>
            <div className="messages" ref="messageContainer">
              {this.state.messages.map((messageObj, index) => (
                <div key={index} className={this.props.profile.email === messageObj.email_sender ? "message_right" : "message_left"}>
                  <img src='/img/icons/user-large.svg' className={this.props.profile.email === messageObj.email_sender ? "icon_hide" : ""} />
                  <p><Message message={messageObj.message}></Message></p>
                </div>
              ))}
            </div>
            <div className="message_bar">
              <input type="text" className="message_input" value={this.state.value} onChange={this.changeHandler} onKeyDown={this.keyDownHandler} placeholder="請輸入文字" />

              <button className="message_btn" onClick={this.clickHandler}>送出</button>
            </div>
          </div>
        </div>
        <div className="service_icon" onClick={this.maximizeHandler}>
          <img src="/img/icons/hotline-large.svg" alt="" />
        </div>
      </React.Fragment>
    )
  }
}
export default connect(Chatroom);
