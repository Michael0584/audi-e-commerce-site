import React from 'react';

export default function Message(props) {
    const message = props.message.replace(/(https?:\/\/[\w\.:\/\?\&#]*)/, function(match){
        return `<a href="${match}">${match}</a>`
    })        
    return <span dangerouslySetInnerHTML={{__html:message}}></span>
}

