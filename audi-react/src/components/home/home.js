import "./home.scss";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import { Container, Row, Col } from "reactstrap";
import ReactFullpage from "@fullpage/react-fullpage";
import CarouselSlider from "./banner-carousel/carousel-slider";
import WhirligigSlider from "./newest-product-carousel/newest-product-carousel";
import Footer from ".././footer/footer";

const anchors = [
	"landing",
	"shop",
	"latest",
	// "featured",
	"footer"
];

const Fullpage = () => (
	<ReactFullpage
		anchors={anchors}
		verticalCentered={true}
		// normalScrollElements="#popoverBody"
		render={({ state, fullpageApi }) => {
			return (
				<ReactFullpage.Wrapper>
					<div className='section landingCarousel'>
						<Container fluid={true}>
							<Row>
								<Col className='text-center'>
									<CarouselSlider />
								</Col>
							</Row>
						</Container>
					</div>
					<div className='section shop'>
						<Container fluid={true}>
							<Row className='d-flex flex-row justify-content-center flex-wrap'>
								<Col xs={5} className='accessories d-flex flex-row justify-content-center'>
									<div className='accessoriesWrapper d-flex flex-column align-items-center'>
										<h1>Audi 原廠配件</h1>
										<Link to='/products/accessories' className='accessoriesLink'>
											<img src='/img/home/AGA_bg.png' alt='' className='accessoriesImg' />
											<button
												className='aui-js-response aui-button aui-button--primary aui-button--stretched'
												type='button'
											>
												探索配件
											</button>
										</Link>
									</div>
								</Col>
								<Col xs={5} className='collection d-flex flex-row justify-content-center'>
									<div className='collectionsWrapper d-flex flex-column align-items-center'>
										<h1>Audi 原廠精品</h1>
										<Link to='/products/collections' className='collectionLink'>
											<img src='/img/home/AC_bg.png' alt='' className='collectionImg' />
											<button
												className='aui-js-response aui-button aui-button--primary aui-button--stretched'
												type='button'
											>
												探索精品
											</button>
										</Link>
									</div>
								</Col>
							</Row>
						</Container>
					</div>
					<div className='section newestProduct'>
						<Container fluid={true}>
							<Row>
								<Col className='text-center newestProductSlider'>
									<h1>最新商品</h1>
									<WhirligigSlider />
								</Col>
							</Row>
						</Container>
					</div>
					{/* <div className='section featuredProducts'>
						<Container fluid={true}>
							<Row>
								<Col className='text-center'>
									<h1>熱銷商品</h1>
								</Col>
							</Row>
						</Container>
					</div> */}
					<div className='section footer fp-auto-height'>
						<Footer />
					</div>
				</ReactFullpage.Wrapper>
			);
		}}
	/>
);
class Home extends Component {
	render() {
		return <Fullpage />;
	}
}

export default Home;
