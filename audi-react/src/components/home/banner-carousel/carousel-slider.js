import './carousel-slider.scss';
import React, { Component } from 'react';
import CarouselSlide from './carousel-slide';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';

// see https://medium.com/@ItsMeDannyZ/build-an-image-slider-with-react-es6-264368de68e4 for tutorial
// to do: make slider responsive when window is being dragged to size (currently only resizes on reload)

class CarouselSlider extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentSlide: 0,
			translateValue: 0,
			totalImages: [ 0, 1, 2, 3, 4, 5 ],
			promoType: [
				'Audi Genuine Accessories',
				'Audi Collections',
				'Audi Collections',
				'Audi Collections',
				'Audi Collections',
				'Audi Collections'
			],
			promoName: [
				{__html:'Audi<br/>原廠配件優惠'},
				{__html:'Audi<br/>原廠精品優惠'},
				{__html:'Audi<br/>精品水壺優惠'},
				{__html:'Audi<br/>兒童玩具優惠'},
				{__html:'冬季女裝<br/>熱銷款式優惠'},
				{__html:'Audi<br/>太陽眼鏡優惠'},
			],
			promoDescription: [
				'Audi原廠升級鋁圈，含輪胎優惠全面7折起',
				'盡情享受探索生活的樂趣',
				'適用於家庭或辦公室的時尚配件',
				'專業的玩具設計團隊、不斷創新的體系',
				'注入當代女性的獨立與優雅魅力',
				'專為亞洲人設計的馳放偏光太陽眼鏡'
			],
			promoPageLink: [
				'/promotion01',
				'/promotion02',
				'/promotion03',
				'/promotion04',
				'/promotion05',
				'/promotion06'
			],
			promotion: []
		};
		this.slideWidth = this.slideWidth.bind(this);
	}

	// componentDidMount() {
	// 	window.addEventListener("resize", this.handleResize);
	// }

	// handleResize = () => {
	// 	this.setState((prevState) => ({
	// 		// currentSlide   : prevState.currentSlide + 1,
	// 		translateValue : this.slideWidth()
	// 	}));
	// };

	goToSlide = (slideNumber) => {
		this.setState({
			currentSlide: slideNumber,
			translateValue: -this.slideWidth() * slideNumber
		});
	};

	goToPrevSlide = () => {
		if (this.state.currentSlide === 0) {
			return this.setState({
				currentSlide: this.state.totalImages.length - 1,
				translateValue: -this.slideWidth() * (this.state.totalImages.length - 1)
			});
		}

		this.setState((prevState) => ({
			currentSlide: prevState.currentSlide - 1,
			translateValue: prevState.translateValue + this.slideWidth()
		}));
	};

	goToNextSlide = () => {
		// Exiting the method early if we are at the end of the images array.
		// We also want to reset currentSlide and translateValue, so we return
		// to the first image in the array.
		if (this.state.currentSlide === this.state.totalImages.length - 1) {
			return this.setState({
				currentSlide: 0,
				translateValue: 0
			});
		}

		// This will not run if we meet the if condition above (when we reach the last img)
		this.setState((prevState) => ({
			currentSlide: prevState.currentSlide + 1,
			translateValue: prevState.translateValue + -this.slideWidth()
		}));
	};

	slideWidth = () => {
		return document.querySelector('.carousel-slide').clientWidth;
	};

	render() {
		return (
			<Container>
				<Row>
					<Col className="carouselLeftCol" xs={4}>
						<div className="promoInfo">
							<h1 className="promoType" >{this.state.promoType[this.state.currentSlide]}</h1>
							<h2 className="promoName" dangerouslySetInnerHTML={this.state.promoName[this.state.currentSlide]}></h2>
							<p className="promoDescription">{this.state.promoDescription[this.state.currentSlide]}</p>
							<Link to={this.state.promoPageLink[this.state.currentSlide]}>
								<button
									className="aui-js-response aui-button aui-button--primary aui-button--stretched"
									type="button">
									了解更多
								</button>
							</Link>
						</div>
					</Col>
					<Col className="carouselRightCol" xs={8}>
						<div className="carouselComponentWrapper">
							<div className="carousel">
								<div
									className="carousel-wrapper"
									style={{
										transform: `translateX(${this.state.translateValue}px)`,
										transition: `transform ease-out 0.45s`
									}}>
									<CarouselSlide image={`/img/home/banner-carousel/7.jpg`} />
									<CarouselSlide image={`/img/home/banner-carousel/2.jpg`} />
									<CarouselSlide image={`/img/home/banner-carousel/8.png`} />
									<CarouselSlide image={`/img/home/banner-carousel/10.png`} />
									<CarouselSlide image={`/img/home/banner-carousel/9.png`} />
									<CarouselSlide image={`/img/home/banner-carousel/11.png`} />
								</div>
							</div>
							<div className="arrows">
								<div className="leftArrow" onClick={this.goToPrevSlide}>
									<img className="leftArrowIcon" src="/img/icons/arrow-up-large.png" alt="" />
								</div>
								<div className="rightArrow" onClick={this.goToNextSlide}>
									<img className="rightArrowIcon" src="/img/icons/arrow-up-large.png" alt="" />
								</div>
							</div>
						</div>
					</Col>
				</Row>
				<Row>
					<Col className="carouselPagination">
						<div className="slidePosition">
							<ul>
								{this.state.totalImages.map((item, index) => (
									<li
										key={index}
										className={`paginationBlock ${this.state.currentSlide === index
											? 'active'
											: ''}`}
										onClick={this.goToSlide.bind(this, index)}
									/>
								))}
							</ul>
						</div>
					</Col>
				</Row>
			</Container>
		);
	}
}

export default CarouselSlider;
