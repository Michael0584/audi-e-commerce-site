import React, { Component } from "react";

const CarouselSlide = ({ image }) => {
	const styles = {
		backgroundImage    : `url(${image})`,
		backgroundSize     : "cover",
		backgroundRepeat   : "no-repeat",
        backgroundPosition : "50% 60%",
	};
	return <div className='carousel-slide' style={styles} />;
};

export default CarouselSlide
