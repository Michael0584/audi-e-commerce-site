import "./newest-product-carousel.scss";
import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import Whirligig from "react-whirligig";

//to do: fix arrow responsive size
//to do: if mobile, show 1 slide only
//to do: add to cart btn functionality

// documentation: https://jane.github.io/react-whirligig/
// init whirligig:
let whirligig;

class WhirligigSlider extends Component {
	constructor(props) {
        super(props);
        this.state = {
            newestProducts: [],
            hoverToggle : false
        };
	}
    async componentWillMount() {
		const response = await fetch(
			`http://localhost:3000/api/products/collections/newest_product`
		);
		const newestProducts = await response.json();
        this.setState({ newestProducts }, ()=>{console.log(this.state.newestProducts)});
    }

	prev = () => whirligig.prev();
    next = () => whirligig.next();

    numberWithCommas = (number) => {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    convertStringToJson = (string) => {
        return JSON.parse(string);
    };
    
    getImagePath = (productImages) => {
        return Object.keys(productImages).map((path) => productImages[path]);
    };

	render() {
		return (
			<Row className='whirligigWrapper'>
				<Col xs='1' className='d-flex justify-content-center'>
					<button className='leftArrow' onClick={this.prev}>
						<img className='leftArrowIcon' src='/img/icons/arrow-up-large.png' alt='' />
					</button>
				</Col>
				<Col xs='10' className='d-flex justify-content-center'>
					<Whirligig
						className='whirligig'
						slideClass='whirligigSlides'
						visibleSlides={3}
						gutter='5vw'
						infinite='true'
						preventScroll='true'
						preventSwipe='false'
						ref={(_whirligigInstance) => {
							whirligig = _whirligigInstance;
						}}
					>
						{this.state.newestProducts.map((product, i) => {
								/* 
                                    the images are stored as json obj in the database so as to 
                                    allow multiple images to be accessed from a single data column. 
                                    To access it, you need to use JSON.parse() first or else the output will be
                                    in string prop type.
                                */
								let productImages = this.convertStringToJson(product.image_path);

								//here, get the image path of the product:
								let getImagePath = this.getImagePath(productImages);
								return (
									<div key={`similarProduct${i}`} className={`item item${i + 1}`}>
										{/* get the first image object's path by getting first index of the array: */}
										<a
											className='productLinkHome'
											href={`/products/collections/${product.category_id}/${product.sub_category_id}/${product.product_sid}`}
										>
											<img src={`/img${getImagePath[0]}`} />
										</a>
										<div className='contentWrap'>
											<a
												className='productLinkText'
												href={`/products/collections/${product.category_id}/${product.sub_category_id}/${product.product_sid}`}
											>
												<p className='similarProductName'>{product.product_name}</p>
												<p className='similarProductPrice'>
													${this.numberWithCommas(product.price)}
												</p>
											</a>
											<a
												href={`/products/collections/${product.category_id}/${product.sub_category_id}/${product.product_sid}`}
											>
												<button
													className='CTABtn aui-button aui-button--primary aui-js-response'
													type='button'
												>
													立即鑒賞
												</button>
											</a>
										</div>
									</div>
								);
							})}
					</Whirligig>
				</Col>
				<Col xs='1' className='d-flex justify-content-center'>
					<button className='rightArrow' onClick={this.next}>
						<img className='rightArrowIcon' src='/img/icons/arrow-up-large.png' alt='' />
					</button>
				</Col>
			</Row>
		);
	}
}

export default WhirligigSlider;
