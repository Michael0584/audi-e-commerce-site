import "./navbar.scss";
import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import ShoppingCartPopover from "./shoppingCartPopover/shoppingCartPopover";
import NotificationBadge from "./notification/notification";
import { connect, setReadCount } from "../../store";
// to do: notifications for each of the icons

class Navbar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active                 : false,
			inboxNotifications     : 0,
			favoritesNotifications : 0,
		};
	}

	componentWillMount() {
		setReadCount();
		window.setInterval(()=>{
			fetch('http://localhost:3000/user/following',{ credentials: 'include' })
			.then(res => res.json())
			.then(data =>{
				let total = 0
				for(let i in data){
				  if(data[i].member_email === this.props.profile.email){
					total +=1
				  }else{
					total =0
				  }
				  this.setState({favoritesNotifications:total})
				}
			})
		},500);
	  
    }	

	toggleClass = () => {
		const currentState = this.state.active;
		this.setState({
			active : !currentState
		});
	};

	loginCheck = (e) => {
		const currentState = this.state.active;
		if(!this.props.profile.isLogin){
			e.preventDefault()
			window.location.href = "/login"
		}
		this.setState({
			active : !currentState
		});
	}

	handleClick = (e) => {
		if (!this.props.profile.isLogin) {
			e.preventDefault();
			window.location.href = "/login";
		}
	};

	backToLastPage = () => {
        if (window.history.state == null) {
            window.location.href = "/shopping-cart"
        } else {
            window.history.back();
        }
	};

	favoritesLoginCheck = (e) => {
		if(!this.props.profile.isLogin){
			e.preventDefault()
			window.location.href = "/login"
			alert("需會員登入方可查看收藏")
		}
	}

	render() {
		return (
			<React.Fragment>
				<header>
					<nav className='Navbar'>
						<div className='horizontalNavbar'>
							<div className='left' />
							<div className='center'>
								<Link to='/'>
									<img id='logo' src='/img/logo.svg' alt='Audi Logo' />
								</Link>
							</div>
							<div className='right'>
								<Link onClick={this.handleClick} to='/membercenter/inbox' className='iconWrapper'>
									<img
										className='navIcons'
										src='/img/icons/user-large.svg'
										alt='Member Center Icon'
									/>
									<NotificationBadge count={this.props.unreadCount} />
								</Link>
								<Link onClick={this.favoritesLoginCheck} to='/membercenter/favorites' className='iconWrapper'>
									<img
										className='navIcons'
										src='/img/icons/favorite-large.svg'
										alt='Favorites Icon'
									/>
									<NotificationBadge count={this.state.favoritesNotifications} />
								</Link>
								<ShoppingCartPopover />
							</div>
						</div>
						<div className={"verticalNavbar " + (this.state.active ? "open" : null)}>
							<div className={"top " + (this.state.active ? "open" : null)} onClick={this.toggleClass}>
								<div id='nav-icon' className={this.state.active ? "open" : null}>
									<span />
									<span />
									<span />
									<span />
								</div>
							</div>
							<div className={"center " + (this.state.active ? "open" : null)}>
								{window.location.pathname === "/" ||
								window.location.pathname === "/home" ||
								window.location.pathname === "/index" ? (
									<a href='#landing' className={this.state.active ? "open" : null}>
										<img
											id='arrowIcon'
											src='/img/icons/arrow-straight-up-large-2x.png'
											alt='Back to previous page arrow icon'
										/>
									</a>
								) : (
									<div onClick={() => this.backToLastPage()} className={this.state.active ? "open" : null}>
										<img
											id='arrowIcon'
											src='/img/icons/arrow-left-normal-large-2x.png'
											alt='Back to previous page arrow icon'
										/>
									</div>
								)}

								<ul id='expandedLinks' className={"top " + (this.state.active ? "open" : null)}>
									<Link to='/' onClick={this.toggleClass}>
										<li>Home</li>
									</Link>
									<Link to='/products/collections' onClick={this.toggleClass}>
										<li>
											Audi Collections<br />原廠精品
										</li>
									</Link>
									<Link to='/products/accessories' onClick={this.toggleClass}>
										<li>
											Audi Genuine Accessories<br />原廠配件
										</li>
									</Link>
									<Link to='/promotion' onClick={this.toggleClass}>
										<li>最新優惠</li>
									</Link>
									<Link to='/membercenter/inbox' onClick={this.loginCheck}>
										<li>會員專區</li>
									</Link>
								</ul>
							</div>
							<div className='bottom'>
								<a href='https://www.youtube.com/channel/UCXVnWQwWs0p4VGsNw_d9CJw'>
									<img
										className='socialMediaIcon'
										src='/img/icons/system-youtube-large.svg'
										alt='Youtube icon'
									/>
								</a>
								<a href='https://www.instagram.com/audi_tw/'>
									<img
										className='socialMediaIcon'
										src='/img/icons/system-instagram-large.svg'
										alt='Instagram icon'
									/>
								</a>
								<a href='https://www.facebook.com/AudiTaiwan/'>
									<img
										className='socialMediaIcon'
										id='facebookIcon'
										src='/img/icons/system-facebook-large.svg'
										alt='Facebook icon'
									/>
								</a>
							</div>
						</div>
					</nav>
				</header>
			</React.Fragment>
		);
	}
}

export default connect(Navbar);
