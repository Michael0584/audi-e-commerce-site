import "./notification.scss";
import React, { Component } from "react";

class NotificationBadge extends Component {
	constructor(props) {
		super(props);	
		
	}
	


	render() {
		if(this.props.count == undefined || this.props.count ==0){
			return(
				<div className="notificationBadge">
                	<span  className='aui-badge is-hidden aui-color-black aui-color-text-light'></span>
				</div>

			)
		}else{
			return (
				<div className="notificationBadge">	
					<span id="badge1" className='aui-badge  aui-color-black aui-color-text-light'>{this.props.count}</span>	
				</div>
			);

		}
		
	}
}

export default NotificationBadge;
