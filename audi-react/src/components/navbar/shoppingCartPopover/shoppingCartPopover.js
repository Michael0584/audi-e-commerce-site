import "./shoppingCartPopover.scss";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Popover, PopoverHeader, PopoverBody } from "reactstrap";
import NotificationBadge from ".././notification/notification";

class ShoppingCartPopover extends Component {
	constructor(props) {
		super(props);
		this.numberWithCommas = (number) => {
			return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		};
		this.toggle = this.toggle.bind(this);
		this.state = {
			popoverOpen : false,
			itemsInCart : [],
			cartLength  : 0
		};
	}

	componentDidMount() {
		window.setInterval(()=>{
			if (localStorage.getItem("cart") != null) {
				let cartLength = JSON.parse(localStorage.getItem("cart")).length;
				this.setState({
					itemsInCart : JSON.parse(localStorage.getItem("cart")),
					cartLength  : cartLength
				});
			}	
		})
		
	}

	toggle() {
		this.setState({
			itemsInCart : JSON.parse(localStorage.getItem("cart")),
			popoverOpen : !this.state.popoverOpen
		});
	}

	render() {
		//判斷購物車内是否有商品，以條件判斷render那個版本
		if (localStorage.getItem("cart") && localStorage.getItem("cart") !== "[]") {
			return (
				<a className='iconWrapper'>
					<img
						className='navIcons shoppingBasketIcon'
						id='Popover'
						src='/img/icons/shopping-basket-large.svg'
						alt='Shopping Cart Icon'
						onClick={this.toggle}
					/>
					<NotificationBadge count={this.state.cartLength} id='lol' />
					<Popover
						placement='bottom-end'
						boundariesElement='Popover'
						isOpen={this.state.popoverOpen}
						target='Popover'
						toggle={this.toggle}
					>
						<PopoverHeader className=' popoverHeader text-center'>購物車</PopoverHeader>
						<PopoverBody className='popoverBody' id="#popoverBody">
							<nav className='aui-nav aui-js-nav aui-nav--list'>
								{this.state.itemsInCart.map((product, i) => (
									<ul key={i} className='aui-nav__items shoppingList'>
										<li className='aui-nav__item'>
											<a href={product.url} className='aui-nav__action'>
												<div className='productItemWrapper'>
													<img
														className='itemImage'
														src={"/img" + product.product_img.listingImg}
														alt='商品圖片'
													/>

													<div className='itemInfo'>
														<p key={product[i]} className='productName'>
															{product.product_name}
														</p>
														<p key={product[i]} className='price'>
															價格: ${this.numberWithCommas(product.product_price * 1)}
														</p>
														<p key={product[i]} className='quantity'>
															x{product.quantity}
														</p>
													</div>
												</div>
											</a>
										</li>
									</ul>
								))}
							</nav>
							<div className='text-center'>
								<Link to='/shopping-cart'>
									<button
										className='aui-button aui-button--primary aui-button--stretched aui-js-response shoppingCartBtn'
										type='button'
									>
										結賬
									</button>
								</Link>
							</div>
						</PopoverBody>
					</Popover>
				</a>
			);
		} else {
			//購物車空的版本
			return (
				<div className='iconWrapper'>
					<img
						className='navIcons shoppingBasketIcon'
						id='Popover'
						src='/img/icons/shopping-basket-large.svg'
						alt='Shopping Cart Icon'
						onClick={this.toggle}
					/>
					<NotificationBadge />
					<Popover
						placement='bottom'
						boundariesElement='viewport'
						isOpen={this.state.popoverOpen}
						target='Popover'
						toggle={this.toggle}
					>
						<PopoverHeader className='popoverHeader text-center pt-3'>您的購物車是空的</PopoverHeader>
						<PopoverBody className='popoverBody'>
							<div className='text-center'>
								<a href='/#shop' className=" aui-button aui-button--primary aui-button--stretched aui-js-response shoppingCartBtn">
									開始購物
								</a>
							</div>
						</PopoverBody>
					</Popover>
				</div>
			);
		}
	}
}

export default ShoppingCartPopover;
