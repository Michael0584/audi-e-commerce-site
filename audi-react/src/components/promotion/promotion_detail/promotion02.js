import React, { Component } from "react";
import "./promotion_detail.scss";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import pg02 from './banner-carousel/2.jpg';
import jack from './banner-carousel/item01.jpg';
import jack02 from './banner-carousel/item02.jpg';
import jack03 from './banner-carousel/item03.jpg';

class Promotion02 extends Component {

	render() {
		return (
            <div className="promoHeader" >
                <div className="promoPhoto">
                    <img src={pg02} />
                </div>
                <div className="promoText">
                    <p className="promoTextT">Audi 原廠精品優惠</p>
                    <p className="promoTextM">當運動休閒風成為時尚代名詞一環，意味著生活充滿更多可能性。Audi原廠精品為您獻上多款生活休</p>
                    <p className="promoTextM">閒精選，簡潔俐落的設計動靜皆宜，適合每個富活力且注重細節的您，盡情享受探索生活的樂趣。</p>
                    <p className="promoTextM">活動期間：2018/10/1-2018/12/31</p> 
                </div>
                <div className="promoTextOther">
                    <div className="promoItem01">
                            <div className="promoItem01L">
                                <img className="item01" src={jack} />
                            </div>
                            <div className="promoItem01R">
                                <p className="jacktext01">Audi鋪棉夾克</p>
                                <p className="jacktext02">優惠價NT$9,588 (原價NT$12,276)</p>
                                <p className="jacktext02">黑與灰內外相襯，低調耀眼卻不失焦點。菱格紋設計、簡潔灰色內面，以紅色</p>
                                <p className="jacktext02">車縫線凸顯出不容小覷的細節，並與Audi的賽道精神相互呼應。</p>
                            </div>
                        </div>
                    <div className="promoItem02">
                            <div className="promoItem02L">
                                <img className="item02" src={jack02} />
                            </div>
                            <div className="promoItem02R">
                                <p className="jack02text01">Audi Sport透氣立領運動外套</p>
                                <p className="jack02text02">優惠價NT$5,888 (原價NT$8,751)</p>
                                <p className="jack02text02">特殊材質設計，簡潔俐落兼顧時尚與實用性。紅色內面承襲Audi Sport的性能</p>
                                <p className="jack02text02">霸氣魅力，黑色拉鍊搭配紅色搶眼細節，無不彰顯對駕馭的渴望。</p>
                            </div>
                        </div>
                    <div className="promoItem03">
                            <div className="promoItem03L">
                                <img className="item03" src={jack03} />
                            </div>
                            <div className="promoItem03R">
                                <p className="jack03text01">Audi運動馬克杯</p>
                                <p className="jack03text02">優惠價NT$9,588 (原價NT$12,276)</p>
                                <p className="jack03text02">奧迪運動菱形完美搭配啞光白瓷</p>
                                <p className="jack03text02">菱形完美搭配啞光白瓷，適用於家庭或辦公室的時尚配件</p>
                            </div>
                            
                        </div>
                    <div className="promoItem04">
                            <div className="promo_btn">
                                <Link to="/home">
                                    <p className="promo_back">BACK</p>
                                </Link>
                            </div>
                        </div>
                </div>
            </div>
        )
	}
}

export default Promotion02;