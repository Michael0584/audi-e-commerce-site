import React, { Component } from "react";
import "./promotion_detail01.scss";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import { Container, Row, Col } from "reactstrap";
import pg01 from "./banner-carousel/7.jpg";
import Tire01 from "./banner-carousel/a1_tire01.jpg";
import Tire02 from "./banner-carousel/a3_tire02.jpg";
import Tire03 from "./banner-carousel/a4_tire03.jpg";
import Tire04 from "./banner-carousel/a5_tire04.jpg";
import Tire05 from "./banner-carousel/a6_tire05.jpg";
import Tire06 from "./banner-carousel/a7_tire06.jpg";

class Promotion02 extends Component {
	render() {
		return (
			<React.Fragment>
				<Container fluid={true} className='promoKV'>
					<Row>
						<Col className='promoKV1'>
							<img src={pg01} />
						</Col>
					</Row>
				</Container>
				<Container className='promoContent'>
					<Row>
						<Col className='promoHeadlines'>
							<h2 className='promoText'>Audi 原廠配件優惠</h2>
							<h3 className='promoText'>再創駕馭魅力</h3>
							<p className='promoText'>Audi原廠升級鋁圈，含輪胎優惠全面7折起</p>
							<p className='promoText'>活動期間：2018/10/1-2018/12/31</p>
						</Col>
					</Row>

					<Row>
						<Col className='promoDescription'>
							<p className='promoText'>
								Audi為您創造最貼近完美的駕馭體驗。Audi原廠鋁圈從設計、性能至耐用度皆提供無可挑剔的完美表現，並且具備獨有特色。嶄新輪輻科技與專屬顏色，皆與Audi各車款完美搭配。值得信賴的卓越工藝與測試認證，符合歐盟標準，滿足您對駕馭的渴望並悸動您內心的賽道精神。
							</p>
							<p className='promoText'>極致優異的耐用度</p>
							<p className='promoText'>> 頂級材料與傑出的鑄造品質，延長鋁圈壽命與絕佳性能表現。</p>
							<p className='promoText'>> 熱處理程序 (硬化) 提升鋁圈強度。 </p>
							<p className='promoText'>> Audi獨家多層塗裝技術，確保鋁圈在極端惡劣天候條件下也不會受損。</p>
						</Col>
					</Row>

					<Row>
						<Col className='promoContent'>
							<p className='promoContentHeading'>優惠內容</p>
							<div className='promoItems'>
								<div className='promoItem'>
									<img className='itemImg' src={Tire01} />
									<div className='textBox'>
										<p className='promoTextM'>A1 鋁圈</p>
										<p className='promoTextS'>建議售價: $12,400</p>
										<p className='promoTextS'>優惠售價: $9,800</p>
										<Link to='/products/accessories/2/2/5' className='aui-button aui-button--primary aui-js-response'>
											立即鑒賞
										</Link>
									</div>
								</div>
								<div className='promoItem'>
									<img className='itemImg' src={Tire02} />
									<div className='textBox'>
										<p className='promoTextM'>A3 鋁圈</p>
										<p className='promoTextS'>建議售價: $12,400</p>
										<p className='promoTextS'>優惠售價: $9,800</p>
										<Link to='/products/accessories/3/3/9' className='aui-button aui-button--primary aui-js-response'>
											立即鑒賞
										</Link>
									</div>
								</div>
								<div className='promoItem'>
									<img className='itemImg' src={Tire03} />
									<div className='textBox'>
										<p className='promoTextM'>A4 鋁圈</p>
										<p className='promoTextS'>建議售價: $12,000</p>
										<p className='promoTextS'>優惠售價: $8,800</p>
										<Link to='/products/accessories/4/4/91' className='aui-button aui-button--primary aui-js-response'>
											立即鑒賞
										</Link>
									</div>
								</div>
								<div className='promoItem'>
									<img className='itemImg' src={Tire04} />
									<div className='textBox'>
										<p className='promoTextM'>A5 鋁圈</p>
										<p className='promoTextS'>建議售價: $12,400</p>
										<p className='promoTextS'>優惠售價: $8,800</p>
										<Link to='/products/accessories/5/5/18' className='aui-button aui-button--primary aui-js-response'>
											立即鑒賞
										</Link>
									</div>
								</div>
								<div className='promoItem'>
									<img className='itemImg' src={Tire05} />
									<div className='textBox'>
										<p className='promoTextM'>A6 鋁圈</p>
										<p className='promoTextS'>建議售價: $12,000</p>
										<p className='promoTextS'>優惠售價: $8,800</p>
										<Link to='/products/accessories/6/6/23' className='aui-button aui-button--primary aui-js-response'>
											立即鑒賞
										</Link>
									</div>
								</div>
								<div className='promoItem'>
									<img className='itemImg' src={Tire06} />
									<div className='textBox'>
										<p className='promoTextM'>A7 鋁圈</p>
										<p className='promoTextS'>建議售價: $10,400</p>
										<p className='promoTextS'>優惠售價: $7,850</p>
										<Link to='/products/accessories/7/7/26' className='aui-button aui-button--primary aui-js-response'>
											立即鑒賞
										</Link>
									</div>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
			</React.Fragment>
		);
	}
}

export default Promotion02;
