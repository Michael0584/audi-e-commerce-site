import React, { Component } from "react";
import "./promotion_detail03.scss";
import { BrowserRouter as Link} from "react-router-dom";
import pg01 from './banner-carousel/8.png';
import bottle01 from './banner-carousel/bottle01.jpg';
import bottle02 from './banner-carousel/bottle02.jpg';
import bottle03 from './banner-carousel/bottle03.jpg';
import bottle04 from './banner-carousel/bottle04.jpg';
import bottle05 from './banner-carousel/bottle05.jpg';

class Promotion02 extends Component {

	render() {
		return (
            <div className="promoHeader03" >
                <div className="promoPhoto03">
                    <img src={pg01} />
                </div>
                <div className="promoText03">
                    <p className="promoTextL">Audi精品水壺優惠</p>
                    <p className="promoTextM">適用於家庭或辦公室的時尚配件</p>
                    <p className="promoTextM">Audi精品水壺，全面優惠7折起</p>
                    <p className="promoTextM">活動期間：2018/10/1-2018/12/31</p> 
                    <p className="promoTextS">起床，準備好，並使用我們的奧迪Sport Velocity Mug走上賽道。</p>
                    <p className="promoTextS">奧迪紅色外部裝飾有奧迪運動標誌。在快車道上開始新的一天。</p>
                    <br></br>
                    <br></br>
                    <p className="promoTextM">優惠內容</p>
                </div>
                <div className="promoTextOther03">
                    <div className="promoItem03">
                        <div className="promoItem03L">
                            <img className="item04" src={bottle01} />
                            <p className="promoTextM">Contigo Sheffield 瓶</p>
                            <p className="promoTextS">原建議售價 128,592</p>
                            <p className="promoTextS">> 優惠售價 90,000</p>
                        </div>
                        <div className="promoItem03M">
                            <img className="item01" src={bottle02} />
                            <p className="promoTextM">Schwarz 16oz 杯</p>
                            <p className="promoTextS">原建議售價 200,744</p>
                            <p className="promoTextS">> 優惠售價 143,000</p>
                        </div>
                        <div className="promoItem03R">
                            <img className="item01" src={bottle03} />
                            <p className="promoTextM">Wasser 瓶</p>
                            <p className="promoTextS">原建議售價 158,684</p>
                            <p className="promoTextS">> 優惠售價 113,000</p>
                        </div>
                    </div>

                    <div className="promoItem03">
                        <div className="promoItem03L">
                            <img className="item04" src={bottle04} />
                            <p className="promoTextM">奧迪運動 Velocity 馬克杯</p>
                            <p className="promoTextS">原建議售價 231,184</p>
                            <p className="promoTextS">> 優惠售價 164,500</p>
                        </div>
                        <div className="promoItem03M">
                            <img className="item01" src={bottle05} />
                            <p className="promoTextM">奧迪運動馬克杯</p>
                            <p className="promoTextS">原建議售價 133,320</p>
                            <p className="promoTextS">> 優惠售價 95,500</p>
                        </div>

                    </div>

                    <div className="promoItem04">
                            <div className="promo_btn">
                                <Link to="/home">
                                    <p className="promo_back">BACK</p>
                                </Link>
                            </div>
                    </div>
                </div>
            </div>
        )
	}
}

export default Promotion02;