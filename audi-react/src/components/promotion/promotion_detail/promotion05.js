import React, { Component } from "react";
import "./promotion_detail05.scss";
import { BrowserRouter as Link} from "react-router-dom";
import pg01 from './banner-carousel/9.png';
import coat01 from './banner-carousel/coat01.jpg';
import coat02 from './banner-carousel/coat02.jpg';
import coat03 from './banner-carousel/coat03.jpg';
import coat04 from './banner-carousel/coat04.jpg';
import coat05 from './banner-carousel/coat05.jpg';
import coat06 from './banner-carousel/coat06.jpg';

class Promotion02 extends Component {

	render() {
		return (
            <div className="promoHeader05" >
                <div className="promoPhoto05">
                    <img src={pg01} />
                </div>
                <div className="promoText05">
                    <p className="promoTextL">冬季女裝熱銷款式優惠</p>
                    <p className="promoTextM">注入當代女性的獨立與優雅魅力</p>
                    <p className="promoTextM">回歸極簡的衣著哲學</p>
                    <p className="promoTextM">整合歐美最尖端的時下潮流</p> 
                    <p className="promoTextS">Adventure Packable Jacket永遠不會讓元素讓你失望</p>
                    <p className="promoTextS">隨身攜帶便攜包裝在左手口袋中，因此您可以隨意佩戴</p>
                    <br></br>
                    <br></br>
                    <p className="promoTextM">優惠內容</p>
                </div>
                <div className="promoTextOther05">
                    <div className="promoItem05">
                        <div className="promoItem01L">
                            <img className="item05" src={coat01} />
                            <p className="promoTextM">Adventure Packable 夾克</p>
                            <p className="promoTextS">原建議售價 128,592</p>
                            <p className="promoTextS">> 優惠售價 90,000</p>
                        </div>
                        <div className="promoItem05M">
                            <img className="item05" src={coat02} />
                            <p className="promoTextM">Angled Zip 夾克</p>
                            <p className="promoTextS">原建議售價 200,744</p>
                            <p className="promoTextS">> 優惠售價 143,000</p>
                        </div>
                        <div className="promoItem05R">
                            <img className="item05" src={coat03} />
                            <p className="promoTextM">Cutter _ Buck Mason 夾克</p>
                            <p className="promoTextS">原建議售價 158,684</p>
                            <p className="promoTextS">> 優惠售價 113,000</p>
                        </div>
                    </div>

                    <div className="promoItem05">
                        <div className="promoItem05L">
                            <img className="item05" src={coat04} />
                            <p className="promoTextM">Moto 夾克</p>
                            <p className="promoTextS">原建議售價 231,184</p>
                            <p className="promoTextS">> 優惠售價 164,500</p>
                        </div>
                        <div className="promoItem05M">
                            <img className="item05" src={coat05} />
                            <p className="promoTextM">OGIO Cadmium 夾克</p>
                            <p className="promoTextS">原建議售價 133,320</p>
                            <p className="promoTextS">> 優惠售價 95,500</p>
                        </div>
                        <div className="promoItem05R">
                            <img className="item05" src={coat06} />
                            <p className="promoTextM">OGIO Intake 夾克</p>
                            <p className="promoTextS">原建議售價 237,184</p>
                            <p className="promoTextS">> 優惠售價 169,000</p>
                        </div>
                    </div>

                    <div className="promoItem04">
                            <div className="promo_btn">
                                <Link to="/home">
                                    <p className="promo_back">BACK</p>
                                </Link>
                            </div>
                    </div>
                </div>
            </div>
        )
	}
}

export default Promotion02;