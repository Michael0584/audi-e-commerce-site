import React, { Component } from "react";

const promotion_detail = ({ image }) => {
	const styles = {
		backgroundImage    : `url(${image})`,
		backgroundSize     : "cover",
		backgroundRepeat   : "no-repeat",
		backgroundPosition : "50% 60%",
	};
	return <div className='promotion_detail' style={styles} />;
};

export default promotion_detail