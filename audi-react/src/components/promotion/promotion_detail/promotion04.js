import React, { Component } from "react";
import "./promotion_detail04.scss";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import pg02 from './banner-carousel/10.png';
import toy01 from './banner-carousel/toy01.jpg';
import toy02 from './banner-carousel/toy02.jpg';
import toy03 from './banner-carousel/toy03.jpg';

class Promotion02 extends Component {

	render() {
		return (
            <div className="promoHeader04" >
                <div className="promoPhoto04">
                    <img src={pg02} />
                </div>
                <div className="promoText04">
                    <p className="promoTextT">Audi 兒童玩具優惠</p>
                    <p className="promoTextM">專業的玩具設計團隊、完善的生產車間</p>
                    <p className="promoTextM">標準化的生產流程、完備的物流設施、不斷創新的體系</p>
                    <p className="promoTextM">活動期間：2018/10/1-2018/12/31</p> 
                </div>
                <div className="promoTextOther04">
                    <div className="promoItem04">
                            <div className="promoItem04L">
                                <img className="item04" src={toy01} />
                            </div>
                            <div className="promoItem04R">
                                <p className="toyText01">Audi Sport Mini quattro</p>
                                <p className="toyText02">優惠價NT$9,588 (原價NT$12,276)</p>
                                <p className="toyText02">Junior Quattro Motorsport為初級車手提供賽道感受和駕駛樂趣</p>
                                <p className="toyText02">帶有正常運行的LED日間行車燈，可打開和關閉</p>
                            </div>
                        </div>
                    <div className="promoItem04">
                            <div className="promoItem04L">
                                <img className="item04" src={toy02} />
                            </div>
                            <div className="promoItem04R">
                                <p className="toyText01">Heritage Hoodie</p>
                                <p className="toyText02">優惠價NT$5,888 (原價NT$8,751)</p>
                                <p className="toyText02">大大小小的可愛伴侶 - 奧迪熊與原裝奧迪運動賽車服。</p>
                                <p className="toyText02">Motorsport以深棕色搭配淺鼻子和耳朵。賽車服由原始的奧迪運動賽車服重新創造。</p>
                            </div>
                        </div>
                    <div className="promoItem04">
                            <div className="promoItem04L">
                                <img className="item04" src={toy03} />
                            </div>
                            <div className="promoItem04R">
                                <p className="toyText01">Bumper the Bunny</p>
                                <p className="toyText02">優惠價NT$9,588 (原價NT$12,276)</p>
                                <p className="toyText02">這只可愛的小兔子高12英寸，耳朵鬆軟，尾巴蓬鬆，碳纖維圖案領結</p>
                                <p className="toyText02">緩慢而穩定不會贏得這場比賽。讓Bumper the Bunny開車讓你可以跳到它。</p>
                            </div>
                            
                        </div>
                    <div className="promoItem07">
                            <div className="promo_btn">
                                <Link to="/home">
                                    <p className="promo_back">BACK</p>
                                </Link>
                            </div>
                        </div>
                </div>
            </div>
        )
	}
}

export default Promotion02;