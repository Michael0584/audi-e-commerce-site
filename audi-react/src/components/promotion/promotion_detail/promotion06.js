import React, { Component } from "react";
import "./promotion_detail06.scss";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import pg02 from './banner-carousel/11.png';
import sun01 from './banner-carousel/sun01.jpg';
import sun02 from './banner-carousel/sun02.jpg';
import sun03 from './banner-carousel/sun03.jpg';

class Promotion02 extends Component {

	render() {
		return (
            <div className="promoHeader06" >
                <div className="promoPhoto06">
                    <img src={pg02} />
                </div>
                <div className="promoText06">
                    <p className="promoTextT">Audi 太陽眼鏡優惠</p>
                    <p className="promoTextM">自主開發各式機能性產品，包括光學級抗藍光眼鏡</p>
                    <p className="promoTextM">專為亞洲人設計的馳放偏光太陽眼鏡、夾片</p>
                    <p className="promoTextM">活動期間：2018/10/1-2018/12/31</p> 
                </div>
                <div className="promoTextOther06">
                    <div className="promoItem06">
                            <div className="promoItem06L">
                                <img className="item06" src={sun01} />
                            </div>
                            <div className="promoItem06R">
                                <p className="sunText01">Under Armour Glimpse 太陽眼鏡</p>
                                <p className="sunText02">優惠價NT$9,588 (原價NT$12,276)</p>
                                <p className="sunText02">風格和運動與Under Armour Glimpse太陽鏡完美融合。</p>
                                <p className="sunText02">100％防紫外線和ANSI Z87 +高級衝擊保護的運動員而設計</p>
                            </div>
                        </div>
                    <div className="promoItem06">
                            <div className="promoItem06L">
                                <img className="item06" src={sun02} />
                            </div>
                            <div className="promoItem06R">
                                <p className="sunText01">Under Armour Roll Out 太陽眼鏡
</p>
                                <p className="sunText02">優惠價NT$5,888 (原價NT$8,751)</p>
                                <p className="sunText02">Under Armour Roll Out太陽鏡時尚實用，提供100％防紫外線</p>
                                <p className="sunText02">款太陽鏡採用聚碳酸酯鏡片和柔美觸感。黑色。</p>
                            </div>
                        </div>
                    <div className="promoItem06">
                            <div className="promoItem06L">
                                <img className="item06" src={sun03} />
                            </div>
                            <div className="promoItem06R">
                                <p className="sunText01">Under Armour Shock 太陽眼鏡</p>
                                <p className="sunText02">優惠價NT$9,588 (原價NT$12,276)</p>
                                <p className="sunText02">100％防紫外線和ANSI Z87 +高級衝擊保護的運動員而設計</p>
                                <p className="sunText02">運動太陽鏡採用鈦金屬和Grilamid製成的耐用ArmourFusion框架</p>
                            </div>
                            
                        </div>
                    <div className="promoItem07">
                            <div className="promo_btn">
                                <Link to="/home">
                                    <p className="promo_back">BACK</p>
                                </Link>
                            </div>
                        </div>
                </div>
            </div>
        )
	}
}

export default Promotion02;