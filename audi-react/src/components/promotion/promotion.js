import React, { Component } from "react";
import "./promotion.scss";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import pg01 from './promotion_detail/banner-carousel/7.jpg';
import pg02 from './promotion_detail/banner-carousel/2.jpg';
import pg03 from './promotion_detail/banner-carousel/8.png';
import pg04 from './promotion_detail/banner-carousel/10.png';
import pg05 from './promotion_detail/banner-carousel/9.png';
import pg06 from './promotion_detail/banner-carousel/11.png';

class Promotion extends Component {

	render() {
		return (
			<div className="promotion_outout">
				<div className="promotion_out">
					<h1 className="promotion_yohue">最新優惠</h1>

					<div className="promotion_apart">
						<div className="promotion_part01">
							<div className="promotion_LF1" >
								<img className="promo01" src={pg01}/>
							</div>
							<div>
								<h3 className="promotion_titleName01"><b>Audi 原廠配件優惠</b></h3>
								<h4 className="promotion_slogan0101">Audi原廠升級鋁圈，含輪胎優惠全面7折起</h4>
								<Link to="/Promotion01" className="promotion_button01 aui-button aui-button--primary aui-js-response">
									立刻鑑賞
								</Link>
							</div>
						</div>

						<div className="promotion_part02">
							<div>
								<h3 className="promotion_titleName02"><b>Audi 原廠精品優惠</b></h3>
								<h4 className="promotion_slogan0201">盡情享受探索生活的樂趣</h4>
								<Link to="/Promotion02" className="promotion_button02 aui-button aui-button--primary aui-js-response">
									立刻鑑賞
								</Link>
							</div>
							<div className="promotion_RG1">
								<img className="promo02" src={pg02}/>
							</div>
						</div>
					</div>
					
					<div className="promotion_apart">
						<div className="promotion_part01">
							<div className="promotion_LF1" >
								<img className="promo03" src={pg03}/>
							</div>
							<div>
								<h3 className="promotion_titleName01"><b>Audi 精品水壺優惠</b></h3>
								<h4 className="promotion_slogan0101">適用於家庭或辦公室的時尚配件</h4>
								<Link to="/Promotion03" className="promotion_button01 aui-button aui-button--primary aui-js-response">
									立刻鑑賞
								</Link>
							</div>
						</div>

						<div className="promotion_part02">
							<div>
								<h3 className="promotion_titleName02"><b>Audi 兒童玩具優惠</b></h3>
								<h4 className="promotion_slogan0201">專業的玩具設計團隊、不斷創新的體系</h4>
								<Link to="/Promotion04" className="promotion_button02 aui-button aui-button--primary aui-js-response">
									立刻鑑賞
								</Link>
							</div>
							<div className="promotion_RG1">
								<img className="promo04" src={pg04}/>
							</div>
						</div>
					</div>

					<div className="promotion_apart">
						<div className="promotion_part01">
							<div className="promotion_LF1" >
								<img className="promo05" src={pg05}/>
							</div>
							<div>
								<h3 className="promotion_titleName01"><b>冬季女裝熱銷款式優惠</b></h3>
								<h4 className="promotion_slogan0101">注入當代女性的獨立與優雅魅力</h4>
								<Link to="/Promotion05" className="promotion_button01 aui-button aui-button--primary aui-js-response">
									立刻鑑賞
								</Link>
							</div>
						</div>

						<div className="promotion_part02">
							<div>
								<h3 className="promotion_titleName02"><b>Audi 太陽眼鏡優惠</b></h3>
								<h4 className="promotion_slogan0201">專為亞洲人設計的馳放偏光太陽眼鏡</h4>
								<Link to="/Promotion06" className="promotion_button02 aui-button aui-button--primary aui-js-response">
									立刻鑑賞
								</Link>
							</div>
							<div className="promotion_RG1">
								<img src={pg06}/>
							</div>
						</div>
					</div>

				</div>	
			</div>
		)
	}
	
}

export default Promotion;