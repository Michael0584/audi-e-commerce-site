import "./shopping-cart-page.scss";
import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import OrderPage from "../../order/cart-content/cart-content";
import { connect } from "../../../store";

class ShoppingCartPage extends Component {
	constructor(props) {
		super(props);
		this.numberWithCommas = (number) => {
			return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		};
		this.totalPrice = () => {
			let data = JSON.parse(localStorage.getItem("cart"));
			let totalPrice = 0;
			data.forEach((item) => {
				totalPrice += item.quantity * item.product_price;			
			});
			this.setState({
				total : totalPrice
			});
		};
		this.state = {
			data  : [],
			total : 0,
			url:"./login",
		};

		console.log(this);

		this.setData = () => {
			let data = this.state.data;
			let newData = localStorage.setItem("cart", JSON.stringify(data));
		};
	}

	componentDidMount() {
		this.getCart();
	}
	getCart(){
		if(this.props.profile.isLogin ===true){
			this.setState({
				url:"./order"
			})
		}
		let data = JSON.parse(localStorage.getItem("cart"));
		let totalPrice = 0;
		data.forEach((item) => {
			totalPrice += item.quantity * item.product_price;
			console.log(totalPrice);
			this.setState({
				total : totalPrice
			});
		});
		this.setState({
			data : data
		});

	}

	productCheckOut(item) {
		if (item != 0) {
			return "是";
		} else {
			return "否";
		}
	}

	decreaseQuantity = (item) => {
		if (item.quantity > 1) {
			item.quantity--;
			this.setData();
			this.totalPrice();
		}
	};

	increaseQuantity = (item) => {
		item.quantity++;
		this.setData();
		this.totalPrice();
	};

	remove = (item) => {
		console.log(item);
		let data = this.state.data;
		console.log(JSON.stringify(data));

		data.splice(data.indexOf(item), 1);
		console.log(data);
		let newData = localStorage.setItem("cart", JSON.stringify(data));
		// window.location.reload();
		this.getCart();
	};
	account =()=>{
		if(this.props.profile.isLogin ===true){
			window.location.href="/order"
		}else{
			window.location.href=this.state.url
		}	
	}

	render() {
		if (localStorage.getItem("cart") !== "[]") {
			return (
				<Container fluid={true} className='containerModifier'>
					<Row>
						<Col>
							<h1 className='headline'>購物車</h1>
						</Col>
					</Row>
					<Row className='cartContent'>
						<Col>
							<div className='aui-table aui-table--stretched'>
								<table>
									<thead className='cartContentTitle'>
										<tr>
											<th>商品</th>
											<th className='text-center'>顔色/材質</th>
											<th className='text-center'>大小</th>
											<th className='text-center'>數量</th>
											<th className='text-center'>金額</th>
											<th className='text-center'>需預約安裝*</th>
											<th className='text-center'>移除</th>
										</tr>
									</thead>
									{this.state.data.map((item, i) => (
										<tr key={i} className='cartItems cartItem00'>
											<td className='product'>
												<img
													src={"/img/" + item.product_img.listingImg}
													className='productImage'
													alt=''
												/>
												<p className='productName'>{item.product_name}</p>
											</td>
											<td className='color'>
												<p className='colorText text-center'>{item.color}</p>
											</td>
											<td className='size'>
												<p className='sizeText text-center'>{item.size}</p>
											</td>
											<td className='quantity'>
												<div className='flexWrap'>
													
														<button
															className='minusBtn'
															onClick={() => this.decreaseQuantity(item)}
														>
															-
														</button>
													
													<input
														className='aui-textfield__input quantityInputNumber'
														type='number'
														value={item.quantity}
														min='0'
														step='1'
														disabled
													/>
													
														<button
															className='plusBtn'
															onClick={() => this.increaseQuantity(item)}
														>
															+
														</button>
													
												</div>
											</td>
											<td className='price'>
												<p className='priceText text-center'>
													${this.numberWithCommas(item.product_price * item.quantity)}
												</p>
											</td>
											<td className='installation'>
												<p className='installationText text-center'>
													{this.productCheckOut(item.installation_required)}{" "}
												</p>
											</td>
											<td className='delete text-center'>
												
													<img
														onClick={() => this.remove(item)}
														className='deleteIcon'
														src='/img/icons/cancel-large-2x.png'
														alt=''
													/>
												
											</td>
										</tr>
									))}
								</table>
							</div>
						</Col>
					</Row>
					{/* <Row>
						<Col>
							<hr />
						</Col>
					</Row>
					<Row>
						<Col>
							<h1 className='headline'>你可能還喜歡</h1>
						</Col>
					</Row> */}
					<Row>
						<Col>
							<hr className='divisionLinesMarginBottom' />
						</Col>
					</Row>
					<Row>
						<Col xs='5' className='basketTotal'>
							<h2>購物車</h2>
							<hr />

							<div className='flexWrap'>
								<p className='title'>商品金額:</p>
								<p className='price'>${this.numberWithCommas(this.state.total)}</p>
							</div>
							<div className='flexWrap'>
								<p className='title'>預計寄送費用:</p>
								<p className='price'>${this.numberWithCommas(0)}</p>
							</div>
							{/* <div className='flexWrap'>
								<p className='title'>折扣:</p>
								<p className='price'>-${this.numberWithCommas(0)}</p>
							</div> */}
							<div className='flexWrap total'>
								<p className='title bold'>總金額:</p>
								<p className='price bold'>${this.numberWithCommas(this.state.total)}</p>
							</div>
							<hr />
						</Col>
						<Col xs='1' />
						<Col xs='6' className="rightColCheckOut">
							{/* <Row>
								<Col xs='6'>
									<div className='discountCoupon'>
										<div className='aui-textfield aui-js-textfield aui-textfield--floating-label'>
											<div className='aui-textfield__field'>
												<label htmlFor='couponCode'>優惠碼</label>
												<hr />
												<input
													className='aui-textfield__input'
													type='text'
													id='couponCode'
													placeholder='輸入優惠碼'
												/>
											</div>
										</div>
									</div>
								</Col>
							</Row> */}
							<Row>
								<Col xs='12' className='checkOutOptions'>
									<p>*部分車身配件需額外與指定奧迪經銷商預約安裝時間，將於結賬時提供該選項。</p>
									<div className='aui-button-group'>
										<button
											to={this.state.url}
											onClick ={()=>this.account()}
											className='checkOutBtn aui-js-response aui-button aui-button--primary'
										>
											結賬
										</button>
										<Link
											to='/#shop'
											className='keepShoppingBtn aui-js-response aui-button aui-button--secondary'
										>
											繼續購物
										</Link>
									</div>
								</Col>
							</Row>
						</Col>
					</Row>
				</Container>
			);
		} else {
			return (
				<Container fluid={true} className='containerModifier'>
					<Row>
						<Col>
							<h1 className='headline'>購物車</h1>
						</Col>
					</Row>
					<Row className="d-flex justify-content-center">
						<Col xs="6" className="emptyCartNoticeWrapper text-center d-flex flex-column justify-content-center align-items-center">
							<h2 className="emptyCartNotice">您的購物車是空的</h2>
							<Link to='/#shop'>
								<button
									className='aui-js-response aui-button aui-button--primary'
									type='button'
								>
									開始購物
								</button>
							</Link>
						</Col>
					</Row>
				</Container>
			);
		}
	}
}

export default connect(ShoppingCartPage);
