import "./accessories-products-listing.scss";
import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import Lazyload from "react-lazyload";

//to do: if fetched data is an empty array, show on client that there are no products in this category
//to do: pagination in the bottom, showing X amount of items per page.

class AccessoriesProductListings extends Component {
	constructor(props) {
		super(props);
		this.numberWithCommas = (number) => {
			return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		};
		this.convertStringToJson = (string) => {
			return JSON.parse(string);
		};
		this.state = {
			products      : [], //must set this as empty array or .map wont work
			triggerUpdate : ""
		};
	}

	componentDidMount() {
		//hacky solution until i find a better solution:
		//use set state within componentDidMount() to ensure that componentDidUpdate() is triggered:
		return this.setState({ triggerUpdate: null });
	}

	componentDidUpdate(prevProps, prevState) {
		fetch(`http://localhost:3000/products_db_api/${this.props.match.url}`)
			.then((response) => {
				//convert the fetched JSON data into string
				return response.json();
			})
			.then((obj) => {
				// console.log("did update:");
				// console.log(obj); //test

				// This condition prevents infinite loop caused by setState().
				// not sure why the condition works, come back later.
				if (this.state.products === prevState.products) {
					this.setState({ products: obj });
				}

				// console.log(this.state.products);
				return this.state.products;
			})
			.catch((error) => console.log(error));
	}

	render() {
		return (
			<React.Fragment>
				<Container className='productListingContainer'>
					<Row className='productRows'>
						{this.state.products.map((product, i) => (
							<Col key={i} xs='3' className='productWrapper'>
								<Link
									to={
										"/products/accessories/" +
										product.category_id +
										"/" +
										product.sub_category_id +
										"/" +
										product.product_sid
									}
								>
									{/* 
                        the images are stored as json obj in the database so as to 
                        allow multiple images to be accessed from a single data column. 
                        To access it, you need to use JSON.parse() first or else the output will be
                        in string prop type.
                    */}
									<Lazyload throttle={200} height={300}>
										<img
											src={`/img${this.convertStringToJson(product.image_path).listingImg}`}
											alt=''
											className='productImg'
										/>
									</Lazyload>
									<p className='productName'>{product.product_name}</p>
									<p className='productPrice'>{`$${this.numberWithCommas(product.price)}`}</p>
								</Link>
							</Col>
						))}
					</Row>
				</Container>
			</React.Fragment>
		);
	}
}

export default AccessoriesProductListings;
