import "./accessories-products-product-page.scss";
import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import ImageSlider from "./product-image-slider";
import SimilarProducts from "./similar-products";
import { connect } from "../../../store";


class AccessoriesProductPage extends Component {
	constructor(props) {
		super(props);
		this.numberWithCommas = (number) => {
			return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		};
		this.convertStringToJson = (string) => {
			return JSON.parse(string);
		};

		this.state = {
			productInfo        : [],
			productImages      : [],
			totalProductImages : 0,
			colors             : [],
			sizes              : [],
			quantity           : 1,
			active             : false,
			color							 :'',
			size							 :'',
			url								 :'',
			loginUrl                         :'login'
		};
		console.log(this)

		// console.log(this.props.location.pathname); //get current url location
		// console.log(this.props.match.params.product); //gets the product id from the url
		// console.log(this.props.match.params.category); //gets the category id from the url
		// console.log(this.props.match.params.subcategory); //gets the subcategory id from the url
	}

	//Fetch product info
	async componentWillMount() {
		const response = await fetch(
			`http://localhost:3000/products_db_api/products/accessories/:category/:subcategory/${this.props.match.params.product}`
		);
		const productInfo = await response.json();
		this.setState({ productInfo });

		/* 
            the images/sizes/colors are stored as json obj in the database so as to 
            allow multiple images/sizes/colors to be accessed from a single data column. 
            To access it, you need to use JSON.parse() first or else the output will be
            in string prop type.
        */

		this.setState({
			productImages : productInfo.map((product) => this.convertStringToJson(product.image_path)),
			sizes         : productInfo.map((product) => this.convertStringToJson(product.size_spec)),
			colors        : productInfo.map((product) => this.convertStringToJson(product.color))
		});

		//this state is to be passed to this slider child component
		//gives the total number of images available for this particular product
		this.setState({
			totalProductImages : Object.keys(this.state.productImages[0]).length
		});

		// console.log(this.state.productInfo);
		// console.log(this.state.productImages);
		// console.log(this.state.totalProductImages);
	}
	componentDidMount(){
		if(this.props.profile.email == undefined){
			this.setState({
				loginUrl:"login"
			})
		}
		
		fetch("http://localhost:3000/user/following",{
		 	  method: 'GET'
		   }).then(res => res.json())
		 	  .then(data => {
				   for(var i in data){
				   let member=data[i].member_email
				   let product = data[i].product_name
				   
				   if(member==this.props.profile.email && product ==this.state.productInfo[0].product_name){
						this.setState({
							active:true
						})
				   }
				}
		})  	
	}

	decreaseQuantity = () => {
		let quantity = this.state.quantity;
		if (quantity > 1) {
			this.setState({ quantity: quantity - 1 });
		}
	};

	increaseQuantity = () => {
		let quantity = this.state.quantity;
		this.setState({ quantity: quantity + 1 });
	};

	Handler(item, e) {
		if(this.props.profile.email !==undefined){
			e.preventDefault();
		const currentState = this.state.active;
		this.setState({
			active : !currentState
		});
		let toggle = this.state.active;
		
		let imgPath = JSON.parse(item.image_path).listingImg

		let following = {
			product_sid  : item.product_sid,
			product_name : item.product_name,
			price        : item.price,
			member_email : this.props.profile.email,
			product_img:imgPath
		};

		if (toggle === false) {
			fetch("http://localhost:3000/user/following", {
				method  : "POST",
				body    : JSON.stringify(following),
				headers : new Headers({
					"Content-Type" : "application/json"
				})
			})
				.then((res) => res.json())
				.then((data) => {
					// alert(data.message);
					// window.location.reload();
				});
		} else {
			fetch("http://localhost:3000/user/following", {
				method  : "DELETE",
				body    : JSON.stringify(following),
				headers : new Headers({
					"Content-Type" : "application/json"
				})
			})
				.then((res) => res.json())
				.then((data) => {
					// alert(data.message);
					// window.location.reload();
				});
		}
		
	}
		
	

	}
	setSize = (event) =>{
		this.setState({
			size:event.target.value
		})

	}

	setColor = (event) =>{
		this.setState({
			color:event.target.value
		})
	}

	addHandler(item,e){	
		e.preventDefault();
		
		let price =0;

		if(item.productInfo[0].discount_price === null){
			price = item.productInfo[0].price;
		}else{
			price = item.productInfo[0].discount_price;
		}
		// console.log(price)

		let cart ={
			product_name:item.productInfo[0].product_name,
			product_price:price,
			product_sid:item.productInfo[0].product_sid,
			color:item.color,
			size:item.size,
			quantity:item.quantity,
			accessories:item.productInfo[0].accessories,
			collections:item.productInfo[0].collections,
			product_img:item.productImages[0],
            url:this.props.match.url,
            installation_required: item.productInfo[0].installation_required,
		}
		let color = cart.color;
		let size = cart.size;


		if(color===""){
			alert("請選擇顏色")
		}else if(size===""){
			alert("請選擇尺寸")
		}else {
			if(localStorage.getItem("cart")){
				let data = JSON.parse(localStorage.getItem("cart"))
				var result = false;
				
					
				for(var i in data){
					if(data[i].product_name==cart.product_name && data[i].color == cart.color && data[i].size == cart.size){
						data[i].quantity=parseInt(data[i].quantity)+parseInt(cart.quantity);
						result = true;
						localStorage.setItem("cart",JSON.stringify(data));
					}
				}
				if(!result){
					data.push(cart);
					localStorage.setItem("cart",JSON.stringify(data));
				}
				
			}
			else{
				let data = []
				data.push(cart)
				localStorage.setItem("cart",JSON.stringify(data));
			}

			alert("已加入購物車");
			// window.location.reload();
		}

	}

	buyHandler(item,e){	
		if(this.props.profile.email !==undefined){
			e.preventDefault();
			
			let price =0;

			if(item.productInfo[0].discount_price === null){
				price = item.productInfo[0].price;
			}else{
				price = item.productInfo[0].discount_price;
			}
			// console.log(price)
			
			let cart ={
				product_name:item.productInfo[0].product_name,
				product_price:price,
				product_sid:item.productInfo[0].product_sid,
				color:item.color,
				size:item.size,
				quantity:item.quantity,
				accessories:item.productInfo[0].accessories,
				collections:item.productInfo[0].collections,
				product_img:item.productImages[0],
				url:this.props.match.url,
				installation_required: 0,

			}
			let color = cart.color;
			let size = cart.size;


			if(color===""){
				alert("請選擇顏色")
			}else if(size===""){
				alert("請選擇尺寸")
			}else {
				if(localStorage.getItem("cart")){
					let data = JSON.parse(localStorage.getItem("cart"))
					var result = false;
					
					for(var i in data){
						if(data[i].product_name==cart.product_name && data[i].color == cart.color && data[i].size == cart.size){
							data[i].quantity=parseInt(data[i].quantity)+parseInt(cart.quantity);
							result = true;
							localStorage.setItem("cart",JSON.stringify(data));
						}
					}
					if(!result){
						data.push(cart);
						localStorage.setItem("cart",JSON.stringify(data));
					}
					
				}else{
					let data = []
					data.push(cart)
					localStorage.setItem("cart",JSON.stringify(data));
				}
				window.location.href="/order"
			}
		}else{
			window.location.href="/login"
		}
		
	}
	



	render() {
		return (
			<React.Fragment>
				<Container className='containerModifier'>
					<Row>
						<Col>
							{this.state.productInfo.map((product, i) => (
								<h1 key={`productTitle${i}`} className='productName'>
									{product.product_name}
								</h1>
							))}
							<Row>
								<Col className='leftPanel'>
									<ImageSlider
										imagesToRender={this.state.productImages}
										totalProductImages={this.state.totalProductImages}
									/>
								</Col>
								<Col className='rightPanel'>
									<div className='price'>
										{//Price - determine whether it is discounted or not.
										this.state.productInfo.map((product, i) => {
											if (product.discount_status === 1) {
												return (
													<div key={`discountedPrice${i}`} className='discountedVersion'>
														<p className='discountedPrice'>
															NT ${this.numberWithCommas(product.discount_price)}
														</p>
														<p className='originalPrice'>
															NT ${this.numberWithCommas(product.price)}
														</p>
													</div>
												);
											} else {
												return (
													<div key={`nondiscountedPrice${i}`} className='originalVersion'>
														<p className='originalPrice'>
															NT ${this.numberWithCommas(product.price)}
														</p>
													</div>
												);
											}
										})}
									</div>
									<div className='productDescription'>
										{this.state.productInfo.map((product, i) => (
											<p key={`productDescription${i}`}>{product.description}</p>
										))}
									</div>
									<Row className='dropDownInputs'>
										<Col className='productSpec'>
											<div className='aui-select aui-js-select aui-select--floating-label'>
												<select  onChange={this.setColor} className='aui-select__input' id='colorSelect'>
													<option selected disabled className='aui-select__input-label'>
														顔色/材質:
													</option>
													{this.state.colors.map((color, i) => {
														let colorsArray = [];
														//for..in.. runs a loop over all properties within an object.
														//here, the sizes are stored inside an object within the array
														for (let property in color) {
															if (color.hasOwnProperty(property)) {
																colorsArray.push(
																	<option
																	key={property} value={color[property]}
																	>
																		{color[property]}
																	</option>
																);
															}
														}
														return colorsArray;
													})}
												</select>
											</div>
										</Col>
										<Col className='productSize'>
											<div className='aui-select aui-js-select aui-select--floating-label'>
												<select  onChange={this.setSize} className='aui-select__input' id='sizeSelect'>
													<option selected disabled className='aui-select__input-label'>
														大小:
													</option>
													{this.state.sizes.map((size, i) => {
														let sizesArray = [];
														//for..in.. runs a loop over all properties within an object.
														//here, the sizes are stored inside an object within the array
														for (let property in size) {
															if (size.hasOwnProperty(property)) {
																sizesArray.push(
																	<option value={size[property]} key={property}>
																		{size[property]}
																	</option>
																);
															}
														}
														return sizesArray;
													})}
												</select>
											</div>
										</Col>
									</Row>
									<Row className='quantity'>
										<Col className='quantityWrapper'>
											<div className='aui-textfield aui-textfield--floating-label'>
												<div className='aui-textfield__field quantityRow'>
													<button
														className='minusBtn hoverEffect'
														onClick={this.decreaseQuantity}
													>
														-
													</button>
													<input
														className='aui-textfield__input quantityInputNumber'
														type='number'
														value={this.state.quantity}
														min='0'
														step='1'
														disabled
													/>
													<button
														className='plusBtn hoverEffect'
														onClick={this.increaseQuantity}
													>
														+
													</button>
												</div>
											</div>
										</Col>
									</Row>
									<Row className='secondaryBtns'>
										<Col className='addToCart'>
											<button
												className='aui-button aui-button--secondary aui-js-response addToCartBtn'
												type='button'
												onClick={this.addHandler.bind(this,this.state)}
											>
												<img
													className='cartIcon'
													src='/img/icons/shopping-basket-large-2x.png'
													alt=''
													
												/>
												加入購物車
											</button>
										</Col>
										<Col className='addToFavorites'>
										<Link to = {"/"+this.state.loginUrl}>
											<button
												className='aui-button aui-button--secondary aui-js-response addToFavoriteBtn'
												type='button'
												onClick={this.Handler.bind(this, this.state.productInfo[0])}
											>
												<img
													className={"favoriteIcon " + (this.state.active ? "favorited" : "")}
													src='/img/icons/favorite-large-2x.png'
													alt=''
												/>
												<img
													className={"favoriteIcon " + (this.state.active ? "" : "favorited")}
													src='/img/icons/favorite-large-2x-filled.png'
													alt=''
												/>
												加入追蹤清單
											</button>
										</Link>	
										</Col>
									</Row>
									<Row className='primaryBtn'>
										<Col>
											<button
												className='checkOutBtn aui-js-response aui-button aui-button--primary aui-button--stretched'
												type='button'
												onClick={this.buyHandler.bind(this,this.state)}
											>
												直接購買
											</button>
										</Col>
									</Row>
								</Col>
							</Row>
						</Col>
					</Row>
				</Container>
                {/* parentCategories passes the url variables used (categories, subcategories, product id) */}
				<SimilarProducts parentCategories={this.props.match.params} thisProduct={this.props.match.params.product}/> 
			</React.Fragment>
		);
	}
}

export default connect(AccessoriesProductPage);
