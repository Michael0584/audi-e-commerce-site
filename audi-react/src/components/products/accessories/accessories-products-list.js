import "./accessories-products-list.scss";
import AccessoriesProductListings from "./accessories-products-listing";
import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import PropTypes from "prop-types";

//to do: on click, bring user back to top of listing.
class AccessoriesProductListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: []
    };
  }

  async componentWillMount() {
    const response = await fetch("http://localhost:3000/products_db_api/products/accessories/queryall")
    const categories = await response.json();
    this.setState({categories})
    // console.log(this.state.categories)
  }

  render() {
    return (
      <React.Fragment>
        <Container fluid={true}>
          <Row>
            <Col className='headlineWrapper'>
              <h1>商品列表</h1>
            </Col>
          </Row>
          <Row>
            <Col xs='2' className='filterNav'>
              <nav className='aui-nav aui-js-nav aui-nav--list'>
                <h1>篩選條件</h1>
                <ul className='aui-nav__items'>
                  {this.state.categories.map((category, i) => (
                    <li key={i} className='aui-nav__item'>
                      <NavLink
                        to={"/products/accessories/" + category.categoryID}
                        className='aui-nav__action aui-select aui-js-select'
                        activeClassName='is-active subCatDisplay'
                      >
                        {category.categoryName}
                      </NavLink>
                      {category.subCategories.map((subCategory, i) => (
                        <ul key={subCategory.sub_category_id} className='subCategoryNav aui-nav__items'>
                          <li className='aui-nav__item'>
                            <NavLink
                              to={
                                "/products/accessories/" +
                                category.categoryID +
                                "/" +
                                subCategory.sub_category_id
                              }
                              className='aui-nav__action'
                              activeClassName='is-active'
                            >
                              > {subCategory.sub_category_name}
                            </NavLink>
                          </li>
                        </ul>
                      ))}
                    </li>
                  ))}
                </ul>
              </nav>
            </Col>
            <Col xs='2' />
            <Col xs='10' className='productOutput'>
              <Route
                exact path='/products/accessories'
                component={AccessoriesProductListings} />
              <Route
                exact path='/products/accessories/:category'
                component={AccessoriesProductListings}
              />
              <Route
                path='/products/accessories/:category/:subcategory'
                component={AccessoriesProductListings}
              />
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}

export default AccessoriesProductListPage;
