import './collections-products-list.scss';
import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { BrowserRouter as Router, Route, Link,  NavLink } from 'react-router-dom';
import CollectionsProductListings from "./collections-products-listing";
// import PropTypes from "prop-types";
import { Container, Row, Col } from "reactstrap";



class CollectionsListPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            categories: []
        };
      
    
    }
  
    async componentWillMount() {
        const response = await fetch("http://localhost:3000/api/products/collections/queryall")
        const categories = await response.json();
        this.setState({categories})
    }
    
 

    
    render() {
        return (
            <React.Fragment> 
              <div className='container-fluid'>
                <Row>
                  <Col className='headlineWrapper'>
                    <h1>商品列表</h1>
                  </Col>
                </Row>
          <Row>
          <Col xs='2' className='filterNav'>
              <nav className='aui-nav aui-js-nav aui-nav--list'>
                <h1>篩選條件</h1>
                <ul className='aui-nav__items'>
                  {this.state.categories.map((category, i) => (
                    <li key={i} className='aui-nav__item'>
                      <NavLink
                          to={"/products/collections/" + category.categoryID}
                          className='aui-nav__action aui-select aui-js-select'
                          activeClassName='is-active subCatDisplay'
                        >
                        {category.categoryName}
                       </NavLink>
                    {category.subCategories.map((subCategory, i) => (
                    <ul key={subCategory.sub_category_id} className='subCategoryNav aui-nav__items'>
                        <li className='aui-nav__item'>
                        <NavLink
                              to={"/products/collections/" + category.categoryID+"/" +
                                                              subCategory.sub_category_id
                            }
                            className='aui-nav__action'
                            activeClassName='is-active'
                            >
                                > {subCategory.sub_category_name}
                          </NavLink>  
                          </li>
                      </ul>
                        ))}
                      </li>
                    ))}
                  </ul>
                </nav>
              </Col>
            <Col xs='2' />
            <Col xs='10' className='productOutput'>
              <Route
                  exact path='/products/collections'
                  component={CollectionsProductListings} />
              <Route
                exact
                path='/products/collections/:category'
                component={CollectionsProductListings}
              />
              <Route
                path='/products/collections/:category/:subcategory'
                component={CollectionsProductListings}
              />
            
            </Col>
            
            </Row>
          </div>
           
          </React.Fragment>
        )}
}

export default  CollectionsListPage;