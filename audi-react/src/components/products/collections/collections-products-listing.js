import "./collections-products-list.scss";
import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import Lazyload from 'react-lazyload';




//to do: if fetched data is an empty array, show on client that there are no products in this category
//to do: change image path of each product to array[0].image_path, and change the stored image_path on
//       DB as json.




class CollectionsProductListings extends Component {
  constructor(props) {
    super(props);
    this.numberWithCommas = (number) => {
      return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
    this.state = {
      products: [], //must set this as empty array or .map wont work
      triggerUpdate: "",
      totalPage:'',

    };
    this.convertStringToJson = (string) => {
      return JSON.parse(string)
  };
    console.log(this)
    
  }

  componentDidMount() {
    return this.setState({ triggerUpdate: null });
  }

  componentDidUpdate(prevProps, prevState) {
    // await fetch(`http://localhost:3000/products_db_api/products/accessories/${this.state.category}/${this.state.subcategory}`, {
    fetch(`http://localhost:3000/api/${this.props.match.url}`)
      .then((response) => {
      
        return response.json();
      })
      .then((obj) => {
       
        if (this.state.products === prevState.products) {
          this.setState({ products: obj });
        }

        // console.log(this.state.products);
        return this.state.products;
      })
      .catch((error) => console.log(error));
  }



  render() {
    return (
      <React.Fragment>
        <Container className='productListingContainer'>
          <Row className='productRows'>
         
          
            {this.state.products.map((product, i) => (
               
              <Col key={i} xs='3' className='productWrapper'>
                <Link
                  to={
                    "/products/collections/" +
                    product.categories_id +
                    "/" +
                    product.categories_sub_id +
                    "/" +
                    product.product_sid
                  }
                >
                <Lazyload throttle={200} height={300}>
                  
                      <img
                      
                        src={`/img${this.convertStringToJson(product.image_path).listingImg}`}
                        alt=''
                        className='productImg'
                      />
                  
                </Lazyload>
                  <p className='productName'>
                    {product.product_name}
                  </p>
                  <p
                    className='productPrice'
                  >{`$${this.numberWithCommas(product.price)}`}</p>
                </Link>
              </Col>
              
            ))}
            
          </Row>
          
            
        
        </Container>
        {/* <PaginationPage products = {this.state.products} totalPage ={this.state.totalPage}/> */}
      </React.Fragment>
      
    );
  }
}

export default CollectionsProductListings;
