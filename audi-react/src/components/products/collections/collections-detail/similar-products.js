import "./similar-products.scss";
import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import WhirligigSlider from "./similar-products-carousel";
class SimilarProducts extends Component {
	constructor(props) {
        super(props);
        this.state = {
            similarProducts: [],
        }
        console.log(this)
    }
    
    // fetch all products under this product's subcategory
    async componentWillMount() {
		const response = await fetch(
			`http://localhost:3000/api/products/collections/${this.props.parentCategories.category}/${this.props.parentCategories.subcategory}`
        );
        const similarProducts = await response.json();
        this.setState({ similarProducts });
    }


    

	render() {
        //判斷有沒有相似商品: 若無，則不render相似商品 
        if (this.state.similarProducts.length <= 1) {
            return null
        } else {
            return (
                <Container fluid={true} className='containerModifier collectionsModifier'>
                    <Row>
                        <Col>
                            <h1 className='headline'>相似商品</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            {/* pass fetched similar product infos to the slider component */}
                            <WhirligigSlider similarProducts ={this.state.similarProducts} thisProduct={this.props.thisProduct}></WhirligigSlider>
                        </Col>
                    </Row>
                </Container>
            );
        }        
	}
}

export default SimilarProducts;
