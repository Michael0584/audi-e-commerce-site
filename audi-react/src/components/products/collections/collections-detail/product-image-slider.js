// import "./product-image-slider.scss";
import React, { Component } from "react";
import Slide from "./product-image-slide";
import "./product-image-slider.scss";

// see https://medium.com/@ItsMeDannyZ/build-an-image-slider-with-react-es6-264368de68e4 for tutorial
// to do: make slider responsive when window is being dragged to size (currently only resizes on reload)

class ImageSlider extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// Note: the images are stored in an array passed from the parent component: this.props.imagesToRender
			currentSlide   : 0,
			translateValue : 0
		};
	}

	// componentWillReceiveProps(props) {
	//     this.setState({totalSlideNum: props.imagesToRender});
	//     console.log(this.state.totalSlideNum);
	// }

	goToPrevSlide = () => {
		if (this.state.currentSlide === 0) {
			return this.setState({
				currentSlide   : this.props.totalProductImages - 1,
				translateValue : -this.slideWidth() * (this.props.totalProductImages - 1)
			});
		}

		this.setState((prevState) => ({
			currentSlide   : prevState.currentSlide - 1,
			translateValue : prevState.translateValue + this.slideWidth()
		}));
		console.log("prev test");
	};

	goToNextSlide = () => {
		// Exiting the method early if we are at the end of the images array.
		// We also want to reset currentSlide and translateValue, so we return
		// to the first image in the array.
		if (this.state.currentSlide === this.props.totalProductImages - 1) {
			return this.setState({
				currentSlide   : 0,
				translateValue : 0
			});
		}

		// This will not run if we meet the if condition above (when we reach the last img)
		this.setState((prevState) => ({
			currentSlide   : prevState.currentSlide + 1,
			translateValue : prevState.translateValue + -this.slideWidth()
		}));

		// console.log(this.state.currentSlide);
	};

	slideWidth = () => {
		return document.querySelector(".slide").clientWidth;
	};

	render() {
		return (
			<React.Fragment>
				<div className='sliderComponentWrapper'>
					<div className='slider'>
						<div
							className='slider-wrapper'
							style={{
								transform  : `translateX(${this.state.translateValue}px)`,
								transition : `transform ease-out 0.45s`
							}}
						>
							{this.props.imagesToRender.map((image, i) => {
								let imagesArray = [];
								//for..in.. runs a loop over all properties within an object.
								//here, the images are stored inside an object within the array
								for (let imagePath in image) {
									if (image.hasOwnProperty(imagePath)) {
										imagesArray.push(<Slide key={imagePath} image={image[imagePath]} />);
									}
								}
								return imagesArray;
							})}
						</div>
					</div>
					<div className='arrows'>
						<div className='leftArrow' onClick={this.goToPrevSlide}>
							<img className='leftArrowIcon' src='/img/icons/arrow-up-large.png' alt='' />
						</div>
						<div className='rightArrow' onClick={this.goToNextSlide}>
							<img className='rightArrowIcon' src='/img/icons/arrow-up-large.png' alt='' />
						</div>
					</div>
					<div className='slidePosition'>
						<span className='currentSlide'>0{`${this.state.currentSlide + 1}`}</span>
						&nbsp;|&nbsp;
						<span className='totalSlide'>0{`${this.props.totalProductImages}`}</span>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default ImageSlider;
