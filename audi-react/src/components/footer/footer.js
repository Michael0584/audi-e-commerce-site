import "./footer.scss";
import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import { connect } from "../../store";
class Footer extends Component {
    loginCheck = (e) => {
		if(!this.props.profile.isLogin){
			e.preventDefault()
			window.location.href = "/login"
		}
	}
	render() {
		return (
			<div className='footerPositionRelativeLayer'>
				<Container fluid={true} className='footer'>
					<Row>
						<Col className='accessories'>
							<Link to='/products/accessories' className='title'>
								Audi Genuine <br /> Accessories 原廠配件
							</Link>
							<Link to='/products/accessories/1'>> Audi A1 配件</Link>
							<Link to='/products/accessories/2'>> Audi A3 配件</Link>
							<Link to='/products/accessories/3'>> Audi A4 配件</Link>
							<Link to='/products/accessories/4'>> Audi A5 配件</Link>
							<Link to='/products/accessories/5'>> Audi A6 配件</Link>
							<Link to='/products/accessories/6'>> Audi A7 配件</Link>
							<Link to='/products/accessories/7'>> Audi A8 配件</Link>
							<Link to='/products/accessories/8'>> Audi Q2 配件</Link>
							<Link to='/products/accessories/9'>> Audi Q3 配件</Link>
							<Link to='/products/accessories/10'>> Audi Q5 配件</Link>
							<Link to='/products/accessories/11'>> Audi Q7 配件</Link>
							<Link to='/products/accessories/12'>> Audi TT 配件</Link>
						</Col>
						<Col className='collections'>
							<Link to='/products/collections' className='title'>
								Audi Collections <br /> 原廠精品
							</Link>
							<Link to='/products/collections/1'>> 男士</Link>
							<Link to='/products/collections/2'>> 女士</Link>
							<Link to='/products/collections/3'>> 兒童</Link>
							<Link to='/products/collections/4'>> 其他</Link>
						</Col>
						<Col className='membercenter'>
							<Link onClick={this.loginCheck} to='/membercenter/inbox' className='title pb'>
								會員專區
							</Link>
							<Link onClick={this.loginCheck} to='/membercenter/inbox'>> 收件匣</Link>
							<Link onClick={this.loginCheck}to='/membercenter/contact'>> 問題回報</Link>
							<Link onClick={this.loginCheck} to='/membercenter/favorites'>> 收藏商品</Link>
							<Link onClick={this.loginCheck} to='/membercenter/order'>> 訂單查詢</Link>
							<Link onClick={this.loginCheck} to='/membercenter/profile'>> 會員資料</Link>
						</Col>
						<Col className='contactus'>
							<p className='title'>聯絡我們</p>
							<div className='operation'>
								<img src='/img/icons/mobile-phone-large-2x.png' alt='' className="phoneIcon"/>
								<div className='text'>
									<p>02-8321-5978</p>
									<p>09:00-18:00</p>
									<p>周一至周五</p>
								</div>
							</div>
							<div className='email'>
								<img src='/img/icons/direct-mail-large-2x.png' alt='' className="mailIcon"/>
								<a href='mailto:audi-store@audi.com.tw'>audi-store@audi.com.tw</a>
							</div>
						</Col>
					</Row>
					<Row>
						<Col className='socialMediaIcons' />
					</Row>
					<Row>
						<Col className='hrLine'>
							<hr />
						</Col>
					</Row>
					<Row>
						<Col className='copyright'>
							<div className='left'>
								<p>Audi Taiwan. All Rights Reserved. &copy; Copyright 2018</p>
							</div>
							<div className='right'>
								<a href='#'>隱私權政策</a>
								<span>&nbsp;|&nbsp;</span>
								<a href='#'>使用條款</a>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default connect(Footer);
