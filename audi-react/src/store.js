import React, { Component } from 'react';
import { getCookie } from "./cookie";

const store = {
    profile: {

    },
    isLogin: Boolean(getCookie('isLogin')),
    unreadCount: 0
};

const subscribers = []

export const setReadCount = async() => {
    const res = await fetch('http://localhost:3000/membercenter/notifications',{ credentials: 'include' })
    const unreadCount = await res.json()
    store.unreadCount = unreadCount
    updateSubscriber()
}


export const fetchProfile = async () => {
    const res = await fetch('http://localhost:3000/api/profile', { credentials: 'include' })
    const profile = await res.json()
    store.profile = profile
    updateSubscriber()
}

const updateSubscriber = () => subscribers.forEach(subscriber => subscriber(store))

export const subscribe = (subscriber) => {
    subscribers.push(subscriber)
}

const unsubscribe = (subscriber) => {
    subscribers.splice(subscribers.indexOf(subscriber), 1)
}

export const connect = MyComponent => {
    class ConnectedComponent extends Component {
        constructor(props) {
            super(props)
            this.state = store            
        }

        syncState = (store) => this.setState(store)

        componentDidMount() {
            subscribe(this.syncState)
        }

        componentWillUnmount(){
            unsubscribe(this.syncState)
        }

        render() {
            const mergedProps = { ...this.props, ...this.state }
            return <MyComponent {...mergedProps} />
        }
    }
    return ConnectedComponent
}

