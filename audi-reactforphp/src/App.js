import React, { Component } from 'react';
import ManagePlatforms from './manageplatforms/manageplatforms';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import SignIn from './manageplatforms/signin';
class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={SignIn} />
          <Route path="/manageplatforms" component={ManagePlatforms} />
        </div>

      </Router>

    )
  }
}


export default App;
