import React, { Component } from 'react';
import './promotion_set.scss';
import Konva from 'konva';
import { render } from 'react-dom';
import { Stage, Layer, Image , Star ,Text } from 'react-konva';
import pg01 from './promotion01.png';

class promotion_set extends Component {
  constructor(props) {
    super(props)
    this.keyIn = React.createRef();
    this.LinkIn = React.createRef();
    
    this.state = ({
        inner: "",
        selectedFile:null,
        img_name:"",
        image: null,
        rectangles: [
            {
              x: 10,
              y: 10,
              width: 100,
              height: 100,
              fill: 'black',
              name: 'rect1',
              cR:'30'
            },
            {
              x: 150,
              y: 150,
              width: 100,
              height: 100,
              fill: 'green',
              name: 'rect2',
              cR:'30'
            }
          ],
          selectedShapeName: '',
          text:"Audi 原廠風箱清潔服務優惠",
          slogan:"為您的車內空氣安心把關",
          Tsize:"50",
          Msize:"36",
          Ssize:'24',
          image: null,
          imgx:0,
          imgy:-50,
          titlex:50,
          titley:20,
          sloganx:150,
          slogany:90,
          detail01x:0,
          detail01y:0,
          detail02x:0,
          detail02y:0,
          detail03x:0,
          detail03y:0,
          detail04x:0,
          detail04y:0,
          detail05x:0,
          detail05y:0,
          detail06x:0,
          detail06y:0,
          detail07x:0,
          detail07y:0,
          detail08x:0,
          detail08y:0,
          Stage:"",
          promotion:[{gx:"",gy:"",tx:"",ty:"",sx:"",sy:"",tn:"Audi 原廠風箱清潔服務優惠",sn:"為您的車內空氣安心把關"}],
          detail01:"> 確保車內空氣清新。",
          detail02:"> 回復冷氣空調效能。",
          detail03:"> 有效去除空氣中的污染微粒。",
          detail04:"> 抑制微生物、黴菌滋生。",
          detail05:"> 清除異味。",
          detail06:"",
          detail07:"原價：NT$2,690，優惠價：NT$2,288（含稅，含工資）",
          detail08:"",
          
        })
    console.log(this);
}
To = (evt) => {
    evt.preventDefault();
    this.setState({
        titleName: document.querySelector('.title').innerHTML
        }, () => {
        let titleName = this.state.titleName;
        let promotion = this.state.promotion;
        promotion[0].TN = titleName
        console.log(this.state.promotion)
        // console.log("aaa:"+JSON.stringify(promotion))
    })
}
// preview = (evt) => {
//     evt.preventDefault();
//     this.setState({
//         inner: document.querySelector('.Left').innerHTML
//     }, () => {
//         let inner = this.state.inner;
//         let promotion = this.state.promotion;
//         promotion[0].IN = inner
//         //console.log(this.state.promotion)
//         console.log(JSON.stringify(promotion));
//     })
// }
push = () => {
    let promotion = this.state.promotion
    fetch("http://localhost:3000/promotion_api/promotion", {
        method: 'POST',
        body: JSON.stringify(promotion),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(res => res.json())
}
word = (evt) => {
    evt.preventDefault();
    this.setState({
        text: this.keyIn.current.value
    },()=>{
        let promotion = this.state.promotion
        promotion[0].tn = this.keyIn.current.value
        console.log(this.state.promotion) 
    })
}
CS = (evt) => {
    evt.preventDefault();
    this.setState({
        slogan: this.LinkIn.current.value
    },()=>{
        let promotion = this.state.promotion
        promotion[0].sn = this.LinkIn.current.value
        console.log(this.state.promotion) 
    })
}
LinkWord =(evt) =>{
    evt.preventDefault();
    this.setState({
        name: this.keyIn.current.value
    })
}
upup = (evt) => {
    this.setState({
        selectedFile:evt.target.files[0],  
        
    })
    this.setState({
        img_name:evt.target.files[0].name,  
        
    })

}
song = () => {

    //上傳圖片檔案
            const formdata = new FormData();
            formdata.append('image',this.state.selectedFile,this.state.img_name);
            fetch("http://localhost:3000/ppu_api/ppu",{
                method:"POST",
                body:formdata
            }).then(function(res){
                return res.json();
            })
    
    //上傳圖片檔名
      fetch("http://localhost:3000/ppu_api/ppu",{
                method:"POST",
                body: JSON.stringify({
                   picName:this.state.img_name,
                })
            }).then(function(res){
                // return res.json();
            })
            console.log(this.state.img_name) ;
}
handleDragStart = e => {
    e.target.setAttrs({
      shadowOffset: {
        x: 15,
        y: 15
      },
      scaleX: 1.1,
      scaleY: 1.1
    });
  };
// handleDragEnd = e => {
//     e.target.to({
//       duration: 0.5,
//       easing: Konva.Easings.ElasticEaseOut,
//       scaleX: 1,
//       scaleY: 1,
//       shadowOffsetX: 5,
//       shadowOffsetY: 5
//     });
//   };
handleStageMouseDown = e => {
    if (e.target === e.target.getStage()) {
      this.setState({
        selectedShapeName: ''
      });
      return;
    }
    const clickedOnTransformer =
      e.target.getParent().className === 'Transformer';
    if (clickedOnTransformer) {
      return;
    }
    const name = e.target.name();
    const rect = this.state.rectangles.find(r => r.name === name);
    if (rect) {
      this.setState({
        selectedShapeName: name
      });
    } else {
      this.setState({
        selectedShapeName: ''
      });
    }
  };
// smallCircle = (evt) =>{
//     evt.preventDefault();
//     this.setState({
//     Stage=JSON.stringify(Stage)
//     },()=>{

//     })
// }
handleDragEnd01 = e =>{
    this.setState({
        imgx: e.target.x() ,
        imgy: e.target.y()
      }, () => {
        let promotion = this.state.promotion
        promotion[0].gx = e.target.x() ;
        promotion[0].gy = e.target.y() ;
        console.log(this.state.promotion) 
    })
      
} ;
handleDragEnd02 = e =>{
    this.setState({
        titlex: e.target.x() ,
        titley: e.target.y()
      }, () => {
        let promotion = this.state.promotion
        promotion[0].tx = e.target.x() ;
        promotion[0].ty = e.target.y() ;
        console.log(this.state.promotion) 
      })
     
} ;
handleDragEnd03 = e =>{
    this.setState({
        sloganx: e.target.x() ,
        slogany: e.target.y()
      }, () => {
        let promotion = this.state.promotion
        promotion[0].sx = e.target.x() ;
        promotion[0].sy = e.target.y() ;
        console.log(this.state.promotion) 
      })
      
} ;

render() {
    return (
        <React.Fragment>
            <div className="promotion_set_out"  >
                
                <div className="promotion_set_Left">

                    <Stage width={window.innerWidth} height={window.innerHeight}
                        onMouseDown={this.handleStageMouseDown} 
                        onClick={this.test}
                        >
                        <Layer>
                        <Image
                            x={this.state.imgx}
                            y={this.state.imgy}
                            draggable
                            opacity={0.8}
                            shadowColor="grey"
                            shadowBlur={20}
                            image={this.state.image}
                            ref={node => {
                            this.imageNode = node;
                            }}
                            onDragEnd={this.handleDragEnd01}
                            />
                        </Layer>
                    </Stage>
                </div>

                <div className="promotion_set_Right">
                    <input className="promotion_set_TitleBox" type="text" ref={this.keyIn} defaultValue="" />
                    <button onClick={this.word}>更換標題</button>
                    <br></br>
                    <input className="promotion_set_LinkBox" type="url" ref={this.LinkIn} defaultValue=""></input>
                    <button onClick={this.CS}>更換標語</button>
                    <br></br>
                    {/* <button onClick={this.To}>名字</button> */}
                    <button onClick={this.push}>送出</button>

                    <p className="promotion_set_preview">{this.state.inner}</p>
                    {/* <p className="promotion_set_preview">{this.state.in}</p> */}

                    <div className="App">
                        <input type="file" onChange={this.upup} />
                        <button onClick={this.song}>Upload</button>
                    </div>
                </div>
            </div>
            <div className="promotion_set_down">
                <div className="promotion_set_dLeft">
                <Stage width={window.innerWidth} height={window.innerHeight}
                        onMouseDown={this.handleStageMouseDown} 
                        onClick={this.test}
                        >
                        <Layer>
                            <Text 
                                x={this.state.titlex}
                                y={this.state.titley}
                                text={this.state.text}
                                fontSize={this.state.Tsize}
                                draggable
                                fontFamily = "Audi Type, 微軟正黑體 "
                                onDragEnd={this.handleDragEnd02}
                            />
                            <Text 
                                x={this.state.sloganx}
                                y={this.state.slogany}
                                text={this.state.slogan}
                                fontSize={this.state.Msize}
                                draggable
                                fontFamily = "Audi Type,微軟正黑體"
                                onDragEnd={this.handleDragEnd03}
                            />
                            <Text 
                                x={this.state.detail01x}
                                y={this.state.detail01y}
                                text={this.state.detail01}
                                fontSize={this.state.Ssize}
                                draggable
                                fontFamily = "Audi Type,微軟正黑體"
                                onDragEnd={this.handleDragEnd04}
                            />
                            <Text 
                                x={this.state.detail02x}
                                y={this.state.detail02y}
                                text={this.state.detail02}
                                fontSize={this.state.Ssize}
                                draggable
                                fontFamily = "Audi Type,微軟正黑體"
                                onDragEnd={this.handleDragEnd05}
                            />
                            <Text 
                                x={this.state.detail03x}
                                y={this.state.detail03y}
                                text={this.state.detail03}
                                fontSize={this.state.Ssize}
                                draggable
                                fontFamily = "Audi Type,微軟正黑體"
                                onDragEnd={this.handleDragEnd06}
                            />
                            <Text 
                                x={this.state.detail04x}
                                y={this.state.detail04y}
                                text={this.state.detail04}
                                fontSize={this.state.Ssize}
                                draggable
                                fontFamily = "Audi Type,微軟正黑體"
                                onDragEnd={this.handleDragEnd07}
                            />
                        </Layer>
                    </Stage>
                    
                </div>  
                <div className="promotion_set_dRight">
                    
                </div>  
            </div>
        </React.Fragment>
    )
}

componentDidMount() {
    this.getPromotions();
    const image = new window.Image();
    image.src = pg01 ;
    image.onload = () => {
    this.setState({
        image: image
    });
    };
    
}
//call restful api
getPromotions() {
    //XMLHttpRequest
    fetch("http://localhost:3000/promotion_api/promotion/")
        .then(res => res.json())
        .then(promotions => this.setState({
            promotions: promotions
        }))
}

// componentDidUpdate(prevProps, prevState) {
//         let height = ReactDOM.findDOMNode(this).offsetHeight;
//         if (this.state.height !== height ) {
//           this.setState({ internalHeight: height });
//         }
//       }

}


export default promotion_set;