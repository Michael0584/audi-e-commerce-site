import React, { Component } from 'react';
import { Col, Form, FormGroup, Label, Input,CustomInput } from 'reactstrap';
import Mail from'./mail'
class Contact extends Component {
  constructor(props){
    super(props)
    this.state = {
      mailSender: "audi",
      mailRecipient: "",
      mailCategories: "",
      mailSubject: "",
      mailDetail: "",
      mailImage: "",
      mailImageName: "",
      mailDate: Date.now(),
      invalidCategories: "",
      invalidSubject: "",
      mailRecipientOptions:[],
    }
  }
  componentWillMount(){
    fetch('http://localhost:8080/getmembers.php')
      .then(res=>res.json())
      .then(data=>{
        console.log(data)
        this.setState({mailRecipientOptions:data.result})
      })
  }
  changeHandler = (evt) => {
    if (evt.target.name === "mailImage") {
      this.setState({
        mailImage: evt.target.files[0],
        mailImageName: Date.now() + evt.target.files[0].name,
      })
    } else {
      this.setState({ [evt.target.name]: evt.target.value })
    }
  }
  submitHandler = (evt) => {
    evt.preventDefault();
    if (this.state.mailCategories === "") {
      this.setState({
        invalidCategories: "請填寫類別"
      })
      if (this.state.mailSubject === "") {
        this.setState({
          invalidSubject: "請填寫標題"
        })
      }
    } else {
      var obj = {
        mailSender: this.state.mailSender,
        mailRecipient: this.state.mailRecipient,
        mailCategories: this.state.mailCategories,
        mailSubject: this.state.mailSubject,
        mailDetail: this.state.mailDetail,
        mailImageName: this.state.mailImageName,
        mailDate: Date.now(),
      }
      fetch('http://localhost:3000/membercenter/contact', {
        method: 'POST',
        body: JSON.stringify(obj),
        mode: 'cors',
        headers: new Headers({
          'Content-Type': 'application/json'
        })
      })
        .then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (data) {
          alert("問題回報成功")
        })

      if (this.state.mailImage !== "") {
        let formData = new FormData();
        formData.append('mailImage', this.state.mailImage, this.state.mailImageName);
        fetch("http://localhost:3000/membercenter/contact/upload", {
          method: "POST",
          body: formData,
        }).then(res => res.json())
          .then(data => console.log(data))
      }
    }
  }
  render() {
    return (
      <div className="container-fluid">
      <Form onSubmit={this.submitHandler}>
      <FormGroup row>
        <Label for="mailSubject" sm={2}>信件標題</Label>
        <Col sm={10}>
          <Input type="text" name="mailSubject" id="mailSubject" onChange={this.changeHandler}  value={this.state.mailSubject}/>
        <span className="text-danger">{this.state.invalidSubject}</span>
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for="mailCategories" sm={2}>信件類別</Label>
        <Col sm={10}>
          <Input type="text" name="mailCategories" id="mailCategories"  onChange={this.changeHandler} value={this.state.mailCategories}/>
        <span className="text-danger">{this.state.invalidCategories}</span>
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for="mailRecipient" sm={2}>收件人</Label>
        <Col sm={10}>
          <Input type="select" name="mailRecipient" id="mailRecipient" onChange={this.changeHandler}>
          <option>請選擇</option>
          {this.state.mailRecipientOptions.map((recipient,index)=>
          <option key={index} value={recipient.email}>{recipient.email}</option>)}
          </Input>
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for="mailDetail" sm={2}>信件內容</Label>
        <Col sm={10}>
          <Input type="textarea" name="mailDetail" id="mailDetail" onChange={this.changeHandler} value={this.state.mailDetail}/>
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for="mailImage" sm={2}>信件圖片</Label>
        <Col sm={10}>
          <CustomInput type="file" name="mailImage" id="mailImage" onChange={this.changeHandler} label={this.state.mailImageName}/>
        </Col>
      </FormGroup>
      <button className="btn btn-outline-success">送出</button>
    </Form>
    </div>
    )
  }
}


export default Contact;