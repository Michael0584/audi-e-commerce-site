import React, { Component } from 'react';
import { Table } from 'reactstrap';
import { Link } from "react-router-dom";
import'./inbox.scss'
class Inbox extends Component {
    constructor(props){
        super(props)
        this.state={
          mails:[],
          search:""
        }
    }

    componentWillMount(){
      fetch('http://localhost:8080/getinbox.php')
      .then(res=>res.json())
      .then(data=>{
        console.log(data)
        this.setState({mails:data.result})
      })
    }
    transferDate(date) {
      let mailDate = new Date(+date);
      return mailDate.toLocaleDateString();
    }

    changeHandler=(evt)=>{
      this.setState({
        search:evt.target.value
      })
    }

  render() {
    return (
      <div className="container-fluid">
        <div className="d-flex justify-content-between" > 
          <Link to='/manageplatforms/contact'><button className="btn btn-outline-success">新增信件</button></Link>
          <input type="text" className="search" value={this.state.search} onChange={this.changeHandler} placeholder="請輸入關鍵字"/>
        </div>
        <Table hover>
        <thead>
          <tr>
            <th>信件標題</th>
            <th>信件類別</th>
            <th>信件內容</th>
            <th>寄件者</th>
            <th>寄件日期</th>
          </tr>
        </thead>
        <tbody>
          {this.state.mails.filter(mail => mail.mail_detail.includes(this.state.search) || mail.mail_subject.includes(this.state.search) || mail.mail_categories.includes(this.state.search) || mail.mail_sender.includes(this.state.search)).map(({mail_subject, mail_categories, mail_detail,mail_sender,mail_date, id}, index)=>
            <tr key={index}>
            <th><Link to={`/manageplatforms/inbox/${id}`}> {mail_subject} </Link></th>
            <td>{mail_categories}</td>
            <td className="mail_detail">{mail_detail}</td>
            <td>{mail_sender}</td>
            <td>{this.transferDate(mail_date)}</td>
          </tr>
            )}
          
        </tbody>

      </Table>
      </div>
    )
  }
}


export default Inbox;