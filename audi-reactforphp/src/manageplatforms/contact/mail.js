import React, { Component } from 'react';
import './mail.scss'
import './contact'
import Contact from './contact';
class Mail extends Component {
    constructor(props){
        super(props)
        this.state={
          mail: {},
          show: false
        }
    }
    componentWillMount(){
      console.log(this.props.match.params.id)
        fetch(`http://localhost:8080/inbox/${this.props.match.params.id}`)
        .then(res=>res.json())
        .then(data=>{
          console.log(data)
          this.setState({mail:data.result})
        })
      }
      transferDate(date) {
        let mailDate = new Date(+date);
        return mailDate.toLocaleDateString();
      }

      clickHandler=()=>{
        this.setState({
          show:!this.state.true,
        })
      }
  render() {
    const { mail_sender, mail_recipient,mail_categories,mail_subject,mail_detail, mail_image_name,mail_date} = this.state.mail
    return (
      <React.Fragment>
        <div className="container-fluid contact">
        <button onClick={this.clickHandler} className="btn btn-outline-success">回覆信件</button>
            <div className="mail" >
              <div className="row">
                <p className="topic col-12 col-lg-2">信件標題</p>
                <p className="px-3 col-12 col-lg-10">{mail_subject}</p>
              </div>
              <div className="row">
                <p className="topic col-12  col-lg-2">寄件時間</p>
                <p className="px-3 col-12 col-lg-10">{this.transferDate(mail_date)}</p>
              </div>
              <div className="row">
                <p className="topic col-12  col-lg-2">寄件者</p>
                <p className="px-3 col-12 col-lg-10">{mail_sender}</p>
              </div>
              <div className="row">
                <p className="topic col-12  col-lg-2">收件者</p>
                <p className="px-3 col-12 col-lg-10">{mail_recipient}</p>
              </div>
              <div className="row">
                <p className="topic col-12  col-lg-2">信件類別</p>
                <p className="px-3 col-12 col-lg-10">{mail_categories}</p>
              </div>
              <div className="row">        
                <p className="topic col-12  col-lg-2">信件內容</p>
              </div> 
                <p className="col-12">{mail_detail}</p>
                <img className="upload_img " src={mail_image_name}  alt=""/>
            </div>
        </div>
        <div className={`px-5 col-12 ${this.state.show ? "show" : "hide"}`}>
      <Contact/>
        </div>
      </React.Fragment>
    )
  }
}


export default Mail;