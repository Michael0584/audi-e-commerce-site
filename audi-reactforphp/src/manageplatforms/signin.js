import React, { Component } from 'react';
import { Form, FormGroup, Label, Input, } from 'reactstrap';
import './signin.scss'
class SignIn extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      email: "",
      password: "",
      invalid:""
    }
  }

  changeHandler = (evt) => {
    this.setState({ [evt.target.name]: evt.target.value })
  }

  submitHandler = (evt) => {
    evt.preventDefault();
    if(this.state.email === "audi@email.com" && this.state.password === "password"){
      fetch('http://localhost:3000/members/signIn', {
              method: 'POST',
              credentials: 'include',
              body: JSON.stringify(this.state),
              headers: new Headers({
                'Content-Type': 'application/json'
              })
            })
            .then(res => res.json())
            .then(window.location.href = "/manageplatforms/inbox")
    }else{
      this.setState({invalid:"請填寫正確帳號密碼"})
    }
    
  }

render() {
  return (
    <React.Fragment>
      <Form onSubmit={this.submitHandler} className="sign_in">
        <FormGroup>
          <Label htmlFor="email">會員帳號(E-mail)</Label>
          <Input type="email" name="email" id="email" value={this.state.email} onChange={this.changeHandler} placeholder="請輸入E-mail" />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="password">會員密碼</Label>
          <Input type="password" name="password" id="password" value={this.state.password} onChange={this.changeHandler} placeholder="請輸入密碼" />
        </FormGroup>
        <button className="btn">SIGN IN</button>
        <br/>
        <span>{this.state.invalid}</span>
      </Form>
    </React.Fragment>
  )
}

}
export default SignIn;