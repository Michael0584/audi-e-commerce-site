import React, { Component } from 'react';
import { Col, Form, FormGroup, Label, Input,CustomInput } from 'reactstrap';

class Add extends Component {
  constructor(props){
    super(props)
    this.state = {
        category:     '',
        name:         '',
        price:        '',
        description:  ''
    }
    console.log(this);
  }
  changeHandler = (evt) => {
    let value = evt.target.dataset.id
    this.setState({
        
        [evt.target.dataset.id]: evt.target.value,
    })
    console.log(this.state)
}
    submitHandler =(evt) =>{
        evt.preventDefault();
        if( this.state.category ==="" || this.state.name ==="" || this.state.price ==="" || this.state.description === ""){
			alert("請填寫完所有資料")
		}else {
            if(localStorage.getItem("Backstage")){
                let data =   JSON.parse(localStorage.getItem("Backstage"))
               
                data.push(this.state);
                localStorage.setItem("Backstage",JSON.stringify(data)); 

            }else{
                let data =[]
                data.push(this.state)
                localStorage.setItem("Backstage",JSON.stringify(data));  
            }        
        }
        window.location.href ="/manageplatforms/product"
    }
		
    
 
  render() {
    return (
    <div className="container-fluid">
    <Form onSubmit={this.submitHandler}>
    <FormGroup row>
        <Label for="productID" sm={2}>商品id</Label>
        <Col sm={2}>
            <Input maxLength='5' type="text" name="productID" data-id="productID" onChange={this.changeHandler} value={this.state.productID} />
        <span className="text-danger"></span>
        </Col>
    </FormGroup>
    <FormGroup row>
        <Label for="category" sm={2}>子分類</Label>
        <Col sm={2}>
            <Input maxLength='3' type="text" name="category" data-id="category" onChange={this.changeHandler}  value={this.state.category}/>
        <span className="text-danger"></span>
        </Col>  
    </FormGroup>
    <FormGroup row>
        <Label for="name" sm={2}>商品名稱</Label>
        <Col sm={2}>
            <Input maxLength='15' type="text" name="name" data-id="name" onChange={this.changeHandler}  value={this.state.name}/>
        <span className="text-danger"></span>
        </Col>
    </FormGroup>
    
    
    <FormGroup row>
    <Label for="price" sm={2}>金額</Label>
    <Col sm={2}>
        <Input maxLength='10' type="text" name="price" data-id="price" onChange={this.changeHandler}  value={this.state.price}/>
    <span className="text-danger"></span>
    </Col>
    </FormGroup>
    <FormGroup row>
    <Label for="description" sm={2}>敘述</Label>
    <Col sm={5}>
        <Input type="textarea" name="description" data-id="description" onChange={this.changeHandler}  value={this.state.description}/>
    <span className="text-danger"></span>
    </Col>
    </FormGroup>
    
    <button className="btn btn-outline-success">送出</button>
    </Form>
    </div>
    )
  }
}


export default Add;