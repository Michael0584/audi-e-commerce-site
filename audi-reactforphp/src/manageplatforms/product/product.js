import React, { Component } from 'react';
import { Container, Row, Col, Table, Button } from "reactstrap";
// import { Col, Form, FormGroup, Label, Input,CustomInput } from 'reactstrap';
import "./product.scss";

class Product extends Component {
   constructor(props){
        super(props);
        this.state = {
            collections: [],
            accessories: []
        };
        console.log(this)
		}
		
		deleteHandler(item,e){
			console.log("type:"+e.target.dataset.type);
			console.log("id:"+e.target.dataset.id);
			let id = e.target.dataset.id;
			console.log(id);
			fetch("http://localhost:3000/control/collections/"+id,{
                method:'DELETE',
								body: JSON.stringify({ product_sid: id }),
                headers: new Headers({
                  'Content-Type':'application/json'
								})
							}).then(res=>{
								res.json()
								console.log(res)
							}) 
				.then(data => {
				  window.location.reload();
			})

		}

    async componentWillMount() {
      const responseCollections = await fetch("http://localhost:3000/control/collections")
      const collections = await responseCollections.json();
      this.setState({collections});

      const responseAccessories = await fetch("http://localhost:3000/control/accessories")
      const  accessories = await responseAccessories.json();
      this.setState({accessories})
  }


  render() {
    return (
      <div>
       <Container fluid={true} >

							<div className='table'>
								<Table>
									<thead className='ContentTitle'>
										<tr>
											<th>商品id</th>
                      <th>商品名稱</th>
                      <th>敘述</th>
                      <th>子分類</th>
											<th>金額</th>
											<th>修改</th>
                      <th>移除</th>
										</tr>
									</thead>
                  {this.state.collections.map((collection, i) => (
										<tr key={i} className='cartItems cartItem00'>
                      <th scope="row" className='product' >
												<p className='productId'>{ collection.product_sid}</p>
											</th>
											<td className='product'>
												<p className='productName'>{ collection.product_name}</p>
											</td>
                      <td className='product'>
												<p className='productDescription'>{ collection.description}</p>
											</td>
                      <td className='product'>
												<p className='productSub_category_id:'>{collection.categories_sub_id}</p>
											</td>
                      <td className='product'>
												<p className='productPrice'>{collection.price}</p>
											</td>
											<td className='product'>
												<Button outline color="primary">修改</Button>
											</td>
											<td className='product'>
											<Button outline color="danger" data-id={collection.product_sid} data-type="del" onClick={this.deleteHandler.bind(this,this.event)}>刪除</Button>
											</td>
											
										</tr>
									))}
									 <td><input type="text"></input></td>
									 <td><input type="text"></input></td>
									 <td><input type="text"></input></td>
									 <td><input type="text"></input></td>
									 <td><input type="text"></input></td>
									 <Button outline color="primary">新增</Button>
								</Table>
							
							</div>    
				</Container>
      </div>
    )
  }
}


export default Product;