import React, { Component } from 'react';
import { Table } from 'reactstrap';
class MemberProfile extends Component {
  constructor(props){
    super(props)
    this.state={
      members:[],
      search:"",
    }
  }
  componentDidMount(){
    fetch('http://localhost:8080/getmembers.php')
      .then(res=>res.json())
      .then(data=>{
        this.setState({members:data.result})
      })
  }
  changeHandler=(evt)=>{
    this.setState({
      search:evt.target.value
    })
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="d-flex justify-content-end" > 
          <input type="text" className="search" value={this.state.search} onChange={this.changeHandler} placeholder="請輸入關鍵字"/>
        </div>
        <Table hover>
        <thead>
          <tr>
            <th>會員帳號</th>
            <th>會員名稱</th>
            <th>手機</th>
            <th>生日</th>
          </tr>
        </thead>
        <tbody>
          {this.state.members.filter(member => member.email.includes(this.state.search) || member.username.includes(this.state.search) || member.mobile.includes(this.state.search) || member.birthday.includes(this.state.search)).map(({email,username,mobile,birthday}, index)=>
            <tr key={index}>
            <th>{email}</th>
            <td>{username}</td>
            <td className="cart">{mobile}</td>
            <td>{birthday}</td>
          </tr>
            )}
          
        </tbody>

      </Table>
      </div>
    )
  }
}


export default MemberProfile;