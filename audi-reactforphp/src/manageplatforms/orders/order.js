import React, { Component } from 'react';
import { Table } from 'reactstrap';
import { Link } from "react-router-dom";
import './order.scss'
class Order extends Component {
  constructor(props){
    super(props)
    this.state={
      orders:[],
      search:"",
      cart:[],
    }
  }
  componentDidMount(){
    fetch('http://localhost:8080/getorders.php')
      .then(res=>res.json())
      .then(data=>{
        this.setState({orders:data.result})
      })      
  }
  changeHandler=(evt)=>{
    this.setState({
      search:evt.target.value
    })
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="d-flex justify-content-end" > 
          <input type="text" className="search" value={this.state.search} onChange={this.changeHandler} placeholder="請輸入關鍵字"/>
        </div>
        <Table hover>
        <thead>
          <tr>
            <th>訂單編號</th>
            <th>訂購人</th>
            <th>訂單價格</th>
          </tr>
        </thead>
        <tbody>
          {this.state.orders.filter(order => order.order_id.includes(this.state.search) || order.member_email.includes(this.state.search) || order.total_price.includes(this.state.search)).map(({order_id,member_email,total_price,order_sid}, index)=>
            <tr key={index}>
            <th><Link to={`/manageplatforms/order/${order_sid}`}>{order_id}</Link></th>
            <td>{member_email}</td>
            <td>{total_price}</td>
          </tr>
            )}
          
        </tbody>

      </Table>
      </div>
    )
  }
}


export default Order;