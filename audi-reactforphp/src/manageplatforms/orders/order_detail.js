import React, { Component } from 'react';
import { Container, Col, Row } from "reactstrap";
import './order_detail.scss'
class OrderDetail extends Component {
  constructor(props) {
    super(props)

    this.state = {
      order: [],
      cart: [],
      orderID: [],
      orderDetail: [],
      cartContent: [],
      reservationInfo: [],
      paymentInfo: [],
      totalPrice: [],
      address: [],
      productImages: [],
      productImgPath: [],
      locations: []
    }
  }

  async componentWillMount() {
    const order = await fetch(`http://localhost:8080/order/${this.props.match.params.id}`);
    const orderDetail = await order.json();
    const locations = await fetch(`http://localhost:8080/getlocations.php`);
    const installationLocations = await locations.json();
    this.setState(
      {
        locations: installationLocations,
        orderDetail: orderDetail //Every detail of the order, this needs to be set first before everything else.
      },
      () => {
        this.setState({
          orderID: this.state.orderDetail.map((item) => item.order_id), //Order ID
          cartContent: this.state.orderDetail.map((item) => JSON.parse(item.cart))[0], //Cart content
          reservationInfo: this.state.orderDetail.map((item) => JSON.parse(item.installation_reservation_info)), //Reservation info
          paymentInfo: this.state.orderDetail.map((item) => JSON.parse(item.payment_info)), //Payment info
          totalPrice: this.state.orderDetail.map((item) => this.numberWithCommas(item.total_price)), //Total price
          address: this.state.orderDetail.map((item) => JSON.parse(item.order_address)) //Order Address
        });
      }
    );
  }

  numberWithCommas = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
  installationRequiredCheck = (item) => {
    if (item !== 0) {
      return "是";
    } else {
      return "否";
    }
  };
  render() {
    return (
      <Container fluid={true} className='containerModifier'>
        <Row>
          <Col>
            <h3 className='headline'>訂購明細</h3>
          </Col>
        </Row>
        <Row className='cartContent'>
          <Col>
            <div className='aui-table aui-table--stretched'>
              <table>
                <thead className='cartContentTitle'>
                  <tr>
                    <th>商品</th>
                    <th className='text-center'>顔色/材質</th>
                    <th className='text-center'>大小</th>
                    <th className='text-center'>數量</th>
                    <th className='text-center'>金額</th>
                    <th className='text-center'>需預約安裝</th>
                  </tr>
                </thead>
                {this.state.cartContent.map((cartItem, i) => (
                  <tbody key={i}>
                    <tr className='cartItems cartItem00'>
                      <td className='product'>
                        <p className='productName'>{cartItem.product_name}</p>
                      </td>
                      <td className='color'>
                        <p className='colorText text-center'>{cartItem.color}</p>
                      </td>
                      <td className='size'>
                        <p className='sizeText text-center'>{cartItem.size}</p>
                      </td>
                      <td className='quantity'>
                        <div className='flexWrap '>
                          <p className='text-center'>
                            {cartItem.quantity}
                          </p>
                        </div>
                      </td>
                      <td className='price'>
                        <p className='priceText text-center'>
                          {this.numberWithCommas(cartItem.product_price * cartItem.quantity)}
                        </p>
                      </td>
                      <td className='installation'>
                        <p className='installationText text-center'>
                          {this.installationRequiredCheck(cartItem.installation_required)}{" "}
                        </p>
                      </td>
                    </tr>
                  </tbody>
                ))}
              </table>
            </div>
          </Col>
        </Row>
        <Row className='orderIDandTotalPrice'>
          <Col>
            <hr className='divisionLinesMarginBottom' />
            <Container fluid={true}>
              <Row>
                <Col xs='6' className='leftCol'>
                  <p>
                    <span className='orderIDTitle'>訂單號碼:</span>{" "}
                    <span className='orderID'>{this.state.orderID}</span>
                  </p>
                </Col>
                <Col xs='6' className='rightCol basketTotal'>
                  <Row>
                    <Col>
                      <h4>訂單金額</h4>
                      <hr className='marginTop' />
                      <div className='flexWrap'>
                        <p className='title'>商品金額:</p>
                        <p className='price'>${this.state.totalPrice}</p>
                      </div>
                      <div className='flexWrap'>
                        <p className='title'>預計寄送費用:</p>
                        <p className='price'>${this.numberWithCommas(0)}</p>
                      </div>
                      <div className='flexWrap'>
                        <p className='title'>折扣:</p>
                        <p className='price'>-${this.numberWithCommas(0)}</p>
                      </div>
                      <div className='flexWrap total'>
                        <p className='title bold'>總金額:</p>
                        <p className='price bold'>${this.state.totalPrice}</p>
                      </div>
                      <hr />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Container>
            <hr className='divisionLinesMargin' />
          </Col>
        </Row>
        <Row className='PaymentAndShipping' style={{ marginBottom: this.state.reservationInfo[0] === null ? "80px" : null }}>
          <Col>
            <Container fluid={true}>
              <Row>
                <Col xs='6' className='leftCol payment'>
                  <h2>付款方式</h2>
                  <br />
                  <div className='payment'>
                    <p>線上刷卡</p>
                    <br />
                    {this.state.paymentInfo.map((info, i) => (
                      <div key={i} className='ccInfo'>
                        <p className='bold'>交易記錄：</p>
                        <p>持卡人: {info.CreditCardHolderName}</p>
                        <p>
                          卡片後四碼: XXXX-XXXX-XXXX-{info.CreditCardNumber.toString().slice(-4)}
                        </p>
                        <p>交易時間: {info.transactionTime}</p>
                      </div>
                    ))}
                  </div>
                </Col>
                <Col xs='6' className='rightCol shipping'>
                  <h2>取貨方式</h2>
                  <br />
                  <div className='shipping'>
                    <p>宅配</p>
                    <br />
                    {this.state.address.map((address, i) => (
                      <div key={i} className='shippingInfo'>
                        <p className='bold'>寄送地址：</p>
                        <p>收件人：{address.shippingAddress.name}</p>
                        <p>城市：{address.shippingAddress.county}</p>
                        <p>區：{address.shippingAddress.district}</p>
                        <p>地址：{address.shippingAddress.address}</p>
                      </div>
                    ))}
                  </div>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
        {this.state.reservationInfo[0] === null ? null : (
          <Row className='InstallationReservation'>
            <Col>
              <hr className='divisionLinesMargin' />
              <Container fluid={true}>
                <Row>
                  <Col xs='6' className='leftCol reservation'>
                    <h2>預約安裝配件地點與時間</h2>
                    <br />
                    <div className='reservation'>
                      {this.state.locations.map((location, i) => {
                        if (
                          location.location_id ==
                          this.state.reservationInfo.map((item, i) => item.location_id)
                        ) {
                          return (
                            <div key={i} className='reservationLocation'>
                              <p className='bold'>安裝地點：</p>
                              <p>據點名稱：{location.location_name}</p>
                              <p>電話：{location.telephone}</p>
                              <p>地址：{location.address}</p>
                              <p>營業時間：{location.hours_of_operation}</p>
                            </div>
                          );
                        }
                      })}
                    </div>
                    <br />
                    <div className='reservation'>
                      {this.state.reservationInfo.map((items, i) => {
                        let timeSlot;
                        switch (items.slot_time !== "") {
                          case items.slot_time === 0:
                            timeSlot = "上午 09:00";
                            break;
                          case items.slot_time === 1:
                            timeSlot = "上午 10:00";
                            break;
                          case items.slot_time === 2:
                            timeSlot = "上午 11:00";
                            break;
                          case items.slot_time === 3:
                            timeSlot = "中午 12:00";
                            break;
                          case items.slot_time === 4:
                            timeSlot = "下午 01:00";
                            break;
                          case items.slot_time === 5:
                            timeSlot = "下午 02:00";
                            break;
                          case items.slot_time === 6:
                            timeSlot = "下午 03:00";
                            break;
                          case items.slot_time === 7:
                            timeSlot = "下午 04:00";
                            break;
                          case items.slot_time === 8:
                            timeSlot = "下午 05:00";
                            break;
                          default:
                            break;
                        }
                        return (
                          <div key={i} className='reservationLocation'>
                            <p className='bold'>安裝日期與時間：</p>
                            <p>{items.slot_date}</p>
                            <p>{timeSlot}</p>
                          </div>
                        );
                      })}
                    </div>
                  </Col>
                  <Col xs='6' className='rightCol' />
                </Row>
              </Container>
            </Col>
          </Row>
        )}
      </Container>
    )
  }
}


export default OrderDetail;