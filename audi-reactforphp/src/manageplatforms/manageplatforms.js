import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './manageplatforms.scss';
import Order from './orders/order';
import MemberProfile from './memberprofile/memberprofile';
import Product from './product/product';
import Add from './product/add';
import Inbox from './contact/inbox';
import Contact from './contact/contact'
import Mail from './contact/mail';
import promotion_set from './promotion_set'
import Chatroom from './chatroom/chatroom'
import { Container, Row, Col } from 'reactstrap';
import OrderDetail from './orders/order_detail';


class ManagePlatforms extends Component {


  render() {
    return (
      <Router>
        <Container className="manageplatforms " fluid={true}>
          <Row>
            <Col lg={2} md={3} sm={4}>
              <div className="row nav list-group">
                <ul>
                  <li><Link to="/manageplatforms/inbox" >問題回報</Link></li>
                  <li><Link to="/manageplatforms/promotion_set" >優惠活動</Link></li>
                  <li><Link to="/manageplatforms/product">所有產品</Link></li>
                  <li><Link to="/manageplatforms/order">所有訂單</Link></li>
                  <li><Link to="/manageplatforms/memberprofile">會員資料</Link></li>
                  <li><Link to="/manageplatforms/chatroom">客服</Link></li>
                  <li><a href="http://localhost:3000/membercenter/logout">登出</a></li>                  
                </ul>
              </div>
            </Col>
            <Col lg={10} md={9} sm={8}>
              <div className="row content">
                <Route exact path="/manageplatforms/order" component={Order} />
                <Route path="/manageplatforms/memberprofile" component={MemberProfile} />
                <Route path="/manageplatforms/product" component={Product} />
                <Route path="/manageplatforms/add" component={Add} />
                <Route exact path="/manageplatforms/inbox" component={Inbox} />
                <Route path="/manageplatforms/promotion_set" component={promotion_set} />
                <Route path="/manageplatforms/inbox/:id" component={Mail} />
                <Route path="/manageplatforms/order/:id" component={OrderDetail} />
                <Route path="/manageplatforms/contact" component={Contact} />
                <Route path="/manageplatforms/chatroom" component={Chatroom} />
                <Route path="/manageplatforms/remail" component={Chatroom} />

              </div>
            </Col>
          </Row>
        </Container>
      </Router>
    )
  }
}

export default ManagePlatforms;
