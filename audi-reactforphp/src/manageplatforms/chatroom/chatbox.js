
import React, { Component } from 'react';
import './chatbox.scss'
import Message from "./Message";

class Chatbox extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: "",
        }
    }

    componentWillReceiveProps() {
        requestAnimationFrame(()=>this.scrollToBottom())

    }

    changeHandler = (evt) => {
        this.setState({
            value: evt.target.value,
        })
    }

    clickHandler = async () => {
        if (this.state.value !== "") {
            let msg = { email_sender: this.props.email, message: this.state.value, roomId: this.props.roomId }
            this.props.sendMessage(msg)
            await this.setState({
                value: ""
            })

            this.scrollToBottom()
        }
    }

    keyDownHandler = (evt) => {
        if (evt.keyCode === 13) {
            this.clickHandler();
        }
    }

    scrollToBottom() {
        const messageContainer = this.refs.messageContainer
        messageContainer.scrollTop = messageContainer.scrollHeight - messageContainer.offsetHeight
    }

    isemail(email){
        var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
        if(email.search(filter) === -1){
            return "訪客"
        }else{
            return email
        }
    }

    render() {
        return (
            <React.Fragment>
                {<div className="chatroom_window">
                    <div className="chatroom">
                        <div className="chatroom_bar text-white">                            
                            {this.isemail(this.props.roomId)}
                        </div>
                        <div className="messages" ref="messageContainer">
                            {this.props.messages.map((messageObj, index) => (
                                <div key={index} className={this.props.email === messageObj.email_sender ? "message_right" : "message_left"}><img src='/img/icons/user-large.svg' className={this.props.email === messageObj.email_sender ? "icon_hide" : ""} />
                                <p ><Message message={messageObj.message}></Message></p></div>
                            ))}
                        </div>
                        <div className="message_bar">
                            <input type="text" className="message_input" value={this.state.value} onChange={this.changeHandler} onKeyDown={this.keyDownHandler} placeholder="請輸入文字" />

                            <button className="message_btn" onClick={this.clickHandler}>送出</button>
                        </div>
                    </div>
                </div>}
            </React.Fragment>
        )
    }
}
export default Chatbox;
