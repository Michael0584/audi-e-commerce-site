import React, { Component } from 'react';
import openSocket from 'socket.io-client'
import Chatbox from './chatbox'

const socket = openSocket('http://localhost:3000')
let chatRoomsCache = [];

Notification.requestPermission();
class Chatroom extends Component {
  constructor(props) {
    super(props)
    this.state = {
      chatRooms: chatRoomsCache,
      email: '',
    }
  }

  componentWillMount() {
    fetch('http://localhost:3000/api/profile', { credentials: 'include' })
      .then(res => res.json())
      .then(data => {
        this.setState({ email: data.email })
      })

    socket.on('recieveMessage', async (msg) => {
      this.updateChatRoom(msg, true)
      console.log(msg)
    });

    socket.on('historyMessage', async (msg) => {
      console.log(msg)
      msg.forEach(msg => this.updateChatRoom(msg))
    });

    socket.on('disconnectUser',(user)=>{
      console.log(user)
      const chatRooms = this.state.chatRooms
      const index = chatRooms.findIndex(chatRoom=>chatRoom.roomId===user)
      this.setState({
        chatRooms: [...chatRooms.slice(0, index), ...chatRooms.slice(index+1)]
      })
    })

  }

  sendMessage = (msg) => {
    socket.emit('sendMessage', msg)
    this.updateChatRoom(msg)
  }

  notifyAdmin(title, body) {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var notification = new Notification(title, {
        icon: 'http://localhost:3006/img/chatting.png',
        body: body,
      });
      notification.onclick = function () {
        window.focus();
      }
    }
  }

  getClientName(email) {
    var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    if (email.search(filter) === -1) {
      return "訪客";
    } else {
      return email
    }
  }
  updateChatRoom = (msg, isNewMessage) => {
    console.log(msg)
    let chatRooms = JSON.parse(JSON.stringify(this.state.chatRooms))
    const { roomId } = msg
    if (!chatRooms.find(chatRoom => chatRoom.roomId === roomId)) {
      chatRooms = chatRooms.concat({ roomId, messages: [msg], email: this.state.email })
      if(isNewMessage){
        this.notifyAdmin("New Client", this.getClientName(roomId) + " needs help!");
      }
    } else {
      chatRooms = chatRooms.map(chatRoom => {
        if (chatRoom.roomId === roomId) {
          chatRoom.messages = chatRoom.messages.concat(msg)
        }
        return chatRoom
      })
    }

    this.setState({ chatRooms })
    chatRoomsCache = chatRooms
  }


  render() {
    return (
      <React.Fragment>
        {this.state.chatRooms.map(chatRoom => <Chatbox {...chatRoom} sendMessage={this.sendMessage} key={chatRoom.roomId}></Chatbox>)}
      </React.Fragment>
    )
  }
}
export default Chatroom;
