
var express = require("express");
var router = express.Router();
var { connection } = require("./mysqlConnection");

// GET all orders:
// http://localhost:3000/order/get_all
router.route("/get_all").get((req, res) => {
	connection.query("SELECT * FROM `orders` WHERE 1", (error, results) => {
		if (error) throw error;
		res.json(results);
	});
});

//GET a specific order:
// http://localhost:3000/order/:order_id
router.route("/:order_id").get((req, res) => {
    connection.query(
      "SELECT * FROM `orders` WHERE `order_id` = ?",
      req.params.order_id,
      (error, rows) => {
        if (error) throw error;
        res.json(rows);
      }
    );
  });

//POST order
// http://localhost:3000/order/submit_order
// Required fields:
//  member_id
//  member_email
//  order_id
//  order_address
//  cart
//  installation_reservation_info
//  payment_info
//  total_price
router.route("/submit_order").post((req, res) => {
	connection.query("INSERT INTO `orders` set ?", req.body, (error, result) => {
		if (error) throw error;
		res.json({ message: "訂單送出" });
	});
});

//PUT installation booking time & location

module.exports = router;