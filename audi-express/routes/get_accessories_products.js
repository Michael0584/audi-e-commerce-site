var express = require("express");
var router = express.Router();
var { connection } = require("./mysqlConnection");

//GET 2 tables: categories, subcategories:
router
  .route("/products/accessories/queryall")
  //req = request from client side
  //res = response from server side
  .get(function (req, res) {
    connection.query(
      "SELECT * FROM `accessories_categories`; SELECT * FROM `accessories_sub_categories`;", [],
      function (error, results) {
        if (error) throw error;
        // `results` is an array with one element for every statement in the query:
        //[0] = categories
        //[1] = sub-categories
        const categories = results[0];
        const subCategories = results[1];
        categories.map(category => {
          category.categoryID = category.category_id
          category.categoryName = category.category_name
          category.subCategories = subCategories.filter(subcategory => subcategory.parent_category_id === category.category_id)
          return category
        })

        res.json(categories);
      }
    );
  });

//GET all products under all categories
router.route("/products/accessories").get((req, res) => {
  connection.query(
    "SELECT `accessories_products`.`product_sid`, `accessories_products`.`product_name`, `accessories_products`.`description`, `accessories_products`.`image_path`, `accessories_products`.`image_path`, `accessories_products`.`size_spec`, `accessories_products`.`color`, `accessories_products`.`price`, `accessories_products`.`discount_status`, `accessories_products`.`discount_price`, `accessories_products`.`promotion_code`, `accessories_products`.`stock`, `accessories_products`.`on_air`, `accessories_products`.`applicable_car_family`, `accessories_products`.`installation_required`, `accessories_products`.`accessories`, `accessories_products`.`collections`, `accessories_sub_categories`.`sub_category_name`, `accessories_categories`.`category_name`, `accessories_sub_categories`.`sub_category_id`, `accessories_categories`.`category_id` FROM `accessories_products` INNER JOIN `accessories_sub_categories` ON `accessories_products`.`sub_category_id`=`accessories_sub_categories`.`sub_category_id` INNER JOIN `accessories_categories` ON `accessories_sub_categories`.`parent_category_id`=`accessories_categories`.`category_id`",
    (error, rows) => {
      if (error) throw error;
      res.json(rows);
    }
  );
});

//GET all products under an accessories category
router.route("/products/accessories/:category").get((req, res) => {
  connection.query(
    "SELECT `accessories_products`.`product_sid`, `accessories_products`.`product_name`, `accessories_products`.`description`, `accessories_products`.`image_path`, `accessories_products`.`image_path`, `accessories_products`.`size_spec`, `accessories_products`.`color`, `accessories_products`.`price`, `accessories_products`.`discount_status`, `accessories_products`.`discount_price`, `accessories_products`.`promotion_code`, `accessories_products`.`stock`, `accessories_products`.`on_air`, `accessories_products`.`applicable_car_family`, `accessories_products`.`installation_required`, `accessories_products`.`accessories`, `accessories_products`.`collections`, `accessories_sub_categories`.`sub_category_name`, `accessories_categories`.`category_name`, `accessories_sub_categories`.`sub_category_id`, `accessories_categories`.`category_id` FROM `accessories_products` INNER JOIN `accessories_sub_categories` ON `accessories_products`.`sub_category_id`=`accessories_sub_categories`.`sub_category_id` INNER JOIN `accessories_categories` ON `accessories_sub_categories`.`parent_category_id`=`accessories_categories`.`category_id` WHERE `accessories_categories`.`category_id` = ?",
    req.params.category,
    (error, rows) => {
      if (error) throw error;
      res.json(rows);
    }
  );
});

//GET all products under an accessories sub-category
router.route("/products/accessories/:category/:subcategory").get((req, res) => {
  connection.query(
    "SELECT `accessories_products`.`product_sid`, `accessories_products`.`product_name`, `accessories_products`.`description`, `accessories_products`.`image_path`, `accessories_products`.`image_path`, `accessories_products`.`size_spec`, `accessories_products`.`color`, `accessories_products`.`price`, `accessories_products`.`discount_status`, `accessories_products`.`discount_price`, `accessories_products`.`promotion_code`, `accessories_products`.`stock`, `accessories_products`.`on_air`, `accessories_products`.`applicable_car_family`, `accessories_products`.`installation_required`, `accessories_products`.`accessories`, `accessories_products`.`collections`, `accessories_sub_categories`.`sub_category_name`, `accessories_categories`.`category_name`, `accessories_sub_categories`.`sub_category_id`, `accessories_categories`.`category_id` FROM `accessories_products` INNER JOIN `accessories_sub_categories` ON `accessories_products`.`sub_category_id`=`accessories_sub_categories`.`sub_category_id` INNER JOIN `accessories_categories` ON `accessories_sub_categories`.`parent_category_id`=`accessories_categories`.`category_id` WHERE `accessories_sub_categories`.`sub_category_id` = ?",
    req.params.subcategory,
    (error, rows) => {
      if (error) throw error;
      res.json(rows);
    }
  );
});

//GET a specific product:
router.route("/products/accessories/:category/:subcategory/:product_sid").get((req, res) => {
  connection.query(
    "SELECT `accessories_products`.`product_sid`, `accessories_products`.`product_name`, `accessories_products`.`description`, `accessories_products`.`image_path`, `accessories_products`.`image_path`, `accessories_products`.`size_spec`, `accessories_products`.`color`, `accessories_products`.`price`, `accessories_products`.`discount_status`, `accessories_products`.`discount_price`, `accessories_products`.`promotion_code`, `accessories_products`.`stock`, `accessories_products`.`on_air`, `accessories_products`.`applicable_car_family`, `accessories_products`.`installation_required`, `accessories_products`.`accessories`, `accessories_products`.`collections`, `accessories_sub_categories`.`sub_category_name`, `accessories_categories`.`category_name`, `accessories_sub_categories`.`sub_category_id`, `accessories_categories`.`category_id` FROM `accessories_products` INNER JOIN `accessories_sub_categories` ON `accessories_products`.`sub_category_id`=`accessories_sub_categories`.`sub_category_id` INNER JOIN `accessories_categories` ON `accessories_sub_categories`.`parent_category_id`=`accessories_categories`.`category_id` WHERE `accessories_products`.`product_sid` = ?",
    req.params.product_sid,
    (error, rows) => {
      if (error) throw error;
      res.json(rows);
    }
  );
});




module.exports = router;
