var {
  connection
} = require("./mysqlConnection");
var {
  sessionMiddleware
} = require("../app")
let usersRoomIds = []
let adminSockets = []

/** @type {function(SocketIO.Server)} */
module.exports = function (io) {
  io.use(function (socket, next) {
    sessionMiddleware(socket.request, socket.request.res, next);
  });

  io.on('connection', (socket) => {
    const { session, sessionID } = socket.request
    const roomId = session.email || sessionID

    if (session.isAdmin) {
      const adminSocket = socket
      adminSocket.hasHistoryRooms = []

      if (!adminSockets.includes(adminSocket)) {
        adminSockets.push(adminSocket)
        usersRoomIds.forEach(roomId => {
          adminSocket.join(roomId)
          adminSocket.emit("waitingMessage",  + usersRoomIds.length )
          connection.query('select  `email_sender`, `message`, `roomId` from messages where roomId=?', roomId, (err, messages) => {
            if (err) {
              console.log(err)
              return
            }
            adminSocket.emit('historyMessage', messages)
          })
        })
        console.log('admin join to', usersRoomIds)
      }

    } else {
      if (!usersRoomIds.includes(roomId)) {
        console.log('user join to ', roomId)
        usersRoomIds.push(roomId)
        adminSockets.forEach(adminSocket => adminSocket.join(roomId))
      }
      socket.join(roomId)
      connection.query('select * from messages where roomId=?', roomId, (err, messages) => {
        if (err) {
          console.log(err)
          return
        }
        socket.emit('historyMessage', messages)
      })
    }

    socket.on('sendMessage', (messageObj) => {
      if (session.isAdmin) {
        console.log(messageObj)
        socket.broadcast.to(messageObj.roomId).emit('recieveMessage', messageObj);
        //admin訊息存資料庫
        connection.query('insert into messages set ?', messageObj, (err) => {
          if (err) {
            console.error(err);
            return
          }
          console.log('新增meesage成功', messageObj)
        })
        
      } else {
        if (adminSockets.length == 0) {
          socket.emit("adminOff", { mail_sender: "audi", message: "客服人員目前不在線上，如有問題請利用問題回報 http://localhost:3001/membercenter/contact，謝謝！" })
        }
        socket.broadcast.to(roomId).emit('recieveMessage', { ...messageObj, roomId });
        //顧客訊息存資料庫
        var copy = {
          "roomId": roomId,
          "email_sender": roomId,
          "message": messageObj.message
        };
        connection.query('insert into messages set ?', copy, (err) => {
          if (err) {
            console.error(err);
            return
          }
        })

        adminSockets.forEach(adminSocket => {

          if (adminSocket.hasHistoryRooms.includes(roomId)) {
            return
          }

          connection.query('select  `email_sender`, `message`, `roomId` from messages where roomId=?', roomId, (err, messages) => {
            if (err) {
              console.log(err)
              return
            }
            adminSocket.emit('historyMessage', messages)

            adminSocket.hasHistoryRooms.push(roomId)
          })
        })
      }
    })


    socket.on('disconnect', () => {
      if (session.isAdmin) {
        const adminSocket = socket
        adminSockets.splice(adminSockets.indexOf(adminSocket), 1)
      } else {
        usersRoomIds.splice(usersRoomIds.indexOf(roomId), 1)
        adminSockets.forEach(adminSocket => {
          adminSocket.emit("disconnectUser", roomId)
          adminSocket.hasHistoryRooms.splice(adminSockets.indexOf(roomId), 1)
        })
      }
    });
  })
}