var express = require('express');
var router = express.Router();
var { connection } = require("./mysqlConnection");
const mysql = require('mysql')



//讀所有資料
router
  .route("/following")
  .get(function (req, res) {
    connection.query("select * from follow_list", function (error, rows) {
      if (error) throw error;
      res.json(rows);
    })
}).post(function(req, res) {//新增資料
    var _following = req.body;
   connection.query("insert into follow_list set ?", _following,function(error){
      if (error) throw error;
      res.json({ message: "加入追蹤" });
   })
 }).delete(function(req, res) {//刪除資料
  connection.query("delete from follow_list where product_sid=?",req.body.product_sid,function(error){
    if(error) throw error;
    res.json({ message: "取消追蹤" });
  })
}); 


module.exports = router;
