var express = require("express");
var router = express.Router();
var mysql = require("mysql");

//建立連線
// var connection = mysql.createConnection({
//   host: "35.221.135.125",
//   database: "audi",
//   user: "root",
//   password: "ILt4x0LENjpdr8Dr"
// });
// connection.connect();

router
  .route("/promotion")
  .get(function(req, res) {
    //  res.send("get all product");
    connection.query("select * from promotion", function(error, results) {
      if (error) throw error;
      res.json(results);
    });
  })
  .post(function(req, res) {
    //res.send("新增資料");
    connection.query("insert into promotion set ?",req.body, function(error) {
      if (error) throw error;
      res.json({ message: "新增成功" });
    });

  });
router
  .route("/promotion/:id")
  .get(function(req, res) {
    //res.send("get product id " + req.params.id);
    connection.query(
      "select * from promotion where id=?",req.params.id,function(error, results) {
        if (error) throw error;
        res.json(results);
      }
    );
  })
  .put(function(req, res) {
    //res.send("修改 " + req.params.id + " 資料");
    connection.query("update promotion set ? where id=?",[req.body,req.params.id] , function(error) {
      if (error) throw error;
      res.json({ message: "修改成功" });
    });
  })
  .delete(function(req, res) {
    //res.send("刪除 " + req.params.id + " 資料");
    connection.query(
      "delete from promotion where id=?",req.params.id,function(error, results) {
        if (error) throw error;
        res.json({ message: "刪除成功" });
      }
    );
  });

module.exports = router;
