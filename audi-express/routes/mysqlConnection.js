const mysql = require("mysql");

//lulu's server
// const connectionConfig = {
// 	multipleStatements : true,
// 	host               : "35.221.135.125",
// 	user               : "root",
// 	password           : "ILt4x0LENjpdr8Dr",
// 	database           : "audi"
// };

// local DB
const connectionConfig = {
    multipleStatements: true,
    host: "localhost",
    user: "root",
    password: "",
    database: "audi"
  }

var connection = mysql.createConnection(connectionConfig);

connection.connect(function(err) {
	if (err) {
		console.error("error connecting: " + err.stack);
		return;
	}
	console.log("connected as id " + connection.threadId);
});

module.exports = { connection, connectionConfig };
