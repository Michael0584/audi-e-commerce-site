var express = require("express");
var router = express.Router();

// http://localhost:3000/api/profile
router.use(function (req, res) {
  res.json({ 
    isLogin       : req.session.isLogin,
    id            : req.session.id,
    email         : req.session.email,
    password      : req.session.password,
    username      : req.session.username || 'guest',
    mobile        : req.session.mobile,
    birthday      : req.session.birthday,
    county        : req.session.county,
    district      : req.session.district,
    address       : req.session.address
  })
})


module.exports = router;