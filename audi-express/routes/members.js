var express = require("express");
var router = express.Router();
var { connection } = require("./mysqlConnection");

// 註冊 http://localhost:3000/members/registered
router
	.route("/registered")
	.get((req, res) => {
		connection.query("select * from `members`", function(error, rows) {
			if (error) throw error;
			res.send(rows);
		});
	})
	.post((req, res) => {
		// 會員註冊 POST
		var _user = req.body;
		connection.query("select * from `members` where email=?", _user.email, function(error, rows) {
			if (rows.length >= 1) {
				res.send({ message: "註冊失敗, 已有重複的E-mail" });
			} else {
				connection.query("insert into `members` set ?", _user, function(error, result) {
					res.send({ message: "註冊成功" });
					// return res.redirect("http://localhost:3001/login");
				});
			}
		});
	});

// 登入 http://localhost:3000/members/signIn
router.route("/signIn").post((req, res) => {
	let url = req.body.url;
	connection.query("select * from `members` where email=?", req.body.email, function(error, rows) {
		if (error) throw error;
		if (rows == "") {
			res.send({ message: "此帳號不存在" });
		} else if (rows[0].password != req.body.password) {
			res.send({ message: "密碼輸入錯誤" });
		} else {
			req.session.isLogin  = true;
			req.session.id 			 = rows[0].id;
			req.session.email 	 = rows[0].email;
			req.session.password = rows[0].password;
			req.session.username = rows[0].username;
			req.session.mobile	 = rows[0].mobile;
			req.session.birthday = rows[0].birthday;
			req.session.county   = rows[0].county;
			req.session.district = rows[0].district;
			req.session.address  = rows[0].address;
			req.session.isAdmin  = rows[0].isAdmin;
			res.json({ message: "登入成功", success: true });
		}
	});
});

// 登出 http://localhost:3000/members/logout
router.route("/logout").get((req, res) => {
	req.session.destroy();
	return res.redirect("http://localhost:3001/login");
});

// 查看session http://localhost:3000/members/session
router.route("/session").get((req, res) => {
	res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
	res.setHeader("Access-Control-Allow-Credentials", "true");
	res.send(req.session);
	// console.log(req.session);
});

// 撈會員Email !!不要動這隻不然整個訂單系統會壞掉!!
// http://localhost:3000/members/member/:email
router
	.route("/member/:email")
	.get((req, res) => {
		// 讀單一個會員
		connection.query("select * from `members` where email=?", req.params.email, function(error, row) {
			if (error) throw error;
			res.send(row);
		});
	})

// 會員修改 http://localhost:3000/members/member/
router
	.route("/member")
	.get((req, res) => {
		connection.query("select * from `members` where email=?", [req.session.email], function(error, rows) {
			if (error) throw error;
			res.send(rows);
		});
	})
	.put((req, res) => {
		console.log(req.body)
		res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
		res.setHeader("Access-Control-Allow-Credentials", "true");
		var _member = req.body
		connection.query("update `members` set ? where email=? ",	[_member, req.session.email], function(error) {
			if (error) throw error;
			res.json({ message: "修改成功" });
		});
	});

// 抓會員id http://localhost:3000/members/memberid/:id
router
	.route("/memberid/:id")
	.get((req, res) => {
		connection.query("SELECT * from `members` where id=?", req.params.id, function(error, results) {
			if (error) throw error;
			res.json(results);
		});
	});

// 追蹤 http://localhost:3000/members/follow
router
  .route("/follow")
	.get((req, res) => { 
		connection.query("select * from follow_list,members WHERE follow_list.member_email=members.email AND members.email=? ORDER BY sid DESC", [req.session.email], function (error, row) {
			if (error) throw error;
      res.json(row);
		});
	});

// 刪除追蹤 http://localhost:3000/members/follow/:sid
router
.route("/follow/:sid")
	.delete((req, res) => {
		connection.query("delete from `follow_list` where sid=?", req.params.sid, function(error){
			if(error) throw error;
			res.json({ message: "刪除成功" });
		});	
	});

// 訂單 http://localhost:3000/members/member_order
router
.route("/member_order")
.get((req, res) => { 
	connection.query("select * from orders,members WHERE orders.member_email= members.email AND members.email=? ORDER BY order_sid DESC", [req.session.email], function (error, row) {
		if (error) throw error;
		res.json(row);
	});
})

module.exports = router;
