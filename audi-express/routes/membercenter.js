var express = require("express");
var router = express.Router();
var {
  connection
} = require("./mysqlConnection");
var multer = require("multer");
const path = require('path')
const crypto = require('crypto')

const getHashFileName = fileName => {
  const md5 = crypto.createHash('md5')
  return md5.update(fileName).digest('hex') + '.' + fileName.split('.').pop()
}

//設定上傳檔案的資料夾
const uploadFolder = path.resolve(__dirname, '../public/uploads')
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadFolder)
  },
  filename: function (req, file, cb) {
    cb(null, `${getHashFileName(file.originalname)}`)
  }
})

const upload = multer({
  storage: storage
})

router.post('/contact/upload', upload.single('mailImage'), function (req, res, next) {
  res.send(req.file);
})

router.route("/contact")
  .get(function (req, res) {
    connection.query("SELECT `order_id` FROM `orders` WHERE `member_email`=?", [req.session.email],
      function (err, results) {
        if (err) throw (err);
        res.json(results.map(order_id => order_id.order_id))
      })
  })

  .post(function (req, res) {
    console.log(req.body)
    var mail = {
      mail_sender: req.body.mailSender,
      mail_recipient: req.body.mailRecipient,
      mail_categories: req.body.mailCategories,
      mail_subject: req.body.mailSubject,
      mail_detail: req.body.mailDetail,
      mail_image_name: req.body.mailImageName ? `http://localhost:3000/uploads/${getHashFileName(req.body.mailImageName)}` : '',
      mail_date: req.body.mailDate,
    }
    connection.query('insert into mailbox set ?', mail, function (err) {
      if (err) console.error(err);
      res.json({
        message: "新增成功"
      })
    })
  })

router.route("/inbox")
  .get(function (req, res) {
    connection.query("SELECT * FROM `mailbox` where `mail_recipient`=? ORDER BY id DESC ",
      [req.session.email],
      function (err, results) {
        if (err) throw (err);
        let mails = results.map(objectToCamelCase)
        res.json(mails);
      });
  }).delete(function (req, res) {
    connection.query('DELETE FROM `mailbox` where id in (?)', [req.body.ids], function (err, results) {
      if (err) {
        console.error(err)
        res.json({
          message: "失敗"
        })
        return
      }
      res.json({
        message: "刪除成功",
        success: true
      });
    })
  })
router.route("/inbox/:id")
  .delete(function (req, res) {
    connection.query('delete from mailbox where id=?', req.params.id, function (err, result) {
      if (err || result.affectedRows === 0) throw (err);
      res.json({
        message: "刪除成功"
      })
    })
  })
  .get(function (req, res) {
    connection.query('SELECT * FROM `mailbox` where id=? ', req.params.id, function (err, results) {
      if (err) throw (err);
      let mails = results.map(objectToCamelCase)
      res.json(mails);
    })
  })
  .put(function (req, res) {
    connection.query("update `mailbox` set un_read=1 where id=?", req.params.id, function (error) {
      if (error) throw error;
      res.json({
        message: "修改成功"
      });
    });
  })

router.route("/outbox")
  .get(function (req, res) {

    connection.query('SELECT * FROM `mailbox` where `mail_sender`=? ORDER BY id DESC', [req.session.email], function (err, results) {
      if (err) throw (err);
      let mails = results.map(objectToCamelCase)
      res.json(mails);
    })
  }).delete(function (req, res) {
    connection.query('DELETE FROM `mailbox` where id in (?)', [req.body.ids], function (err, results) {
      if (err) {
        console.error(err)
        res.json({
          message: "失敗"
        })
        return
      }
      res.json({
        message: "刪除成功",
        success: true
      });
    })
  })
router.route("/outbox/:id")
  .delete(function (req, res) {
    connection.query('delete from mailbox where id=?', req.params.id, function (err, result) {
      if (err || result.affectedRows === 0) throw (err);
      res.json({
        message: "刪除成功"
      })
    })
  })
  .get(function (req, res) {
    connection.query('SELECT * FROM `mailbox` where id=? ', req.params.id, function (err, results) {
      if (err) throw (err);
      let mails = results.map(objectToCamelCase)
      res.json(mails);
    })
  })


router.route("/notifications")
  .get(function (req, res) {
    connection.query("SELECT COUNT(un_read) as unread FROM mailbox WHERE `mail_recipient`=? AND `un_read`= 0 ", [req.session.email], function (err, results) {
      if (err) console.log(err);
      res.json(results[0].unread)
    })
  })

router.route("/logout").get((req, res) => {
    req.session.destroy();
    return res.redirect("http://localhost:3006/");
  });
 
function objectToCamelCase(obj) {
  const result = {}
  for (let key in obj) {
    const camelCaseKey = toCamelCase(key)
    result[camelCaseKey] = obj[key]
  }
  return result
}

function toCamelCase(snakeCase) {
  const result = snakeCase.replace(/_\w/, function (match) {
    return match[1].toUpperCase();
  })
  return result
}

module.exports = router;