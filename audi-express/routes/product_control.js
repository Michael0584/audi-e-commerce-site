var express = require('express');
var router = express.Router();
var { connection } = require("./mysqlConnection");
const mysql = require('mysql')


//讀所有精品資料
router
  .route("/collections")
  .get(function (req, res) {
    connection.query("select * from collections_products", function (error, rows) {
      if (error) throw error;
      res.json(rows);
    })
  })

router
    .route("/collections/:id")
    .get(function (req, res) {
    connection.query("select * from collections_products where product_sid=?",req.body.product_sid, function (error, rows) {
        if (error) throw error;
        res.json(rows);
        })
    })
    .delete(function(req, res) {//刪除資料
    connection.query("delete from collections_products where product_sid=?",req.body.product_sid,function(error){
        if(error) throw error;
    })
}); 


  //讀所有配件資料
router
.route("/accessories")
.get(function (req, res) {
  connection.query("select * from accessories_products", function (error, rows) {
    if (error) throw error;
    res.json(rows);
  })
})


router
    .route("/accessories/:id")
    .get(function (req, res) {
    connection.query("select * from accessories_products where product_sid=?",req.body.product_sid, function (error, rows) {
        if (error) throw error;
        res.json(rows);
        })
    })
    .delete(function(req, res) {//刪除資料
    connection.query("delete from accessories_products where product_sid=?",req.body.product_sid,function(error){
        if(error) throw error;
    })
}); 



module.exports = router;