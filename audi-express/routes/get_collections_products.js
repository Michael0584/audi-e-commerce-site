var express = require('express');
var router = express.Router();
var { connection } = require("./mysqlConnection");
const mysql = require('mysql')



router
  .route("/products/collections/queryall")
  //req = request from client side
  //res = response from server side
  .get(function (req, res) {
    // get data entries from sql database: Categories
    // connection.query(
    // 	"SELECT accessories_products.product_sid, accessories_products.product_name, accessories_products.description, accessories_products.image_path, accessories_products.image_path, accessories_products.size_spec, accessories_products.color, accessories_products.price, accessories_products.discount_status, accessories_products.discount_price, accessories_products.promotion_code, accessories_products.stock, accessories_products.on_air, accessories_products.applicable_car_family, accessories_products.installation_required, accessories_products.accessories, accessories_products.collections, accessories_sub_categories.sub_category_name, accessories_categories.category_name FROM accessories_products INNER JOIN accessories_sub_categories ON accessories_products.sub_category_id=accessories_sub_categories.sub_category_id INNER JOIN accessories_categories ON accessories_sub_categories.parent_category_id=accessories_categories.category_id",
    // 	function(error, results) {
    // 		if (error) throw error; //if there is an error, show the error
    // 		res.json(results); //display the data entry requested in json format
    // 	}
    // );
    connection.query(
      "SELECT * FROM `collections_categories`; SELECT * FROM `collections_sub_categories`;", [],
      function (error, results) {
        if (error) throw error;

        // `results` is an array with one element for every statement in the query:
        //[0] = categories
        //[1] = sub-categories
        //[2] = categories
        const categories = results[0]
        const subCategories = results[1]
        categories.map(category => {
          category.categoryID = category.category_id
          category.categoryName = category.category_name
          category.subCategories = subCategories.filter(subcategory => subcategory.parent_category_id === category.category_id)
          return category
        })

        res.json(categories);
      }
    );
  });


//讀最新的15筆
// http://localhost:3000/api/products/collections/newest_product
router
  .route("/products/collections/newest_product")
  .get(function (req, res) {
    connection.query("SELECT * FROM `collections_products` WHERE 1 ORDER BY `created_at` DESC LIMIT 15;", function (error, rows) {
      if (error) throw error;
      res.json(rows);
    })
  })

//讀所有資料
router
  .route("/products/collections")
  .get(function (req, res) {
    connection.query("select * from collections_products", function (error, rows) {
      if (error) throw error;
      res.json(rows);
    })
  })
// 讀出所有分類大項
router
  .route("/products/collections/:category")
  .get(function (req, res) {                                                                                             //主選單分類                                     //內部選單分類
    connection.query("SELECT * FROM collections_categories,collections_sub_categories,collections_products WHERE collections_categories.category_id=collections_sub_categories.parent_category_id AND collections_sub_categories.sub_category_id=collections_products.categories_sub_id AND collections_categories.category_id=?",
    req.params.category ,function (error, rows) {
      if (error) throw error;
      res.json(rows);
    })
  })

  // collections_sub_categories.sub_category_id

  // 讀出所有分類細項
  router
  .route("/products/collections/:category/:subcategory")
  .get((req, res) => {
    connection.query(
      "SELECT * FROM collections_categories,collections_sub_categories,collections_products WHERE collections_categories.category_id=collections_sub_categories.parent_category_id AND collections_sub_categories.sub_category_id=collections_products.categories_sub_id AND collections_sub_categories.sub_category_id=?",
      req.params.subcategory,
      (error, rows) => {
        if (error) throw error;
        res.json(rows);
      }
    );
  });


  // SELECT * FROM collections_categories,collections_sub_categories,collections_products WHERE collections_categories.category_id=collections_sub_categories.parent_category_id AND collections_sub_categories.sub_category_id=collections_products.categories_sid AND collections_sub_categories.sub_category_id=1






router
  .route("/collections_detail/:sid")
  .get(function (req, res) {//讀出單筆資料
    connection.query("SELECT * FROM `collections_products` WHERE product_id = ?", req.params.sid, function (error, rows) {
      if (error) throw error;
      res.json(rows);
    })
  })

  router.route("/products/collections/:category/:subcategory/:product_sid").get((req, res) => {
    connection.query( //讀出單筆資料
      "SELECT * FROM collections_categories,collections_sub_categories,collections_products WHERE collections_categories.category_id=collections_sub_categories.parent_category_id AND collections_sub_categories.sub_category_id=collections_products.categories_sub_id AND product_sid =?",
      req.params.product_sid,
      (error, rows) => {
        if (error) throw error;
        res.json(rows);
      }
    );
  });




router
  .route("/collections_index/")
  .get(function (req, res) {//讀出清單資料
    connection.query("SELECT * FROM `collections_categories` WHERE 1", function (error, rows) {
      if (error) throw error;
      res.json(rows);
    })
  })



router
  .route("/products/collections/:category/:subcategory/:page")
  .get(function (req, res) {
    //先統計總共幾筆資料
    var query = "select count(*) as TotalCount from collections_products";
    var totalCount = 0;
    connection.query(query, function (error, row) {
      if (error) throw error;
      totalCount = row[0].TotalCount;

      //讀出分頁資料
      var LimitNum = 12   //一次讀取12筆資料
      var startNum = 0;    //從第幾筆開始讀
      if (req.params.page) {
        page = parseInt(req.params.page);
        startNum = (page - 1) * LimitNum;
      }
      var query = "select * from collections_products limit ? OFFSET ?";
      var params = [LimitNum, startNum];
      query = mysql.format(query, params);
      connection.query(query, function (error, row) {
        if (error) throw error;
        res.json({ "TotalCount": totalCount, "datas": row ,"has_pre":false , "has_next":true , "current_page":1});
      });
    });
  })

module.exports = router;
