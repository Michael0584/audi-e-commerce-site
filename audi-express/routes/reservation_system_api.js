var express = require("express");
var router = express.Router();
var { connection } = require("./mysqlConnection");

// GET all installation locations:
// http://localhost:3000/installation_locations/locations
router.route("/locations").get((req, res) => {
	connection.query("SELECT * FROM `installation_location` WHERE 1", (error, results) => {
		if (error) throw error;
		res.json(results);
	});
});

// GET specific installation location name:
// http://localhost:3000/installation_locations/locations/:name
router.route("/locations/:location_id").get((req, res) => {
	connection.query(
		"SELECT * FROM `installation_location` WHERE `location_id` = ?",
		req.params.location_id,
		(error, results) => {
			if (error) throw error;
			res.json(results);
		}
	);
});

// // GET specific installation location info:
// // http://localhost:3000/installation_locations/locations
// router.route("/locations/:location_name").get((req, res) => {
//     connection.query("SELECT * FROM `installation_location` WHERE 1",
//     req.params.location_name,
//     (error, results) => {
// 		if (error) throw error;
// 		res.json(results);
// 	});
// });

// GET all booked times of a specific location, if any.
// total time slot: 0-7, 0 = 9-10am 7 = 4-5pm, length = 8
// returns an array with object for each slot
router.route("/reservations/:location_id").get((req, res) => {
	connection.query(
		"SELECT * FROM `installation_reservation` WHERE `installation_reservation`.`location_id` = ?",
		req.params.location_id,
		function(error, results) {
			if (error) throw error;
			const timeSlots = results;
			res.json(timeSlots);
		}
	);
});

//POST installation booking time & location
// http://localhost:3000/installation_locations/reservations
// Required fields:
//  member_id
//  email
//  name
//  phone
//  location_id
//  slot_date
//  slot_time
//  order_id
router.route("/reservations").post((req, res) => {
	connection.query("INSERT INTO `installation_reservation` set ?", req.body, (error, result) => {
		if (error) throw error;
		res.json({ message: "Reservation successful." });
	});
});

//PUT installation booking time & location

module.exports = router;
