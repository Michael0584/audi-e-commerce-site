var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors'); //import cors
var session = require('express-session'); // 引入 express-session
var MySQLStore = require('express-mysql-session')(session);
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var accessoriesRouter = require('./routes/get_accessories_products');
var productsRouter = require('./routes/get_collections_products');
var membersRouter = require('./routes/members');
var membercenterRouter = require('./routes/membercenter');
var userFollowingRouter = require('./routes/user_following');
var profileRouter= require('./routes/profile')
var locationsRouter= require('./routes/reservation_system_api')
var orderRouter= require('./routes/order_api')
var { connectionConfig } = require('./routes/mysqlConnection')
var productControlRouter= require('./routes/product_control')
var promotionRouter = require('./routes/promotion');
var ppuRouter = require('./routes/ppu');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(cors({origin:['http://localhost:3001','http://localhost:3006'], credentials: true})); //init cors

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var sessionStore = new MySQLStore(connectionConfig);
// 登入用 express-session 設定
var sessionMiddleware=session({
  secret: "Shh, its a secret!",
  resave:true,
  store: sessionStore
  // cookie: { maxAge: 60 * 1000 } // cookie 1分鐘過期
})
app.use(sessionMiddleware);

// http://localhost:3000/
app.use('/', indexRouter);
// http://localhost:3000/users
app.use('/users', usersRouter);
// http://localhost:3000/products_db_api
app.use('/products_db_api', accessoriesRouter);
// http://localhost:3000/installation_locations
app.use('/installation_locations', locationsRouter);
// http://localhost:3000/order
app.use('/order', orderRouter);


app.use('/api/profile', profileRouter);
// http://localhost:3000/api/products
app.use('/api', productsRouter);
// http://localhost:3000/members
app.use('/members', membersRouter);
// http://localhost:3000/membercenter/contact
app.use('/membercenter', membercenterRouter);
//http://localhost:3000/promotion_api/promotion
app.use('/promotion_api', promotionRouter);
//http://localhost:3000/ppu_api/ppu
app.use('/ppu_api', ppuRouter);

// catch 404 and forward to error handler
// http://localhost:3000/user
app.use('/user', userFollowingRouter);

// http://localhost:3000/control
app.use('/control', productControlRouter);




app.use(function(req, res, next) {
  next(createError(404));
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = {app,sessionMiddleware};
